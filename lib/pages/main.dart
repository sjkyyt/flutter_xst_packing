import 'dart:async';
import 'dart:convert';
import 'package:animated_toggle_switch/animated_toggle_switch.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jpush_flutter/jpush_flutter.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/libs/utils/tools.dart';
import 'package:packing/pages/my.dart';
import 'package:packing/pages/order/order_new.dart';
import 'package:packing/pages/order/order_new_abnormal.dart';
import 'package:packing/pages/order/order_new_select_packer.dart';
import 'package:packing/pages/order/order_old.dart';
import 'package:packing/pages/order/order_old_search.dart';
import 'package:packing/pages/order/order_transfer.dart';
import 'package:packing/pages/widgets/other.dart';
import 'package:packing/provider/gateway_provider.dart';
import 'package:packing/provider/home_provider.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with SingleTickerProviderStateMixin {
  //, AutomaticKeepAliveClientMixin
  final JPush jpush = JPush();
  final player = AudioPlayer();

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<OrderNewPageState> newOrderKey1 = GlobalKey<OrderNewPageState>();
  final GlobalKey<OrderNewPageState> newOrderKey2 = GlobalKey<OrderNewPageState>();
  final GlobalKey<TransferOrderState> transferOrderKey = GlobalKey<TransferOrderState>();

  final GetXDrawerController drawerController = Get.put(GetXDrawerController());
  final GetXOrderController getOrderController = Get.find();
  final GetSettingController getSettingController = Get.find();

  late String userName;

  late TabController bottomTabController;
  late Color newOrderDayNumColor;

  @override
  void initState() {
    int expire = getSettingController.login.expire!;
    int nowTime = Tools.getNowTimestamp();
    bool isLogin = expire > 0 && expire >= nowTime ? true : false;
    if (!isLogin) {
      Get.offAllNamed('/login', arguments: {'registrationId': getSettingController.get<String>('registrationId')});
    }

    userName = getSettingController.login.realName;
    newOrderDayNumColor = getOrderController.tabIndex.value == 0 ? XSTColors.shop : XSTColors.success;

    bottomTabController = TabController(length: 4, vsync: this);

    initJpush();

    getOrderController.pageTo(getOrderController.tabIndex.value);
    bottomTabController.animateTo(getOrderController.tabIndex.value, duration: const Duration(seconds: 0));
    super.initState();
  }

  Future initJpush() async {
    if (GetPlatform.isAndroid) {
      try {
        //监听消息通知
        jpush.addEventHandler(onReceiveNotification: (Map<String, dynamic> message) async {
          // 接收通知回调方法。
          // print("接收通知回调: $message");
          var extras = message['extras']['cn.jpush.android.EXTRA'];
          Map jsonMap = json.decode(extras);
          switch (jsonMap['sound']) {
            case 'new_order':
              // 新订单
              await player.play(AssetSource('audio/new.mp3'));
              break;
            case 'transfer_order':
              // 转单
              await player.play(AssetSource('audio/transfer.mp3'));
              break;
          }
        }, onOpenNotification: (Map<String, dynamic> message) async {
          // 点击通知回调方法。
          // print("点击通知回调: $message");
          getOrderController.onRefresh();
          var extras = message['extras']['cn.jpush.android.EXTRA'];
          Map jsonMap = json.decode(extras);
          switch (jsonMap['sound']) {
            case 'new_order':
              // 新订单
              getOrderController.pageTo(0);
              bottomTabController.animateTo(0);
              break;
            case 'transfer_order':
              // 转单
              getOrderController.pageTo(2);
              bottomTabController.animateTo(2);
              break;
          }
          jpush.clearAllNotifications();
        }, onReceiveMessage: (Map<String, dynamic> message) async {
          // 接收自定义消息回调方法。
          // print("接收自定义消息回调:$message");
        });
      } catch (e) {
        // print('极光sdk配置异常');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: XSTColors.background,
      appBar: AppBar(
        backgroundColor: XSTColors.shop,
        leadingWidth: 45,
        foregroundColor: Colors.white,
        surfaceTintColor: Colors.white,
        titleTextStyle: const TextStyle(fontSize: 20),
        title: titleWidget(),
        elevation: 5,
        shadowColor: Colors.grey,
        actions: [appBarSearchButtonWidget()],
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(48),
          child: Material(
            color: Colors.white,
            child: appBarBottomTab(),
          ),
        ),
      ),
      drawer: GetPlatform.isAndroid ? appMyDrawerWidget() : null,
      endDrawer: appBarDrawerWidget(),
      body: bodyWidget(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: floatingActionButtonWidget(),
    );
  }

  Widget bodyWidget() {
    return GetX<GetXOrderController>(
      init: getOrderController,
      builder: (data) {
        return IndexedStack(
          index: data.tabIndex.value,
          clipBehavior: Clip.hardEdge,
          children: [
            OrderNewPage(
              key: newOrderKey1,
              scaffoldKey: scaffoldKey,
              deliveryType: 1,
              online: data.isOnline.value,
              alone: getSettingController.get<bool>('alone', defaultValue: false),
            ),
            OrderNewPage(
              key: newOrderKey2,
              scaffoldKey: scaffoldKey,
              deliveryType: 2,
              online: data.isOnline.value,
              alone: getSettingController.get<bool>('alone', defaultValue: false),
            ),
            TransferOrderPage(
              key: transferOrderKey,
              online: data.isOnline.value,
            ),
            const OrderOldPage()
          ],
        );
      },
    );
  }

  Widget titleWidget() {
    return Row(
      children: [
        Container(
          width: 110,
          height: 35,
          padding: const EdgeInsets.only(top: 5),
          alignment: Alignment.center,
          child: GetX<GetXOrderController>(
            init: getOrderController,
            builder: (data) {
              return AnimatedToggleSwitch<bool>.dual(
                current: data.isOnline.value,
                first: true,
                second: false,
                spacing: 5.0,
                minTouchTargetSize: 40,
                loadingIconBuilder: _loadingIconBuilder,
                style: const ToggleStyle(
                  borderColor: Colors.transparent,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: Offset(0, 1.5),
                    ),
                  ],
                ),
                borderWidth: 0.0,
                height: 35,
                onChanged: (b) async {
                  if (b) {
                    getOrderController.setOnlineLoading(true);
                    await GatewayProvider.onLine();
                    getOrderController.setOnlineLoading(false);
                    getOrderController.onRefresh(old: false);
                  } else {
                    getOrderController.setOnlineLoading(true);
                    await GatewayProvider.offLine();
                    getOrderController.setOnlineLoading(false);
                  }
                  getOrderController.isOnline.value = b;
                },
                // loading: true,
                styleBuilder: (b) => ToggleStyle(
                  backgroundColor: b ? Colors.white : Colors.white70,
                  indicatorColor: b ? Colors.green : Colors.white38,
                  borderRadius: const BorderRadius.horizontal(left: Radius.circular(50.0), right: Radius.circular(50.0)),
                  indicatorBorderRadius: BorderRadius.circular(b ? 50.0 : 50.0),
                ),
                iconBuilder: (value) => Icon(
                  value ? Icons.access_time_rounded : Icons.power_settings_new_rounded,
                  size: 25.0,
                  color: value ? Colors.white : const Color.fromRGBO(255, 0, 0, 1),
                  weight: 5,
                ),
                textBuilder: (value) => Center(
                  child: Text(
                    value ? '在线' : '离线',
                    style: TextStyle(color: value ? Colors.black : Colors.white, fontSize: 11),
                  ),
                ),
              );
            },
          ),
        ),
        const SizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(userName, style: const TextStyle(fontSize: 16)),
            Text(getSettingController.login.storeName!, style: const TextStyle(fontSize: 13), softWrap: true, maxLines: 2),
          ],
        ),
      ],
    );
  }

  Widget appBarBottomTab() {
    return GetBuilder<GetXOrderController>(
      builder: (data) {
        return TabBar(
            controller: bottomTabController,
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorColor: XSTColors.danger,
            labelColor: XSTColors.danger,
            unselectedLabelColor: XSTColors.info,
            tabs: [
              _tabItem('配送/自提', data.deliveryNumber.value),
              _tabItem('乡镇', data.townshipNumber.value),
              _tabItem('转单', data.transferNumber.value),
              _tabItem('历史订单', data.oldNumber.value),
            ],
            onTap: (index) {
              getOrderController.pageTo(index);
            });
      },
    );
  }

  Tab _tabItem(String title, int number) {
    return Tab(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(title, style: const TextStyle(fontSize: 11)),
          Text('($number)', style: const TextStyle(fontSize: 10)),
        ],
      ),
    );
  }

  Widget appBarSearchButtonWidget() {
    return GetX<GetXOrderController>(
      init: getOrderController,
      builder: (data) {
        String text = (data.selectedTime.isNotEmpty || (data.startTime.isNotEmpty && data.endTime.isNotEmpty)) ? '已筛选' : '筛选';
        if (data.tabIndex.value == 3) {
          return Container(
            // height: 30,
            width: 65,
            margin: const EdgeInsets.only(top: 5, bottom: 5, right: 10),
            child: InkWell(
              hoverColor: Colors.transparent,
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              onTap: () {
                drawerController.setDrawer(DrawerType.old);
                scaffoldKey.currentState!.openEndDrawer();
              },
              child: Text(
                text,
                style: const TextStyle(fontSize: 14),
                textAlign: TextAlign.center,
              ),
            ),
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }

  Widget appMyDrawerWidget() {
    return Drawer(
      backgroundColor: Colors.grey[200],
      shape: const ContinuousRectangleBorder(borderRadius: BorderRadius.zero),
      child: const MyPage(),
    );
  }

  Widget appBarDrawerWidget() {
    Widget currentDrawerWidget = GetBuilder<GetXDrawerController>(builder: (data) {
      switch (data.selectedDrawer) {
        case DrawerType.transfer:
          return SelectPackerPage(
              scaffoldKey: scaffoldKey,
              onReturn: (packerId, packerName) async {
                HomePageProvider.createPackTransfer(data.orderId, packerId).then((res) {
                  if (res.status == 200) {
                    getOrderController.deleteNewOrderExpanded(data.orderId);
                    getOrderController.onRefresh(old: false);
                    showTopMessage('操作成功', '转给$packerName的转单已成功创建', type: HandleType.success);
                  } else {
                    String errMsg = res.msg.length > 100 ? res.msg.substring(0, 100) : res.msg;
                    showTopMessage('操作失败', errMsg, type: HandleType.error);
                  }
                });
              });
        case DrawerType.abnormal:
          return AbnormalOrderPage(
            scaffoldKey: scaffoldKey,
            onReturn: (mark) {
              HomePageProvider.finishPack(data.orderId, normal: 2, mark: mark).then((value) {
                if (value) {
                  // getOrderController.deleteNewOrderExpanded(data.orderId);
                  getOrderController.onRefresh(old: false);
                }
              });
            },
          );
        case DrawerType.old:
          return SearchOldPage(
            scaffoldKey: scaffoldKey,
            onReturn: (startTime, endTime) async {
              getOrderController.setOldOrderNull();
              getOrderController.setLoading(true);
              await getOrderController.getOldOrder(pages: 1, startTime: startTime, endTime: endTime);
              getOrderController.setLoading(false);
            },
          );
        default:
          return const SizedBox();
      }
    });
    return Drawer(
      backgroundColor: Colors.grey[200],
      // shape: const ContinuousRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(0), bottomLeft: Radius.circular(0))),
      child: currentDrawerWidget,
    );
  }

  Widget floatingActionButtonWidget() {
    return GetX<GetXOrderController>(
        init: getOrderController,
        builder: (controller) {
          if (controller.tabIndex < 3) {
              if (controller.onlineLoading.isTrue) {
                return const SizedBox();
              } else {
                return MaterialButton(
                  elevation: 3,
                  color: controller.isOnline.isTrue ? Colors.white : XSTColors.shop,
                  textColor: controller.isOnline.isTrue ? Colors.black : Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  onPressed: () async {
                    if (controller.isOnline.isTrue) {
                      getOrderController.onRefresh();
                    } else {
                      if (!controller.isOnline.isTrue) {
                        getOrderController.setOnlineLoading(true);
                        bool action = await GatewayProvider.onLine();
                        getOrderController.setOnlineLoading(false);
                        if (action) {
                          getOrderController.setOnline(1);
                        }
                      }
                    }
                  },
                  child: SizedBox(
                    width: Get.width - 50,
                    height: 45,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(controller.isOnline.isTrue ? Icons.refresh : Icons.online_prediction),
                        const SizedBox(width: 20),
                        Text.rich(
                          TextSpan(text: controller.isOnline.isTrue ? '刷新' : '上线', children: [
                            if (controller.isOnline.isTrue)
                              const TextSpan(
                                text: '\t\t\t\t(30秒自动刷新)',
                                style: TextStyle(fontSize: 10),
                              ),
                          ]),
                        ),
                      ],
                    ),
                  ),
                );
              }
          } else {
            return const SizedBox();
          }
        });
  }

  Widget _loadingIconBuilder(BuildContext context, DetailedGlobalToggleProperties<dynamic> properties) {
    bool online = getSettingController.login.online == 1;
    Color color = online ? Colors.white : Colors.red;
    Color backgroundColor = online ? Colors.greenAccent : Colors.black12;
    double size = 15;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox.square(
        dimension: size,
        child: CircularProgressIndicator(backgroundColor: backgroundColor, color: color, strokeWidth: 3.0),
      ),
    );
  }
// @override
// TODO: implement wantKeepAlive
// bool get wantKeepAlive => true;
}
