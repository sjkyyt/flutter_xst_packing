import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:packing/libs/models/order_state.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/pages/order/widgets.dart';

class SettingAlonePage extends StatefulWidget {
  const SettingAlonePage({super.key});

  @override
  State<SettingAlonePage> createState() => _SettingAlonePageState();
}

class _SettingAlonePageState extends State<SettingAlonePage> {
  final GetXOrderController getOrderController = Get.find();
  late final ScrollController scrollController;
  late ValueNotifier<bool> isAlone;
  final abnormalList = Rx<List<OrderState>>([]);
  final dataList = Rx<List<OrderState>>([]);
  final ValueNotifier<List<int>> isChecked = ValueNotifier([]);

  final currentPage = 1.obs;
  final pageSize = 20.obs;
  final pageCount = 0.obs;
  final total = 0.obs;
  final loading = true.obs;
  final noMore = false.obs;
  final moreLoading = false.obs;

  @override
  void initState() {
    super.initState();
    isAlone = ValueNotifier(Get.arguments != null ? true : false);
    _handleRefresh();
    scrollController = ScrollController()
      ..addListener(() {
        if (scrollController.position.pixels == scrollController.position.maxScrollExtent) {
          _handleLoad();
        }
      });
  }

  Future<Null> _handleRefresh() async {
    currentPage.value = 1;
    dataList.value = [];
    abnormalList.value = [];
    loading.value = true;
    await Future.delayed(const Duration(milliseconds: 500)).then((value) async {
      dataList.value = getOrderController.abnormalList;
      dataList.value = dataList.value.reversed.toList();
      total.value = dataList.value.length;
      if (total.value > 0) {
        isAlone.value = isAlone.value ? true : false;
      }
      int end = 0;
      if (currentPage * pageSize.value < total.value) {
        end = currentPage.value * pageSize.value;
        noMore.value = false;
      } else {
        end = total.value;
        noMore.value = true;
      }
      if (total % pageSize.value == 0) {
        pageCount.value = total.value ~/ pageSize.value;
      } else {
        pageCount.value = total.value ~/ pageSize.value + 1;
      }
      abnormalList.value = dataList.value.sublist(0, end);
      loading.value = false;
    });
  }

  Future<void> _handleLoad() async {
    if (moreLoading.value) return;
    if (currentPage.value < pageCount.value) {
      currentPage.value += 1;
      moreLoading.value = true;
      await Future.delayed(const Duration(milliseconds: 500)).then((value) {
        int end = 0;
        if (currentPage.value * pageSize.value < total.value) {
          end = currentPage.value * pageSize.value;
          noMore.value = false;
        } else {
          end = total.value;
          noMore.value = true;
        }
        noMore.value = currentPage.value * pageSize.value < total.value ? false : true;
        abnormalList.value.addAll(dataList.value.sublist((currentPage.value - 1) * pageSize.value, end));
        moreLoading.value = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: XSTColors.background,
      appBar: AppBar(
        elevation: 3,
        shadowColor: Colors.grey,
        backgroundColor: Colors.white,
        surfaceTintColor: Colors.white,
        title: const Text('查看商品拣货记录', style: TextStyle(fontSize: 18)),
        // centerTitle: true,
        actions: [_builderActions()],
      ),
      body: _builderBodyWidget(),
    );
  }

  Widget _builderActions() {
      return Padding(
        padding: const EdgeInsets.only(right: 5),
        child: ValueListenableBuilder(
          valueListenable: isAlone,
          builder: (BuildContext context, bool isManage, Widget? child) {
            if (isManage) {
              return Obx(() {
                return Row(
                  children: [
                    if (isChecked.value.isNotEmpty)
                      InkWell(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                        onTap: () {
                          isAlone.value = dataList.value.isEmpty ? true : false;
                          getOrderController.deleteOrderAbnormalList(isChecked.value);
                          isChecked.value = [];
                          _handleRefresh();
                          setState(() {});
                        },
                        child: const SizedBox(
                          width: 45,
                          height: 25,
                          child: Center(child: Text('删除')),
                        ),
                      ),
                    const SizedBox(width: 5),
                    if (dataList.value.isNotEmpty)
                      InkWell(
                        borderRadius: const BorderRadius.all(Radius.circular(5)),
                        onTap: () {
                          if (isChecked.value.length == dataList.value.length) {
                            isChecked.value = [];
                          } else {
                            for (OrderState item in dataList.value) {
                              isChecked.value.add(item.packOrderId);
                            }
                          }
                          setState(() {});
                        },
                        child: SizedBox(
                          width: 45,
                          height: 25,
                          child: Center(child: Text(isChecked.value.length == dataList.value.length ? '反选' : '全选')),
                        ),
                      ),
                    const SizedBox(width: 5),
                    InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(5)),
                      onTap: () => isAlone.value = false,
                      child: const SizedBox(
                        width: 45,
                        height: 25,
                        child: Center(child: Text('返回')),
                      ),
                    ),
                  ],
                );
              });
            } else {
              return Obx(() {
                if (dataList.value.isNotEmpty) {
                  return InkWell(
                    borderRadius: const BorderRadius.all(Radius.circular(5)),
                    onTap: () => isAlone.value = true,
                    child: const SizedBox(
                      width: 45,
                      height: 25,
                      child: Center(child: Text('管理')),
                    ),
                  );
                } else {
                  return const SizedBox();
                }
              });
            }
          },
        ),
      );
  }

  Widget _builderBodyWidget() {
    return Obx(() {
      if (dataList.value.isNotEmpty) {
        return RefreshIndicator(
          onRefresh: _handleRefresh,
          child: ListView.builder(
            controller: scrollController,
            physics: const AlwaysScrollableScrollPhysics(),
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            itemBuilder: (BuildContext context, int index) {
              if (index < abnormalList.value.length) {
                return _builderItems(index);
              } else {
                if (moreLoading.value) {
                  return Container(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.grey[700],
                            color: Colors.red,
                          ),
                        ),
                        Text('加载中...', style: TextStyle(color: XSTColors.info, fontSize: 12)),
                      ],
                    ),
                  );
                } else {
                  return Container(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Center(
                      child: Text(noMore.isTrue ? '没有更多数据了' : '上滑加载更多数据', style: TextStyle(color: XSTColors.info, fontSize: 12)),
                    ),
                  );
                }
              }
            },
            itemCount: abnormalList.value.length + 1,
            cacheExtent: 20,
          ),
        );
      } else {
        if (loading.value) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 30,
                  height: 30,
                  child: CircularProgressIndicator(
                    color: Colors.red,
                  ),
                ),
                const SizedBox(height: 20),
                Text('加载中...', style: TextStyle(color: XSTColors.info, fontSize: 12)),
              ],
            ),
          );
        } else {
          return const Center(
            child: Text('当前还没有任何商品拣货记录'),
          );
        }
      }
    });
  }

  Widget _builderItems(int index) {
    OrderState data = dataList.value[index];
    List<OrderStateItem> list = data.goods;
    return ValueListenableBuilder(
      valueListenable: isAlone,
      builder: (BuildContext context, bool value, Widget? child) {
        return Container(
          margin: const EdgeInsets.only(bottom: 10),
          padding: const EdgeInsets.all(10),
          decoration: const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              if (value) ...[
                Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: 45,
                    child: ValueListenableBuilder(
                      valueListenable: isChecked,
                      builder: (BuildContext context, List<int> checkList, Widget? child) {
                        return Checkbox(
                          value: checkList.contains(data.packOrderId),
                          onChanged: (checked) {
                            if (checkList.contains(data.packOrderId)) {
                              isChecked.value.removeWhere((e) => e == data.packOrderId);
                            } else {
                              isChecked.value.add(data.packOrderId);
                            }
                            setState(() {});
                          },
                        );
                      },
                    ),
                  ),
                ),
              ],
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: double.infinity,
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(data.orderSn, style: const TextStyle(fontWeight: FontWeight.w600)),
                    ),
                    Divider(height: 0.5, color: XSTColors.line),
                    const SizedBox(height: 8),
                    widgetOrderStatus(data),
                    const SizedBox(height: 2),
                    Divider(height: 0.5, color: XSTColors.line),
                    Column(children: list.map((e) => _builderItem(e)).toList()),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _builderItem(OrderStateItem data) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              children: [
                cachedNetworkImage(data),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(data.goodName, style: const TextStyle(fontSize: 14), softWrap: true),
                        const SizedBox(height: 10),
                        Text(
                          data.mark,
                          style: TextStyle(fontSize: 12, color: data.abnormal ? XSTColors.danger2 : XSTColors.success2),
                          softWrap: true,
                          textAlign: TextAlign.right,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            width: 50,
            child: Text.rich(
              textAlign: TextAlign.right,
              TextSpan(
                text: 'x',
                style: TextStyle(color: Colors.grey[700], fontSize: 12),
                children: [
                  TextSpan(
                    text: data.number.toString(),
                    style: const TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget cachedNetworkImage(OrderStateItem data) {
    return CachedNetworkImage(
      imageUrl: data.image,
      width: 60,
      height: 60,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
      ),
      progressIndicatorBuilder: (context, url, downloadProgress) => Container(
        padding: const EdgeInsets.all(15),
        width: 20,
        height: 20,
        child: CircularProgressIndicator(
          color: Colors.grey[300],
          value: downloadProgress.progress,
          strokeWidth: 5,
          strokeCap: StrokeCap.round,
        ),
      ),
      errorWidget: (context, url, error) => Container(
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.circular(5),
        ),
        child: const Icon(
          Icons.error_outline_sharp,
          color: Colors.grey,
          size: 40,
        ),
      ),
    );
  }

  Widget widgetOrderStatus(OrderState order) {
    return Container(
      alignment: Alignment.topLeft,
      child: Column(
        children: [
          showTime('派单时间：', order.startTime),
          if (order.endTime > 0) showTime('完成时间：', order.endTime),
          showText('拣货状态：', setOrderStatus(order.packOrderStatus)),
        ],
      ),
    );
  }

  String setOrderStatus(int packOrderStatus) {
    List<String> arrString = ['未拣货', '拣货中', '完成', '', '', '', '全退已关'];
    return arrString[packOrderStatus - 1];
  }
}
