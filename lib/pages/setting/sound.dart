import 'dart:async';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:packing/libs/utils/constants.dart';

class SoundPage extends StatelessWidget {
  SoundPage({super.key});

  final AudioPlayer player = AudioPlayer();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: XSTColors.background,
      appBar: AppBar(
        title: const Text('提示音', style: TextStyle(fontSize: 18)),
        centerTitle: true,
      ),
      body: _bodyWidget(),
    );
  }

  Widget _bodyWidget() {
    return Column(
      children: [
        widgetTableSoundCard(
          children: [
            widgetTableCardRowSound(title: '新拣货单', sound: 'audio/new.mp3', line: true),
            widgetTableCardRowSound(title: '转单订单', sound: 'audio/transfer.mp3'),
          ],
        ),
        const SizedBox(height: 10),
        const SizedBox(height: 10),
      ],
    );
  }

  Widget widgetTableSoundCard({String title = '', required List<Widget> children}) {
    double marginTop = title.isNotEmpty ? 0 : 5;
    Widget titleWidget = Container(
      margin: const EdgeInsets.only(top: 5, left: 5, bottom: 2),
      child: Text(title, style: TextStyle(color: HexColor('#656565'), fontSize: 13)),
    );
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        title.isNotEmpty ? titleWidget : const SizedBox(),
        Container(
          margin: EdgeInsets.only(top: marginTop, bottom: 5),
          decoration: const BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                spreadRadius: 0,
                blurRadius: 1,
                offset: Offset(0, 0.5),
              ),
            ],
          ),
          child: Column(
            children: children,
          ),
        ),
      ],
    );
  }

  Widget widgetTableCardRowSound({
    required String title,
    required String sound,
    bool line = false,
    double lineWidth = 0.5,
  }) {
    final StreamController<bool> playController = StreamController();
    playController.add(false);
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(width: 10),
        Expanded(
          child: Container(
            padding: const EdgeInsets.only(top: 12, bottom: 12),
            decoration: BoxDecoration(
              border: line
                  ? Border(
                      bottom: BorderSide(
                        width: lineWidth,
                        color: XSTColors.line,
                      ),
                    )
                  : const Border(),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Text(title, style: const TextStyle(fontSize: 14)),
                ),
                StreamBuilder<bool>(
                  stream: playController.stream,
                  builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                    bool isPlay = false;
                    if (snapshot.hasData) {
                      isPlay = snapshot.data!;
                    }
                    return SizedBox(
                      width: 20,
                      height: 20,
                      child: IconButton(
                        iconSize: 22,
                        padding: EdgeInsets.zero,
                        icon: Icon(isPlay ? Icons.stop : Icons.play_arrow, color: Colors.black),
                        onPressed: () async {
                          if (!isPlay) {
                            playController.add(true);
                            await player.play(AssetSource(sound));
                            player.onPlayerStateChanged.listen((event) {
                              if (event == PlayerState.completed) {
                                playController.add(false);
                                player.release();
                              }
                            });
                          } else {
                            player.stop();
                            playController.add(false);
                          }
                        },
                      ),
                    );
                  },
                ),
                const SizedBox(width: 10)
              ],
            ),
          ),
        ),
        const SizedBox(width: 10),
      ],
    );
  }
}
