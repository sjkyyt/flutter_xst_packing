import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/pages/widgets/other.dart';
import 'package:packing/libs/utils/getx_controller.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final GetSettingController getSettingController = Get.find();
  late final isAlone = false.obs;

  @override
  void initState() {
    isAlone.value = getSettingController.get<bool>('alone', defaultValue: false);
    ever(isAlone, (callback) => getSettingController.set<bool>('alone', isAlone.value));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: XSTColors.background,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text('设置', style: TextStyle(fontSize: 18)),
        centerTitle: true,
      ),
      body: _bodyWidget(),
    );
  }

  Widget _bodyWidget() {
    return Column(
      children: [
        widgetTableCard(
          title: '拣货设置',
          children: [
            Obx(() => widgetTableCardRowSwitch(
              title: '单商品确认模式',
              subTitle: Text.rich(
                TextSpan(
                  text: '对订单的商品单独进行确认是否异常',
                  style: TextStyle(fontSize: 12, color: Colors.grey[700]),
                  children: [
                    if (isAlone.value) ...[
                      const TextSpan(
                        text: '\n开启后，您可以',
                      ),
                      TextSpan(
                        text: '查看记录',
                        style: const TextStyle(color: Colors.orangeAccent),
                        recognizer: TapGestureRecognizer()..onTap = () => Get.toNamed('/setting/alone'),
                      ),
                      const TextSpan(text: '或'),
                      TextSpan(
                        text: '清除记录',
                        style: const TextStyle(color: Colors.redAccent),
                        recognizer: TapGestureRecognizer()..onTap = () => Get.toNamed('/setting/alone', arguments: true),
                      ),
                    ],
                  ],
                ),
              ),
              tip: '开启后可以对订单的单个商品确认是否异常\n关闭后对整个订单商品一起确认',
              value: isAlone.value,
              onChanged: (value) {
                isAlone.value = value;
                return value;
              },
            )),
          ],
        ),
        widgetTableCard(
          title: '其它',
          children: [
            widgetTableCardRow(
              title: '提\t\t示\t\t音',
              showIcon: true,
              icon: Icons.audiotrack_outlined,
              iconColor: XSTColors.warning,
              isLink: true,
              line: true,
              onClick: () => Get.toNamed('/setting/sound'),
            ),
          ],
        ),
        const SizedBox(height: 20),
      ],
    );
  }
}
