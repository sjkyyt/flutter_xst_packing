import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:tdesign_flutter/tdesign_flutter.dart';

Widget widgetTableCard({String title = '', required List<Widget> children}) {
  double marginTop = title.isNotEmpty ? 0 : 5;
  Widget titleWidget = Container(
    margin: const EdgeInsets.only(top: 5, left: 5, bottom: 2),
    child: Text(title, style: TextStyle(color: HexColor('#656565'), fontSize: 13)),
  );
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      title.isNotEmpty ? titleWidget : const SizedBox(),
      Container(
        margin: EdgeInsets.only(top: marginTop, bottom: 5),
        decoration: const BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              spreadRadius: 0,
              blurRadius: 1,
              offset: Offset(0, 0.5),
            ),
          ],
        ),
        child: Column(
          children: children,
        ),
      ),
    ],
  );
}

Widget widgetTableCardRow({
  required String title,
  String subTitle = '',
  bool leading = true,
  bool showIcon = false,
  Color iconColor = Colors.black,
  bool isLink = false,
  bool line = false,
  bool redDot = false,
  IconData icon = Icons.more_horiz,
  void Function()? onClick,
}) =>
    InkWell(
      onTap: () {
        if (isLink) {
          onClick!();
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(width: 10),
          leading
              ? showIcon
                  ? Container(margin: const EdgeInsets.only(right: 10), width: 25, child: Icon(icon, size: 20, color: iconColor))
                  : const SizedBox(width: 35)
              : const SizedBox(width: 5),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(top: 12, bottom: 12),
              decoration: BoxDecoration(
                border: line
                    ? Border(
                        bottom: BorderSide(
                          width: 1,
                          color: XSTColors.line,
                        ),
                      )
                    : const Border(),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        SizedBox(width: leading ? 80 : 110, child: Text(title, style: const TextStyle(fontSize: 14))),
                        Expanded(child: Text(subTitle, style: TextStyle(color: XSTColors.info, fontSize: 13))),
                      ],
                    ),
                  ),
                  redDot
                      ? Container(
                          width: 6,
                          height: 6,
                          decoration: BoxDecoration(
                            color: Colors.red[700],
                            borderRadius: const BorderRadius.all(Radius.circular(50)),
                          ),
                        )
                      : const SizedBox(),
                  isLink ? Icon(Icons.arrow_forward_ios, color: XSTColors.info, size: 15) : const SizedBox(),
                  const SizedBox(width: 15)
                ],
              ),
            ),
          ),
        ],
      ),
    );

Widget widgetTableCardRowSwitch({
  required String title,
  Widget? subTitle,
  String? subTitleText,
  String tip = '',
  Duration showDuration = const Duration(milliseconds: 10000),
  bool value = true,
  bool Function(bool value)? onChanged,
}) {
  Widget subTitleWidget = subTitle ?? (subTitleText != null ? Text(subTitleText, style: TextStyle(fontSize: 12, color: Colors.grey[700])) : const SizedBox());
  return StatefulBuilder(
    builder: (BuildContext context, void Function(void Function()) setState) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(title),
                      const SizedBox(width: 20),
                      if (tip.isNotEmpty)
                        Tooltip(
                          message: tip,
                          triggerMode: TooltipTriggerMode.tap,
                          showDuration: showDuration,
                          child: const Icon(Icons.question_mark_rounded, color: Colors.grey, size: 22),
                        ),
                    ],
                  ),
                  subTitleWidget,
                ],
              ),
            ),
            TDSwitch(
              isOn: value,
              type: TDSwitchType.icon,
              onChanged: onChanged,
            ),
          ],
        ),
      );
    },
  );
}

/// 未完成
void showGetDefaultDialog(
  String title,
) {
  Get.defaultDialog(title: title);
}

void showTopMessage(
  String title,
  String message, {
  HandleType? type,
  Duration? duration = const Duration(milliseconds: 2000),
  Duration? animationDuration = const Duration(milliseconds: 500),
  TextButton? mainButton,
}) {
  Color defaultColor = Colors.white.withOpacity(0.4);
  Icon defaultIcon = const Icon(Icons.chevron_right);
  if (type != null) {
    switch (type) {
      case HandleType.primary:
        defaultColor = XSTColors.primary.withOpacity(0.2);
        defaultIcon = Icon(Icons.stay_primary_portrait, color: XSTColors.primary2);
        break;
      case HandleType.error:
        defaultColor = XSTColors.danger.withOpacity(0.2);
        defaultIcon = Icon(Icons.dangerous_outlined, color: XSTColors.danger2);
        break;
      case HandleType.warning:
        defaultColor = XSTColors.warning.withOpacity(0.2);
        defaultIcon = Icon(Icons.warning, color: XSTColors.warning2);
        break;
      case HandleType.info:
        defaultColor = XSTColors.info.withOpacity(0.2);
        defaultIcon = const Icon(Icons.info, color: Colors.white);
        break;
      case HandleType.success:
        defaultColor = XSTColors.success.withOpacity(0.2);
        defaultIcon = Icon(Icons.gpp_good_outlined, color: XSTColors.success2);
        break;
    }
  }
  Get.snackbar(
    title,
    message,
    duration: duration,
    animationDuration: animationDuration,
    colorText: Colors.white,
    backgroundColor: type != null ? defaultColor : null,
    icon: type != null ? defaultIcon : null,
    shouldIconPulse: true,
    borderRadius: 10,
    padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
    mainButton: mainButton,
  );
}
