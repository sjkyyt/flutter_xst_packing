import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/pages/main.dart';
import 'package:packing/pages/my.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:window_manager/window_manager.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({super.key});

  @override
  IndexState createState() => IndexState();
}

class IndexState extends State<IndexPage> {
  final GetSettingController getSettingController = Get.find();
  final PageController pageController = PageController();
  final currentIndex = 0.obs;

  @override
  void initState() {
    super.initState();
  }

  final pageList = [
    const MainPage(),
    const MyPage(),
  ];

  List<BottomNavigationBarItem> bottomBarItems = [
    BottomNavigationBarItem(icon: Tabbar.home.icon, label: Tabbar.home.label),
    BottomNavigationBarItem(icon: Tabbar.my.icon, label: Tabbar.my.label),
  ];

  List<NavigationRailDestination> navigationRailItems = [
    NavigationRailDestination(icon: Tabbar.home.icon, label: Text(Tabbar.home.label)),
    NavigationRailDestination(icon: Tabbar.my.icon, label: Text(Tabbar.my.label)),
  ];

  Widget bottomNavigationBar(int index) {
    return BottomNavigationBar(
      elevation: 15,
      iconSize: 30,
      selectedFontSize: 12,
      backgroundColor: Colors.white,
      type: BottomNavigationBarType.fixed,
      currentIndex: index,
      selectedIconTheme: const IconThemeData(color: Colors.red),
      selectedItemColor: Colors.red,
      onTap: (int idx) => currentIndex.value = idx,
      items: bottomBarItems,
    );
  }

  Widget navigationRailWidget(int index) {
    return Row(
      children: [
        NavigationRail(
          elevation: 10,
          leading: Container(
            width: 60,
            margin: const EdgeInsets.only(top: 10, bottom: 20),
            child: Column(
              children: [
                Image.asset('assets/images/logo.png', width: 60),
                const SizedBox(height: 10),
                const Text('拣货端', style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600)),
                const Divider(color: Colors.white),
              ],
            ),
          ),
          trailing: Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  onPressed: showExitDialog,
                  icon: const Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.exit_to_app, color: Colors.green, size: 20),
                      SizedBox(width: 10),
                      Text('退出', style: TextStyle(color: Colors.white, fontSize: 14)),
                    ],
                  ),
                ),
              ],
            ),
          ),
          selectedLabelTextStyle: const TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
          unselectedLabelTextStyle: const TextStyle(color: Colors.white),
          selectedIconTheme: const IconThemeData(color: Colors.red),
          unselectedIconTheme: const IconThemeData(color: Colors.white),
          indicatorColor: Colors.white,
          backgroundColor: HexColor('#2c3036'),
          destinations: navigationRailItems,
          selectedIndex: index,
          labelType: NavigationRailLabelType.all,
          minWidth: 90,
          minExtendedWidth: 90,
          onDestinationSelected: (idx) {
            currentIndex.value = idx;
            pageController.jumpToPage(idx);
          },
        ),
        Expanded(
          child: PageView(
            controller: pageController,
            children: pageList,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (GetPlatform.isAndroid) {
      return const MainPage();
    } else {
      return Obx(() => navigationRailWidget(currentIndex.value));
    }
  }

  void showExitDialog() => showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            surfaceTintColor: Colors.white,
            elevation: 5,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            title: const Text('您确定要退出本程序？'),
            actions: [
              TextButton(
                child: const Text('取消', style: TextStyle(color: Colors.grey)),
                onPressed: () => Get.back(),
              ),
              TextButton(
                child: const Text('确定'),
                onPressed: () async {
                  Get.back();
                  await windowManager.destroy();
                },
              ),
            ],
          );
        },
      );
}
