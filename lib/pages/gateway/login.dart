import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:packing/libs/models/user_login.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/libs/models/gateway.dart';
import 'package:packing/provider/gateway_provider.dart';
import 'package:packing/pages/order/widgets.dart';

import '../widgets/other.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final GlobalKey _formKey = GlobalKey<FormState>();

  final GetStateController getStateController = Get.put(GetStateController());
  final GetSettingController getSettingController = Get.find();
  final GetXOrderController getOrderController = Get.find();
  final ValueNotifier<bool> buttonDisable = ValueNotifier(false);
  final ValueNotifier<bool> showUserExpand = ValueNotifier(false);
  late String _registrationId;

  late TextEditingController accountController;
  late TextEditingController pwdController;
  late FocusNode accountNode;
  late FocusNode passwordNode;
  ValueNotifier<bool> accountEmpty = ValueNotifier(true);
  ValueNotifier<bool> passwordEmpty = ValueNotifier(true);
  bool _isObscure = true;
  Color _eyeColor = Colors.grey;

  @override
  void initState() {
    accountController = TextEditingController()
      ..addListener(() {
        accountEmpty.value = accountController.text == '' ? true : false;
      });
    pwdController = TextEditingController()
      ..addListener(() {
        passwordEmpty.value = pwdController.text == '' ? true : false;
      });

    accountNode = FocusNode();
    passwordNode = FocusNode()
      ..addListener(() {
        if (passwordNode.hasFocus) {
          showUserExpand.value = false;
        }
      });

    if (getStateController.userBox.values.isNotEmpty) {
      var userLogin = getStateController.userBox.values.last;
      accountController.text = userLogin.account;
      pwdController.text = userLogin.passWord;
    }

    _registrationId = (Get.arguments.isNotEmpty ? Get.arguments['registrationId'] : getSettingController.get<String>('registrationId')).toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: XSTColors.background,
      body: Container(
        padding: EdgeInsets.only(top: GetPlatform.isAndroid ? 20 : 50),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: kToolbarHeight),
              _buildFormTitleWidget(), // 登录标题
              Container(
                margin: const EdgeInsets.symmetric(vertical: 20),
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Center(
                  child: Container(
                    width: GetPlatform.isAndroid ? double.infinity : 400,
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colors.grey[50],
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Column(
                      children: [
                        const Text('您好！欢迎登录', style: TextStyle(fontSize: 20)),
                        const SizedBox(height: 10),
                        Stack(
                          clipBehavior: Clip.none,
                          children: [
                            Form(
                              key: _formKey,
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              child: Column(
                                children: [
                                  _buildTextInputWidget(
                                    controller: accountController,
                                    focusNode: accountNode,
                                    labelText: '帐号',
                                    hintText: '请输入帐号',
                                    prefixIcon: const Icon(Icons.supervisor_account),
                                    suffixIcon: ValueListenableBuilder<bool>(
                                      valueListenable: accountEmpty,
                                      builder: (BuildContext context, bool value, Widget? child) {
                                        return Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            suffixIcons(value, icon: Icons.clear, onTap: () {
                                              accountController.text = '';
                                              pwdController.text = '';
                                              showUserExpand.value = false;
                                            }),
                                          ],
                                        );
                                      },
                                    ),
                                    onTap: () => showUserExpand.value = !showUserExpand.value,
                                    onEditingComplete: () {
                                      showUserExpand.value = false;
                                      FocusScope.of(context).requestFocus(passwordNode);
                                    },
                                    validator: (value) {
                                      if (value!.isEmpty) return '用户帐号不能为空!';
                                      return null;
                                    },
                                  ),
                                  _buildTextInputWidget(
                                    controller: pwdController,
                                    focusNode: passwordNode,
                                    obscureText: _isObscure,
                                    keyboardType: TextInputType.visiblePassword,
                                    textInputAction: TextInputAction.done,
                                    labelText: '密码',
                                    hintText: '请输入密码',
                                    prefixIcon: const Icon(Icons.lock_person_sharp),
                                    suffixIcon: ValueListenableBuilder<bool>(
                                      valueListenable: passwordEmpty,
                                      builder: (BuildContext context, bool value, Widget? child) {
                                        return Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            suffixIcons(value, icon: Icons.clear, onTap: () => pwdController.text = ''),
                                            const SizedBox(width: 10),
                                            suffixIcons(value, icon: Icons.remove_red_eye, color: _eyeColor, onTap: () {
                                              if (mounted) {
                                                setState(() {
                                                  _isObscure = !_isObscure;
                                                  _eyeColor = (_isObscure ? Colors.grey : Theme.of(context).iconTheme.color)!;
                                                });
                                              }
                                            }),
                                          ],
                                        );
                                      },
                                    ),
                                    onFieldSubmitted: (value) => submitLogin(),
                                    validator: (value) {
                                      if (value!.isEmpty) return '用户密码不能为空!';
                                      if (value.length < 6) return '用户密码最少需要6位';
                                      return null;
                                    },
                                  ),
                                  buildLoginButtonWidget(context),
                                ],
                              ),
                            ),
                            ValueListenableBuilder(
                              valueListenable: showUserExpand,
                              builder: (ctx, value, child) {
                                return Positioned(
                                  right: 0,
                                  top: 40,
                                  child: _buildListView(value),
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildListView(bool showUserExpand) {
    return ValueListenableBuilder(
      valueListenable: getStateController.userBox.listenable(),
      builder: (BuildContext context, Box<UserLoginModel> box, Widget? child) {
        if (box.isNotEmpty) {
          int length = box.length;
          double itemHeight = 45.0;
          double dividerHeight = 1;
          double animationBegin = showUserExpand ? 0.0 : (length * itemHeight + (length - 1) * dividerHeight);
          double animationEnd = showUserExpand ? (length * itemHeight + (length - 1) * dividerHeight) : 0.0;
          return TweenAnimationBuilder(
            tween: Tween(begin: animationBegin, end: animationEnd),
            duration: const Duration(milliseconds: 150),
            builder: (BuildContext context, double boxHeight, Widget? child) {
              return Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: HexColor('#FD3F2A'), width: 1),
                  boxShadow: const [
                    BoxShadow(
                      offset: Offset(1, 1), // 阴影的偏移量
                      color: Colors.grey, // 阴影的颜色
                      blurRadius: 1, // 阴影的模糊程度
                      spreadRadius: 0, // 扩散的程度，如果设置成正数，则会扩大阴影面积；负数的话，则会缩小阴影面积
                    )
                  ],
                ),
                width: 180,
                height: boxHeight,
                child: ListView.separated(
                  reverse: true,
                  padding: const EdgeInsets.all(0),
                  itemBuilder: (BuildContext context, int index) {
                    return _buildItem(length, itemHeight, box.getAt(index)!);
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider(color: HexColor('#f5c6c0').withOpacity(0.5), height: dividerHeight);
                  },
                  itemCount: length,
                ),
              );
            },
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }

  ///构建单个历史记录item
  Widget _buildItem(
    int length,
    double itemHeight,
    UserLoginModel user,
  ) {
    return InkWell(
      splashColor: Colors.transparent,
      child: SizedBox(
        height: itemHeight,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(user.userName, style: const TextStyle(color: Colors.black, fontSize: 15)),
                    Text(user.account, style: const TextStyle(color: Colors.grey, fontSize: 12)),
                  ],
                ),
              ),
            ),
            InkWell(
              child: Padding(
                padding: const EdgeInsets.only(right: 8),
                child: Icon(Icons.highlight_off, color: Colors.grey[600]),
              ),
              onTap: () {
                if (accountController.text == user.account) {
                  accountController.text = '';
                  pwdController.text = '';
                }
                getStateController.userDelete(user);
                //处理最后一个数据，假如最后一个被删掉，将Expand置为false
                if (!(length > 1 || user != UserLoginModel(account: accountController.text, userName: user.userName, passWord: pwdController.text))) {
                  //如果个数大于1个或者唯一一个账号跟当前账号不一样才弹出历史账号
                  showUserExpand.value = false;
                }
              },
            ),
          ],
        ),
      ),
      onTap: () {
        accountController.text = user.account;
        pwdController.text = user.passWord;
        showUserExpand.value = false;
      },
    );
  }

  // 登录标题
  Widget _buildFormTitleWidget() {
    return const Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image(image: AssetImage('assets/images/logo.png'), fit: BoxFit.fill, width: 80),
        SizedBox(height: 10),
        Text('鲜升通拣货端', style: TextStyle(fontSize: 25)),
      ],
    );
  }

  Widget _buildTextInputWidget({
    Key? key,
    required TextEditingController controller,
    double height = 80,
    required FocusNode focusNode,
    bool obscureText = false,
    TextInputAction textInputAction = TextInputAction.next,
    TextInputType? keyboardType,
    required String labelText,
    required String hintText,
    required Widget prefixIcon,
    required Widget suffixIcon,
    void Function()? onTap,
    void Function()? onEditingComplete,
    void Function(String)? onFieldSubmitted,
    String? Function(String?)? validator,
  }) {
    return SizedBox(
      height: height,
      child: TextFormField(
        key: key,
        controller: controller,
        focusNode: focusNode,
        obscureText: obscureText,
        textInputAction: textInputAction,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          helperText: '',
          border: const OutlineInputBorder(),
          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: HexColor('#FD3F2A'), width: 1)),
          contentPadding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
          labelText: labelText,
          hintText: hintText,
          prefixIcon: prefixIcon,
          suffixIcon: Container(
            padding: const EdgeInsets.only(right: 10),
            width: 60,
            child: suffixIcon,
          ),
        ),
        onTap: onTap,
        onSaved: (value) => controller.text = value!,
        onEditingComplete: onEditingComplete,
        onFieldSubmitted: onFieldSubmitted,
        validator: validator,
      ),
    );
  }

  Widget buildLoginButtonWidget(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: buttonDisable,
      builder: (BuildContext context, bool value, Widget? child) {
        return Align(
          child: SizedBox(
            width: double.infinity,
            child: AbsorbPointer(
              absorbing: value,
              child: ElevatedButton(
                style: TextButton.styleFrom(
                  backgroundColor: value ? Colors.grey[300] : Colors.red[900],
                  foregroundColor: value ? Colors.white60 : Colors.white,
                ),
                onPressed: () => submitLogin(),
                child: const Text('登录'),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget suffixIcons(
    bool condition, {
    required IconData icon,
    Color? color,
    double size = 20,
    required Function() onTap,
  }) {
    if (condition) {
      return const SizedBox();
    } else {
      return InkWell(
        child: Icon(icon, color: color ?? Colors.grey, size: size),
        onTap: () => onTap(),
      );
    }
  }

  Future<void> submitLogin() async {
    if ((_formKey.currentState as FormState).validate()) {
      (_formKey.currentState as FormState).save();
      buttonDisable.value = true;
      EasyLoading.show(status: '加载中...');
      GatewayProvider.loginResult(account: accountController.text, password: pwdController.text, registrationId: _registrationId).then((res) async {
        EasyLoading.dismiss();
        if (res.status == 200) {
          LoginModel data = res.data as LoginModel;
          if (data.status == 1) {
            getSettingController.setUser<LoginModel>('user', data);
            getStateController.userAdd(UserLoginModel(
              account: accountController.text,
              userName: data.realName,
              passWord: pwdController.text,
            ));
            if (getSettingController.isLogin) {
              showTopMessage('登录成功', '${data.realName} 欢迎您回来!', type: HandleType.success);
              // getOrderController.onRefresh();
              getOrderController.onNewData();
              Get.offNamed('/home');
            }
          } else {
            buttonDisable.value = false;
            EasyLoading.showError('该帐号已被禁止使用！');
          }
        } else {
          buttonDisable.value = false;
          EasyLoading.showError(res.msg);
        }
      });
    }
  }
}
