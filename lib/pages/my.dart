import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/libs/utils/version.dart';
import 'package:packing/pages/order/widgets.dart';
import 'package:packing/pages/widgets/other.dart';
import 'package:package_info_plus/package_info_plus.dart';

class MyPage extends StatefulWidget {
  const MyPage({super.key});

  @override
  State<MyPage> createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> {
  final GetXOrderController getOrderController = Get.find();
  final GetSettingController getSettingController = Get.find();
  String _version = '';
  late String? _registrationId;
  late bool isLogin;

  @override
  void initState() {
    isLogin = getSettingController.isLogin;
    _registrationId = getSettingController.get<String>('registrationId', defaultValue: '');
    getVersion();

    super.initState();
  }

  void getVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      _version = packageInfo.version;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: XSTColors.background,
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.zero,
        child: SingleChildScrollView(
          child: Column(
            children: [
              _builderHeaderWidget(),
              const SizedBox(height: 10),
              _builderContentWidget(),
              const SizedBox(height: 10),
              _builderVersionWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _builderHeaderWidget() {
    String avatarUrl = getSettingController.login.avatar;
    return Column(
      children: [
        Container(
          color: XSTColors.shop,
          // color: Colors.transparent,
          height: context.mediaQueryPadding.top,
          width: double.infinity,
        ),
        Container(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.white,
            gradient: LinearGradient(
                begin: Alignment.topCenter, //渐变开始于上面的中间开始
                end: Alignment.bottomCenter, //渐变结束于下面的中间
                colors: [
                  XSTColors.shop,
                  HexColor('#e44645'),
                  HexColor('#f07676'),
                  HexColor('#fab9b8'),
                  HexColor('#fdebeb'),
                  HexColor('#F8F8FF'),
                  Colors.white,
                ]),
            boxShadow: const [
              BoxShadow(
                color: Colors.black12,
                spreadRadius: 0,
                blurRadius: 1,
                offset: Offset(0, 0.5),
              ),
            ],
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 60,
                height: 60,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white, width: 1),
                  image: DecorationImage(
                    image: NetworkImage(avatarUrl.isNotEmpty ? avatarUrl : 'https://tbzjiahui.tbzmy.com/assets/img/logo.png'),
                    fit: BoxFit.cover,
                  ),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.black26,
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: Offset(2, 2),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  height: 55,
                  margin: const EdgeInsets.only(left: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        getSettingController.login.realName,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 19,
                          fontWeight: FontWeight.w600,
                          shadows: [
                            BoxShadow(
                              color: Colors.black,
                              spreadRadius: 15,
                              blurRadius: 15,
                              offset: Offset(0, 0.5),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text.rich(
                            TextSpan(
                              text: 'ID：',
                              style: const TextStyle(color: Colors.black87, fontSize: 13),
                              children: [
                                TextSpan(
                                  text: getSettingController.login.packerId.toString(),
                                  style: const TextStyle(fontWeight: FontWeight.w600),
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _builderContentWidget() => Column(
        children: [
          widgetTableCard(
            children: [
              widgetTableCardRow(
                title: '所在门店',
                subTitle: getSettingController.login.storeName!,
              ),
            ],
          ),
          widgetTableCard(
            title: '用户',
            children: [
              widgetTableCardRow(
                title: '登录帐号',
                subTitle: getSettingController.login.account,
                showIcon: true,
                icon: Icons.account_box,
                iconColor: XSTColors.primary,
                line: true,
              ),
              widgetTableCardRow(
                title: '在线状态',
                subTitle: isLogin
                    ? getSettingController.login.online == 1
                        ? '在线'
                        : '离线'
                    : 'Token已过期或其它地方登录，请重新登录',
                showIcon: true,
                icon: Icons.online_prediction,
                iconColor: XSTColors.info2,
                line: true,
                isLink: !isLogin,
                onClick: _showLogoutDialog,
              ),
              widgetTableCardRow(
                title: '登录次数',
                subTitle: '${getSettingController.login.loginCount.toString()} 次',
                showIcon: true,
                icon: Icons.login,
                iconColor: XSTColors.success,
              ),
            ],
          ),
          widgetTableCard(
            title: '系统',
            children: [
              if (GetPlatform.isAndroid)
                widgetTableCardRow(
                  title: '设置',
                  showIcon: true,
                  icon: Icons.settings,
                  iconColor: XSTColors.warning,
                  isLink: true,
                  line: true,
                  onClick: () => Get.toNamed('/setting'),
                ),
              widgetTableCardRow(
                title: '检查更新',
                showIcon: true,
                icon: Icons.upload,
                iconColor: Colors.purple,
                line: true,
                isLink: true,
                redDot: getSettingController.get<bool>('newVersion', defaultValue: false),
                onClick: () => Version.checkVersion(
                  context,
                  plat: getSettingController.get('plat', defaultValue: Plat.android),
                  version: _version,
                  skipExecution: () => showTopMessage('没有更新', '当前已是最新版本', type: HandleType.info, duration: const Duration(seconds: 3)),
                  updateBack: (bool isNew, String backText, int plat) {},
                ),
              ),
              widgetTableCardRow(
                title: '帐号登出',
                showIcon: true,
                icon: Icons.logout,
                iconColor: XSTColors.shop,
                isLink: true,
                onClick: _showLogoutDialog,
              ),
            ],
          ),
        ],
      );

  Widget _builderVersionWidget() => Center(
        child: Text('当前版本：$_version', style: TextStyle(color: XSTColors.info, fontSize: 12)),
      );

  void _showLogoutDialog() {
    showDialog(
      context: context,
      builder: (BuildContext ctx) => showCustomDialog(
        title: '确定登出？',
        subTitle: '您确定要退出登录？',
        content: '登出后您将无法接收到最新的拣货单信息',
        onConfirm: () {
          getSettingController.loginBox.clear();
          getOrderController.setPoll(false);
          Get.offAllNamed('/login', arguments: {'registrationId': _registrationId});
        },
      ),
    );
  }
}
