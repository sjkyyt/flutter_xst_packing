import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:packing/libs/models/order.dart';
import 'package:packing/libs/models/order_state.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/pages/order/widgets.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/provider/home_provider.dart';
import 'package:packing/pages/order/order_new_confirm.dart';

class OrderDetailPage extends StatefulWidget {
  final OrderModel order;

  const OrderDetailPage({super.key, required this.order});

  @override
  State<OrderDetailPage> createState() => _OrderDetailPageState();
}

class _OrderDetailPageState extends State<OrderDetailPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GetXDrawerController ctrl = Get.find();
  final GetXOrderController orderController = Get.find();
  bool submitButtonLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: XSTColors.shop,
        foregroundColor: Colors.white,
        surfaceTintColor: Colors.white,
        title: const Text('开始拣货'),
        titleTextStyle: const TextStyle(fontSize: 18),
        centerTitle: true,
        actions: const [SizedBox()],
      ),
      body: _bodyWidget(),
      endDrawer: _endDrawer(),
    );
  }

  Widget _bodyWidget() {
    OrderModel data = widget.order;
    data.isExpanded = true;
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
      child: SingleChildScrollView(
        child: ValueListenableBuilder(
          valueListenable: orderController.abnormalBox.listenable(),
          builder: (BuildContext context, Box<dynamic> box, Widget? child) {
            return widgetNewOrderDetail(
              data,
              stateBox: orderController.getOrderAbnormalGoods(data.packOrderId),
              rightBoxColor: XSTColors.shop,
              rightBoxTextColor: Colors.white,
              onTap: (good) {
                OrderStateItem state = orderController.getGoodsAbnormal(packOrderId: data.packOrderId, skuId: good.skuId);
                ctrl.setSkuId(data.packOrderId, good.skuId, state.mark);
                scaffoldKey.currentState!.openEndDrawer();
              },
              closed: () => Get.back(),
              confirm: () {
                if (submitButtonLoading) {
                  EasyLoading.showToast('您点得太快了');
                  return;
                } else {
                  submitButtonLoading = true;
                  Future.delayed(const Duration(milliseconds: 2000)).then((value) => submitButtonLoading = false);
                  int normal = 1;
                  String mark = '';
                  for (OrderStateItem item in orderController.getOrderAbnormalGoods(data.packOrderId)) {
                    if (item.abnormal) {
                      normal = 2;
                      mark += '${mark.isNotEmpty ? '\n' : ''}${item.goodName} x ${item.number} - ${item.mark}';
                    }
                  }
                  showAlertDialog(
                    context,
                    title: '确认完成？',
                    content: '确定该订单已拣货完成？',
                    onTap: () {
                      HomePageProvider.finishPack(data.packOrderId, normal: normal, mark: mark).then((value) {
                        if (value) {
                          orderController.onRefresh();
                          Get.back();
                        }
                      });
                    },
                  );
                }
              },
            );
          },
        ),
      ),
    );
  }

  Widget _endDrawer() {
    return Drawer(
      backgroundColor: Colors.grey[200],
      child: GetBuilder<GetXDrawerController>(
        id: 'detail',
        builder: (_) {
          return ConfirmOrderPage(
            scaffoldKey: scaffoldKey,
            mark: _.mark,
            onReturn: (String mark) {
              orderController.setGoodsAbnormal(packOrderId: _.packOrderId, abnormal: mark == '无异常' ? false : true, skuId: _.skuId, mark: mark);
            },
          );
        },
      ),
    );
  }
}
