import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:packing/libs/models/order.dart';
import 'package:packing/libs/models/result_base.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/pages/order/order_detail.dart';
import 'package:packing/pages/order/widgets.dart';
import 'package:packing/provider/home_provider.dart';

class OrderNewPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final int deliveryType;
  final bool online;
  final bool alone;

  const OrderNewPage({
    super.key,
    required this.scaffoldKey,
    required this.deliveryType,
    required this.online,
    required this.alone,
  }) : assert(deliveryType > 0);

  @override
  State<OrderNewPage> createState() => OrderNewPageState();
}

class OrderNewPageState extends State<OrderNewPage> {
  final GetXDrawerController getDrawerController = Get.find();
  final GetXOrderController getOrderController = Get.find();
  final ScrollController scrollController = ScrollController();

  // 新订单下拉刷新方法
  Future<void> _handleRefresh(type) async {
    await Future.delayed(Duration(milliseconds: widget.online ? 500 : 0)).then((value) async {
      refreshData(type);
    });
  }

  void refreshData(int type) async {
    if (widget.online) {
      getOrderController.setLoading(true);
      await getOrderController.getNewOrder(type);
      getOrderController.setLoading(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      color: Colors.redAccent[200],
      onRefresh: () => _handleRefresh(widget.deliveryType),
      child: GetBuilder<GetXOrderController>(
        init: getOrderController,
        builder: (data) => _createListView(context, data.loading.value, widget.deliveryType == 1 ? data.deliveryData.value : data.townshipData.value),
      ),
    );
  }

  Widget _createListView(BuildContext context, bool isLoading, ResultList<OrderModel> data) {
    List<OrderModel> order = data.data;
    if (order.isNotEmpty && widget.online) {
      if (!GetPlatform.isAndroid) {
        return SingleChildScrollView(
          controller: scrollController,
          child: windowsTableViewOrderWidget(order, type: OrderType.order, transfer: (data) {
            getDrawerController.setOrderId(data.packOrderId);
            getDrawerController.setDrawer(DrawerType.transfer);
            widget.scaffoldKey.currentState!.openEndDrawer();
          }, picking: (data) {
            getOrderController.setDefaultGoodsAbnormal(order: data, goods: data.goods);
            showDialog(context: context, builder: (context) => OrderDetailPage(order: data));
          }),
        );
      } else {
        return ListView.builder(
          physics: const AlwaysScrollableScrollPhysics(),
          padding: const EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 65),
          itemBuilder: (BuildContext context, int index) => _itemBuilder(context, index, order),
          itemCount: order.length,
          controller: scrollController,
        );
      }
    } else {
      if (isLoading && widget.online) {
        return buildLoading();
      } else {
        double sizeHeight = Get.height;
        return ListView(
          physics: const AlwaysScrollableScrollPhysics(),
          children: [
            SizedBox(
              height: (sizeHeight / 2) + (sizeHeight / 5),
              child: orderClosed(open: widget.online, openText: widget.deliveryType == 1 ? '配送/自提拣货单数据为空' : '乡镇拣货单数据为空'),
            )
          ],
        );
      }
    }
  }

  Widget _itemBuilder(BuildContext context, int index, List<OrderModel> order) {
    OrderModel data = order[index];
    data.isExpanded = getOrderController.getNewOrderExpanded(data.orderId);
    bool submitButtonLoading = false;
    return widgetNewOrderList(
      data,
      stateBox: getOrderController.getOrderAbnormalGoods(data.packOrderId),
      alone: widget.alone,
      goodsShowCount: 5,
      expandedGoods: (bool isExpanded) {
        getOrderController.setNewOrderExpanded(data.orderId, isExpanded);
        data.isExpanded = getOrderController.getNewOrderExpanded(data.orderId);
      },
      transfer: () {
        getDrawerController.setOrderId(data.packOrderId);
        getDrawerController.setDrawer(DrawerType.transfer);
        widget.scaffoldKey.currentState!.openEndDrawer();
      },
      abnormal: () {
        getDrawerController.setOrderId(data.packOrderId);
        getDrawerController.setDrawer(DrawerType.abnormal);
        widget.scaffoldKey.currentState!.openEndDrawer();
      },
      confirm: () {
        if (submitButtonLoading) {
          EasyLoading.showToast('您点的太快了');
          return;
        } else {
          submitButtonLoading = true;
          Future.delayed(const Duration(milliseconds: 3000)).then((value) => submitButtonLoading = false);
          showAlertDialog(
            context,
            throttleClick: true,
            title: '确认完成？',
            content: '确定该订单已拣货完成？',
            onTap: () {
              HomePageProvider.finishPack(data.packOrderId).then((value) {
                if (value) {
                  getOrderController.deleteNewOrderExpanded(data.orderId);
                  getOrderController.onRefresh();
                }
              });
            },
          );
        }
      },
      picking: () {
        getOrderController.setDefaultGoodsAbnormal(order: data, goods: data.goods);
        showDialog(context: context, builder: (context) => OrderDetailPage(order: data));
      },
      rightBoxColor: widget.deliveryType == 1 ? XSTColors.shop : XSTColors.primary2,
      rightBoxTextColor: Colors.white,
    );
  }
}
