import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/pages/order/widgets.dart';
import 'package:packing/provider/home_provider.dart';
import 'package:tdesign_flutter/tdesign_flutter.dart';

class SelectPackerPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(int packerId, String packerName) onReturn;

  const SelectPackerPage({super.key, required this.scaffoldKey, required this.onReturn});

  @override
  State<SelectPackerPage> createState() => _SelectPackerPageState();
}

class _SelectPackerPageState extends State<SelectPackerPage> {
  final getPackerController = Get.put(GetXPackerController());
  bool submitButtonLoading = false;

  List<TDRadio> tdRadios = [];
  List<Radio> radios = [];

  @override
  void initState() {
    getData();
    super.initState();
  }

  void getData() {
    getPackerController.setLoading(true);
    HomePageProvider.getPackerList().then((res) {
      getPackerController.setLoading(false);
      if (res.status == 200) {
        if (res.data.isEmpty) {
          EasyLoading.showError('当前无其它在线拣货员');
          return;
        }
        getPackerController.renew(res.data, res.data[0].packerId, res.data[0].realName);
      } else {
        getPackerController.setLoading(false);
        EasyLoading.showError(res.msg);
        return;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: XSTColors.background,
      appBar: AppBar(
        title: const Text('选择转单的拣货员', style: TextStyle(color: Colors.white, fontSize: 18)),
        centerTitle: true,
        toolbarHeight: 35,
        backgroundColor: Colors.red[400],
        leading: IconButton(
          onPressed: () => widget.scaffoldKey.currentState!.closeEndDrawer(),
          icon: const Icon(
            Icons.close,
            size: 25,
            color: Colors.white,
          ),
        ),
      ),
      body: _bodyWidget(),
    );
  }

  Widget _bodyWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(child: _showOnlinePacker()),
        Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  surfaceTintColor: Colors.white,
                  foregroundColor: Colors.black,
                ),
                onPressed: () {
                  widget.scaffoldKey.currentState!.closeEndDrawer();
                },
                child: const SizedBox(
                  width: 60,
                  child: Text(
                    '取消',
                    style: TextStyle(fontSize: 14),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  surfaceTintColor: Colors.red,
                  foregroundColor: Colors.white,
                ),
                onPressed: () {
                  if (submitButtonLoading) {
                    EasyLoading.showToast('您点得太快了');
                    return;
                  } else {
                    submitButtonLoading = true;
                    Future.delayed(const Duration(milliseconds: 2000)).then((value) => submitButtonLoading = false);
                    int userIndex = getPackerController.packerDataList.indexWhere((e) => e.packerId == getPackerController.packerUserId);
                    if (userIndex > -1) getPackerController.setName(getPackerController.packerDataList[userIndex].realName);
                    showAlertDialog(context, title: '确定转单？', content: '确定把订单转给${getPackerController.packerUserName}？', onTap: () {
                      widget.onReturn(getPackerController.packerUserId, getPackerController.packerUserName);
                      widget.scaffoldKey.currentState!.closeEndDrawer();
                    });
                  }
                },
                child: const SizedBox(
                  width: 60,
                  child: Text(
                    '确认',
                    style: TextStyle(fontSize: 14),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _showOnlinePacker() {
    return Container(
      alignment: Alignment.topCenter,
      height: 500,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                  bottom: BorderSide(
                    width: 0.5,
                    color: HexColor('#EFEFEF'),
                  ),
                )),
            child: const Text('请选择拣货员', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
          ),
          Expanded(
            child: Scrollbar(
              child: GetBuilder<GetXPackerController>(
                builder: (data) {
                  if (data.loading) {
                    return Center(
                      child: SizedBox(
                        width: 30,
                        height: 30,
                        child: CircularProgressIndicator(
                          color: Colors.red[200],
                          strokeWidth: 5,
                          strokeCap: StrokeCap.square,
                        ),
                      ),
                    );
                  } else {
                    return Column(
                      children: getPackerController.packerDataList
                          .map((e) => RadioListTile(
                                dense: true,
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                contentPadding: const EdgeInsets.symmetric(horizontal: 15),
                                value: e.packerId,
                                activeColor: Colors.red,
                                tileColor: Colors.white,
                                selectedTileColor: Colors.grey[300],
                                groupValue: data.packerUserId,
                                selected: e.packerId == data.packerUserId,
                                title: Text(e.realName, style: const TextStyle(fontSize: 15)),
                                secondary: SizedBox(
                                  width: 30,
                                  height: 30,
                                  child: e.avatar.isNotEmpty ? CircleAvatar(
                                    backgroundImage: NetworkImage(e.avatar),
                                  ) : const CircleAvatar(
                                    backgroundImage: AssetImage('assets/images/avatar.png'),
                                  ),
                                ),
                                onChanged: (int? id) => getPackerController.setId(id!),
                              ))
                          .toList(),
                    );
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
