import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:packing/libs/models/order.dart';
import 'package:packing/libs/models/result_base.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/pages/order/widgets.dart';
import 'package:packing/provider/home_provider.dart';
import 'package:packing/libs/utils/constants.dart';

import '../widgets/other.dart';

class TransferOrderPage extends StatefulWidget {
  final bool online;

  const TransferOrderPage({
    super.key,
    required this.online,
  });

  @override
  State<TransferOrderPage> createState() => TransferOrderState();
}

class TransferOrderState extends State<TransferOrderPage> {
  final GetXOrderController getOrderController = Get.find();
  final GetSettingController getSettingController = Get.find();
  late ScrollController scrollController = ScrollController();
  bool submitButtonLoading = false;

  // 下拉刷新方法
  Future<void> _handleRefresh() async {
      await Future.delayed(Duration(milliseconds: widget.online ? 500 : 0)).then((value) async {
        refreshData();
      });
  }

  void refreshData() async {
    if (widget.online) {
      getOrderController.setLoading(true);
      await getOrderController.getTransferOrder();
      getOrderController.setLoading(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      color: Colors.redAccent[200],
      onRefresh: _handleRefresh,
      child: GetBuilder<GetXOrderController>(
        builder: (data) => _createListView(context, data.loading.value, data.transferData.value),
      ),
    );
  }

  Widget _createListView(BuildContext context, bool isLoading, ResultList<OrderModel> data) {
    List<OrderModel> order = data.data;
    if (order.isNotEmpty && widget.online) {
      if (!GetPlatform.isAndroid) {
        return SingleChildScrollView(
          controller: scrollController,
          child: windowsTableViewOrderWidget(
            order,
            type: OrderType.transfer,
            packerId: getSettingController.login.packerId,
            cancel: (data) => cancel(data),
            refuse: (data) => refuse(data),
            agree: (data) => agree(data),
          ),
        );
      } else {
        return ListView.builder(
          physics: const AlwaysScrollableScrollPhysics(),
          padding: const EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 65),
          itemBuilder: (BuildContext context, int index) => _itemBuilder(context, index, order[index]),
          itemCount: order.length,
          controller: scrollController,
        );
      }
    } else {
      if (isLoading && widget.online) {
        return buildLoading();
      } else {
        double sizeHeight = Get.height;
        return ListView(
          physics: const AlwaysScrollableScrollPhysics(),
          children: [
            SizedBox(
              height: (sizeHeight / 2) + (sizeHeight / 5),
              child: orderClosed(open: widget.online, openText: '转单数据为空', closedText: '离线状态中，无法查看最新的转单数据'),
            )
          ],
        );
      }
    }
  }

  Widget _itemBuilder(BuildContext context, int index, OrderModel order) {
    OrderModel data = order;
    data.isExpanded = getOrderController.getNewOrderExpanded(data.orderId);
    return widgetTransferOrderList(
      data,
      packerId: getSettingController.login.packerId,
      goodsShowCount: 5,
      expandedGoods: (bool isExpanded) {
        getOrderController.setNewOrderExpanded(data.orderId, isExpanded);
        data.isExpanded = getOrderController.getNewOrderExpanded(data.orderId);
      },
      cancel: () => cancel(data),
      refuse: () => refuse(data),
      agree: () => agree(data),
    );
  }

  void cancel(OrderModel data) {
    showAlertDialog(
      context,
      title: '是否取消？',
      content: '取消该转单拣货单？',
      onTap: () {
        if (submitButtonLoading) {
          EasyLoading.showToast('您点得太快了');
          return;
        } else {
          submitButtonLoading = true;
          Future.delayed(const Duration(milliseconds: 2000)).then((value) => submitButtonLoading = false);
          HomePageProvider.closeTransferOrder(data.packOrderTransferId).then((res) {
            if (res.status == 200) {
              getOrderController.onRefresh(old: false);
              showTopMessage('操作成功', '该拣货单转单已成功取消', type: HandleType.success);
            }
          });
        }
      },
    );
  }

  void refuse(OrderModel data) {
    showAlertDialog(
      context,
      title: '是否拒绝？',
      content: '拒绝该转单拣货单？',
      confirmText: '拒绝',
      onTap: () {
        if (submitButtonLoading) {
          EasyLoading.showToast('您点得太快了');
          return;
        } else {
          submitButtonLoading = true;
          Future.delayed(const Duration(milliseconds: 2000)).then((value) => submitButtonLoading = false);
          HomePageProvider.refuseTransferOrder(data.packOrderTransferId).then((res) {
            if (res.status == 200) {
              getOrderController.onRefresh(old: false);
              showTopMessage('操作成功', '您已拒绝接受该拣货单');
            }
          });
        }
      },
    );
  }

  void agree(OrderModel data) {
    showAlertDialog(
      context,
      title: '是否接受？',
      content: '接受该转单拣货单？',
      confirmText: '同意',
      onTap: () {
        if (submitButtonLoading) {
          EasyLoading.showToast('您点得太快了');
          return;
        } else {
          submitButtonLoading = true;
          Future.delayed(const Duration(milliseconds: 2000)).then((value) => submitButtonLoading = false);
          HomePageProvider.receiveTransferOrder(data.packOrderTransferId).then((res) {
            if (res.status == 200) {
              getOrderController.onRefresh();
              showTopMessage('操作成功', '您已同意接受该拣货单');
            }
          });
        }
      },
    );
  }
}
