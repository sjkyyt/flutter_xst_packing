import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:packing/libs/components/easy_tags/easy_tags.dart';
import 'package:packing/libs/components/sjk_datetime_picker/sjk_datetime_picker.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/libs/utils/tools.dart';

class SearchOldPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function(String startTime, String endTime) onReturn;

  const SearchOldPage({
    super.key,
    required this.scaffoldKey,
    required this.onReturn,
  });

  @override
  State<SearchOldPage> createState() => _SearchOldPageState();
}

class _SearchOldPageState extends State<SearchOldPage> {
  final GetXOrderController orderController = Get.find();
  final DateTime now = DateTime.now();
  String startTime = '';
  String endTime = '';
  late String selectedTime;

  void setTime(String tag) {
    int year = 2024;
    int month = 1;
    int day = 1;
    switch (tag) {
      case '一周内':
        DateTime date = now.add(const Duration(days: -7));
        if (date.year >= 2024) {
          year = date.year;
          month = date.month;
          day = date.day;
        }
        startTime = '$year-${month.toString().padLeft(2, '0')}-${day.toString().padLeft(2, '0')} 00:00:00';
        endTime = '${now.year}-${now.month.toString().padLeft(2, '0')}-${now.day.toString().padLeft(2, '0')} 23:59:59';
        break;
      case '这个月':
        startTime = '${now.year}-${now.month.toString().padLeft(2, '0')}-01 00:00:00';
        endTime = '${now.year}-${now.month.toString().padLeft(2, '0')}-${now.day.toString().padLeft(2, '0')} 23:59:59';
        break;
      case '1个月内':
        DateTime date = now.add(const Duration(days: -30));
        if (date.year >= 2024) {
          year = date.year;
          month = date.month;
          day = date.day;
        }
        startTime = '$year-${month.toString().padLeft(2, '0')}-${day.toString().padLeft(2, '0')} 00:00:00';
        endTime = '${now.year}-${now.month.toString().padLeft(2, '0')}-${now.day.toString().padLeft(2, '0')} 23:59:59';
        break;
      case '3个月内':
        DateTime date = now.add(const Duration(days: -90));
        if (date.year >= 2024) {
          year = date.year;
          month = date.month;
          day = date.day;
        }
        startTime = '$year-${month.toString().padLeft(2, '0')}-${day.toString().padLeft(2, '0')} 00:00:00';
        endTime = '${now.year}-${now.month.toString().padLeft(2, '0')}-${now.day.toString().padLeft(2, '0')} 23:59:59';
        break;
      case '6个月内':
        DateTime date = now.add(const Duration(days: -180));
        if (date.year >= 2024) {
          year = date.year;
          month = date.month;
          day = date.day;
        }
        startTime = '$year-${month.toString().padLeft(2, '0')}-${day.toString().padLeft(2, '0')} 00:00:00';
        endTime = '${now.year}-${now.month.toString().padLeft(2, '0')}-${now.day.toString().padLeft(2, '0')} 23:59:59';
        break;
      case '今年内':
        startTime = '${now.year}-01-01 00:00:00';
        endTime = '${now.year}-${now.month.toString().padLeft(2, '0')}-${now.day.toString().padLeft(2, '0')} 23:59:59';
        break;
      case '':
        startTime = '';
        endTime = '';
        break;
      default:
        String tags = tag.replaceAll('年', '');
        startTime = '$tags-01-01 00:00:00';
        endTime = '$tags-12-31 23:59:59';
        break;
    }
    orderController.setStartTime(startTime);
    orderController.setEndTime(endTime);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: XSTColors.background,
      appBar: AppBar(
        title: const Text('历史订单筛选', style: TextStyle(color: Colors.white, fontSize: 18)),
        centerTitle: true,
        toolbarHeight: 35,
        backgroundColor: Colors.red[400],
        leading: IconButton(
          onPressed: () {
            widget.scaffoldKey.currentState!.closeEndDrawer();
          },
          icon: const Icon(
            Icons.close,
            size: 25,
            color: Colors.white,
          ),
        ),
      ),
      body: _bodyWidget(),
    );
  }

  Widget _bodyWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: _yearListWidget(onTap: (tag) => setTime(tag)),
        ),
        _widgetTableCard(children: [
          GetBuilder<GetXOrderController>(builder: (_) {
            return _widgetTableCardRow(
              title: '开始时间',
              subTitle: _.startTime,
              line: true,
              onClick: () async {
                List<DateTime>? dateTimeList = await _showDateRangePicker(defaultView: DefaultView.start);
                if (dateTimeList != null) {
                  startTime = Tools.formatDateTime(dateTimeList[0]);
                  endTime = Tools.formatDateTime(dateTimeList[1]);
                  orderController.setStartTime(startTime);
                  orderController.setEndTime(endTime);
                }
              },
            );
          }),
          GetBuilder<GetXOrderController>(builder: (_) {
            return _widgetTableCardRow(
              title: '结束时间',
              subTitle: _.endTime,
              onClick: () async {
                List<DateTime>? dateTimeList = await _showDateRangePicker(defaultView: DefaultView.end);
                if (dateTimeList != null) {
                  startTime = Tools.formatDateTime(dateTimeList[0]);
                  endTime = Tools.formatDateTime(dateTimeList[1]);
                  orderController.setStartTime(startTime);
                  orderController.setEndTime(endTime);
                }
              },
            );
          }),
        ]),
        Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  surfaceTintColor: Colors.white,
                  foregroundColor: Colors.black,
                ),
                onPressed: () {
                  startTime = '';
                  endTime = '';
                  orderController.setSelectedTime('');
                  orderController.setStartTime(startTime);
                  orderController.setEndTime(endTime);
                  widget.onReturn(startTime, endTime);
                  widget.scaffoldKey.currentState!.closeEndDrawer();
                },
                child: const SizedBox(
                  width: 60,
                  child: Text('重置', style: TextStyle(fontSize: 14), textAlign: TextAlign.center),
                ),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red[800],
                  foregroundColor: Colors.white,
                ),
                onPressed: () {
                  if (orderController.startTime.isEmpty || orderController.endTime.isEmpty) {
                    EasyLoading.showError('请选择开始和结束时间', duration: const Duration(milliseconds: 1500));
                    return;
                  }
                  widget.onReturn(startTime, endTime);
                  widget.scaffoldKey.currentState!.closeEndDrawer();
                },
                child: const SizedBox(
                  width: 60,
                  child: Text(
                    '确认',
                    style: TextStyle(fontSize: 14),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _yearListWidget({required Function(String tag) onTap}) {
    List<String> yearList = [];
    for (var i = 2024; i <= now.year; i++) {
      String txt = i == now.year ? '今年内' : '$i年';
      yearList.add(txt);
    }
    List<String> timeList = ['一周内', '1个月内', '这个月', '3个月内', '6个月内', ...yearList.reversed];

    return GetBuilder<GetXOrderController>(builder: (_) {
      return Container(
        color: Colors.white,
        padding: const EdgeInsets.only(top: 5, bottom: 5, left: 15, right: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 35,
              child: Row(
                children: [
                  const Text('按时间', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
                  const SizedBox(width: 10),
                  Text(_.selectedTime, style: const TextStyle(fontSize: 14, color: Colors.redAccent)),
                ],
              ),
            ),
            SizedBox(
              // height: 200,
              child: SingleChildScrollView(
                child: EasyTags(
                  content: timeList,
                  selectedTag: _.selectedTime,
                  wrapSpacing: 5,
                  wrapRunSpacing: 5,
                  onTagPress: (tag) {
                    orderController.setSelectedTime(tag);
                    onTap(tag);
                  },
                  tagContainerMargin: const EdgeInsets.only(bottom: 5),
                  tagContainerPadding: const EdgeInsets.only(top: 6, bottom: 6, left: 15, right: 15),
                  tagTextStyle: const TextStyle(color: Colors.black),
                  selectedTextStyle: TextStyle(color: XSTColors.shop),
                  // tagWidth: 85,
                  // tagIcon: const Icon(Icons.clear, size: 12),
                  tagContainerDecoration: BoxDecoration(
                    color: HexColor('#f2f2f2'),
                    border: Border.all(color: HexColor('#f2f2f2')),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                  selectedContainerDecoration: BoxDecoration(
                    color: XSTColors.danger9,
                    border: Border.all(color: XSTColors.danger3),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(20),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    });
  }

  Widget _widgetTableCard({required List<Widget> children}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 5),
          decoration: const BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                spreadRadius: 0,
                blurRadius: 1,
                offset: Offset(0, 0.5),
              ),
            ],
          ),
          child: Column(
            children: children,
          ),
        ),
      ],
    );
  }

  Widget _widgetTableCardRow({
    required String title,
    String subTitle = '',
    bool line = false,
    required void Function() onClick,
  }) {
    return InkWell(
      onTap: () {
        onClick();
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(width: 10),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(top: 12, bottom: 12),
              decoration: BoxDecoration(
                border: line
                    ? Border(
                        bottom: BorderSide(
                          width: 0.5,
                          color: HexColor('#EEEEEE'),
                        ),
                      )
                    : const Border(),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        SizedBox(width: 110, child: Text(title, style: const TextStyle(fontSize: 14))),
                        Expanded(child: Text(subTitle, style: TextStyle(color: XSTColors.info, fontSize: 13))),
                      ],
                    ),
                  ),
                  Icon(Icons.arrow_forward_ios, color: XSTColors.info, size: 15),
                  const SizedBox(width: 15)
                ],
              ),
            ),
          ),
          const SizedBox(width: 10),
        ],
      ),
    );
  }

  Future<List<DateTime>?> _showDateRangePicker({DefaultView defaultView = DefaultView.start}) async {
    DateTime now = DateTime.now();
    DateTime? startInitialDate = DateTime(now.year, now.month, now.day);
    DateTime? endInitialDate = DateTime(now.year, now.month, now.day, 23, 59, 59);
    if (orderController.startTime.isNotEmpty && orderController.endTime.isNotEmpty) {
      DateTime start = DateTime.parse(orderController.startTime);
      startInitialDate = DateTime(start.year, start.month, start.day, start.hour, start.minute, start.second);
      DateTime end = DateTime.parse(orderController.endTime);
      endInitialDate = DateTime(end.year, end.month, end.day, end.hour, end.minute, end.second);
    }
    return await showSJKDateTimeRangePicker(
      context: context,
      defaultView: defaultView,
      startTimeText: '开始时间',
      endTimeText: '结束时间',
      startInitialDate: startInitialDate,
      // startFirstDate: DateTime(2024).subtract(const Duration(days: 3652)),
      startFirstDate: DateTime(2024),
      startLastDate: DateTime(now.year, now.month + 1, 0, 23, 59, 59),
      endInitialDate: endInitialDate,
      endFirstDate: DateTime(2024),
      endLastDate: DateTime(now.year, now.month + 1, 0, 23, 59, 59),
      timeItemHeight: 40,
      isForce2Digits: true,
      is24HourMode: true,
      isShowSeconds: true,
      minutesInterval: 1,
      secondsInterval: 1,
      borderRadius: const BorderRadius.all(Radius.circular(16)),
      constraints: const BoxConstraints(
        maxWidth: 350,
        maxHeight: 600,
      ),
      transitionBuilder: (context, anim1, anim2, child) {
        return FadeTransition(
          opacity: anim1.drive(
            Tween(
              begin: 0,
              end: 1,
            ),
          ),
          child: child,
        );
      },
      transitionDuration: const Duration(milliseconds: 200),
      barrierDismissible: true,
      theme: ThemeData.light().copyWith(
        colorScheme: const ColorScheme.light(
          primary: Colors.red, // 主色
          surface: Colors.black, // 顶部Tabbar未选中下划线
          surfaceTint: Colors.white, //背景颜色
        ),
        // buttonTheme: const ButtonThemeData(textTheme: ButtonTextTheme.primary),
      ),
    );
  }
}
