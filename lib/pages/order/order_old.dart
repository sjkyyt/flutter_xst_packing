import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:packing/libs/models/order.dart';
import 'package:packing/libs/models/result_base.dart';
import 'package:packing/libs/utils/getx_controller.dart';
import 'package:packing/pages/order/widgets.dart';
import 'package:pager/pager.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/tools.dart';
import 'package:packing/provider/home_provider.dart';

class OrderOldPage extends StatefulWidget {
  const OrderOldPage({super.key});

  @override
  State<OrderOldPage> createState() => _OrderOldPageState();
}

class _OrderOldPageState extends State<OrderOldPage> {
  final GetXOrderController getOrderController = Get.find();
  late ScrollController scrollController;
  List<OrderModel> dataList = [];
  int currentPage = 1;
  int pageCount = 1;

  @override
  void initState() {
    currentPage = getOrderController.oldData.value.data.currentPage;
    pageCount = getOrderController.oldData.value.data.totalPage;

    // 监听ListView是否滚动到底部
    scrollController = ScrollController()
      ..addListener(() {
        if (GetPlatform.isAndroid) {
          if (scrollController.position.pixels >= scrollController.position.maxScrollExtent) {
            _loadMore();
          }
        }
      });
    super.initState();
  }

  // 新订单上拉加载
  Future<void> _loadMore() async {
    if (!getOrderController.moreLoading.value) {
      if (getOrderController.oldData.value.data.currentPage < getOrderController.oldData.value.data.totalPage) {
        int pages = getOrderController.oldData.value.data.currentPage + 1;
        getOrderController.setMoreLoading(true);
        await getOrderController.getOldOrder(pages: pages, plat: Plat.android.name, startTime: getOrderController.startTime, endTime: getOrderController.endTime);
        getOrderController.setMoreLoading(false);
      }
    }
  }

  Future<void> pageTo({int pages = 1, bool isAndroid = true}) async {
    if (isAndroid) {
      await Future.delayed(const Duration(milliseconds: 500)).then((value) async {
        getOrderController.setLoading(true);
        await getOrderController.getOldOrder(pages: pages, plat: Plat.android.name, startTime: getOrderController.startTime, endTime: getOrderController.endTime);
        getOrderController.setLoading(false);
      });
    } else {
      getOrderController.setLoading(true);
      await getOrderController.getOldOrder(pages: pages, plat: Plat.windows.name, startTime: getOrderController.startTime, endTime: getOrderController.endTime);
      getOrderController.setLoading(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (GetPlatform.isAndroid) {
      return RefreshIndicator(
        color: Colors.redAccent[200],
        onRefresh: pageTo,
        child: GetX<GetXOrderController>(
          init: getOrderController,
          builder: (data) {
            return _builderAndroidWidget(data.oldData.value, data.loading.value);
          },
        ),
      );
    } else {
      return GetX<GetXOrderController>(
        init: getOrderController,
        builder: (data) {
          if (data.loading.value) {
            return buildLoading();
          } else {
            return _builderWindowsWidget(data);
          }
        },
      );
    }
  }

  Widget _builderWindowsWidget(GetXOrderController data) {
    dataList = data.oldData.value.data.list;
    if (dataList.isNotEmpty) {
      return SingleChildScrollView(
        controller: scrollController,
        child: Column(
          children: [
            windowsTableViewOldOrderWidget(dataList),
            Pager(
              currentPage: data.oldData.value.data.currentPage,
              totalPages: data.oldData.value.data.totalPage,
              pagesView: 5,
              onPageChanged: (page) => pageTo(pages: page, isAndroid: false),
            ),
          ],
        ),
      );
    } else {
      double sizeHeight = Get.height;
      double sizeBoxHeight = (sizeHeight / 2) + (sizeHeight / 5);
      return SizedBox(height: sizeBoxHeight, child: orderClosed(open: true, openText: '历史拣货单数据为空'));
    }
  }

  Widget windowsTableViewOldOrderWidget(List<OrderModel> orderList) {
    int index = 0;
    final List<String> headerCell = ['序号', '订单号', '类型', '状态', '时间', '商品列表'];
    return Container(
      margin: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 20),
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Table(
        columnWidths: const {
          0: FixedColumnWidth(60),
          1: FixedColumnWidth(200),
          2: FixedColumnWidth(75),
          3: FixedColumnWidth(75),
          4: FixedColumnWidth(150),
        },
        border: TableBorder.all(color: HexColor('#ECEEF4'), width: 0.5),
        children: [
          TableRow(
              decoration: BoxDecoration(
                color: HexColor('#F5F7FA'),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                ),
              ),
              children: headerCell
                  .map((e) => TableCell(
                      child: SizedBox(
                          height: 40,
                          child: Center(
                            child: Text(e, style: const TextStyle(fontSize: 15)),
                          ))))
                  .toList()),
          ...orderList.map((item) {
            index++;
            Color colors = item.packOrderStatus == 1 || item.packOrderStatus == 7
                ? Colors.black
                : item.packOrderStatus == 2
                    ? XSTColors.primary
                    : item.isNormal == 1
                        ? XSTColors.success
                        : XSTColors.warning;
            return TableRow(
              decoration: BoxDecoration(
                color: index % 2 == 0 ? HexColor('#FAFAFA') : Colors.white,
              ),
              children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Center(
                    heightFactor: 1,
                    child: Text.rich(
                      TextSpan(
                        text: '#',
                        style: const TextStyle(color: Colors.grey, fontSize: 13),
                        children: [
                          TextSpan(
                            text: item.orderDayNumber.toString(),
                            style: const TextStyle(color: Colors.black, fontSize: 18),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(item.orderSn, style: TextStyle(color: Colors.grey[600], fontSize: 13)),
                        const SizedBox(width: 10),
                        InkWell(
                          child: Icon(
                            Icons.copy,
                            color: XSTColors.shop,
                            size: 20,
                            weight: 500,
                            semanticLabel: '复制订单号',
                          ),
                          onTap: () {
                            Clipboard.setData(ClipboardData(text: item.orderSn));
                            EasyLoading.showSuccess('订单号\n${item.orderSn}\n已成功复制', duration: const Duration(milliseconds: 1000));
                          },
                        )
                      ],
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Center(
                      child: Text(HomePageProvider.setOrderDeliveryType(item.deliveryType, item.packTransferStatus), style: const TextStyle(fontSize: 13)),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Center(
                      child: Column(
                        children: [
                          Text(HomePageProvider.setOrderStatus(item.packOrderStatus, item.isNormal), style: TextStyle(color: colors, fontSize: 13)),
                          if (item.endTime > 0 && item.deliveryType == 1 && item.isOvertime == 2) Text('超时', style: TextStyle(color: XSTColors.danger, fontSize: 13)),
                        ],
                      ),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(Tools.getTimeFormat(item.startTime), style: const TextStyle(fontSize: 13)),
                          if (item.endTime > 0) Text(Tools.getTimeFormat(item.endTime), style: TextStyle(color: colors, fontSize: 13))
                        ],
                      ),
                    ),
                  ),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Container(
                    // height: 100,
                    padding: const EdgeInsets.all(5),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    constraints: const BoxConstraints(
                      maxHeight: 250,
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: item.goods
                            .map(
                              (good) => Container(
                                margin: const EdgeInsets.only(top: 2.5, bottom: 2.5, right: 15),
                                child: Row(
                                  children: [
                                    CachedNetworkImage(
                                      imageUrl: good.image,
                                      width: 40,
                                      height: 40,
                                      imageBuilder: (context, imageProvider) => Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(5),
                                          image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      progressIndicatorBuilder: (context, url, downloadProgress) => Container(
                                        padding: const EdgeInsets.all(15),
                                        width: 20,
                                        height: 20,
                                        child: CircularProgressIndicator(
                                          color: Colors.grey[300],
                                          value: downloadProgress.progress,
                                          strokeWidth: 5,
                                          strokeCap: StrokeCap.round,
                                        ),
                                      ),
                                      errorWidget: (context, url, error) => Container(
                                        decoration: BoxDecoration(
                                          color: Colors.grey[200],
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        child: const Icon(
                                          Icons.error_outline_sharp,
                                          color: Colors.grey,
                                          size: 40,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 10),
                                    Expanded(
                                      child: SizedBox(width: 350, child: Text(good.goodsName, style: const TextStyle(fontSize: 13), softWrap: true)),
                                    ),
                                    const SizedBox(width: 10),
                                    SizedBox(
                                      width: 45,
                                      child: Text.rich(
                                        TextSpan(
                                          text: 'x',
                                          style: const TextStyle(fontSize: 10, color: Colors.grey),
                                          children: [
                                            TextSpan(text: good.skuBuyNum.toString(), style: const TextStyle(fontSize: 14, color: Colors.black)),
                                          ],
                                        ),
                                        textAlign: TextAlign.right,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    ),
                  ),
                ),
              ],
            );
          }),
        ],
      ),
    );
  }

  Widget _builderAndroidWidget(ResultPage<OrderModel> data, bool isLoading) {
    dataList = data.data.list;
    if (dataList.isNotEmpty) {
      return ListView.builder(
        physics: const AlwaysScrollableScrollPhysics(),
        padding: const EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 5),
        itemBuilder: (BuildContext context, int index) => _itemBuilder(context, index, dataList),
        itemCount: dataList.length + 1,
        cacheExtent: 20,
        controller: scrollController,
      );
    } else {
      if (isLoading) {
        return buildLoading();
      } else {
        double sizeHeight = Get.height;
        double sizeBoxHeight = (sizeHeight / 2) + (sizeHeight / 5);
        return ListView(
          physics: const AlwaysScrollableScrollPhysics(),
          children: [
            SizedBox(height: sizeBoxHeight, child: orderClosed(open: true, openText: '历史拣货单数据为空')),
          ],
        );
      }
    }
  }

  Widget _itemBuilder(BuildContext context, int index, List<OrderModel> order) {
    if (index < order.length) {
      OrderModel data = order[index];
      data.isExpanded = getOrderController.getOldOrderExpanded(data.orderId);
      return widgetOldOrderList(
        data,
        stateBox: getOrderController.getOrderAbnormalGoods(data.packOrderId),
        goodsShowCount: 5,
        expandedGoods: (bool isExpanded) {
          getOrderController.setOldOrderExpanded(data.orderId, isExpanded);
          data.isExpanded = getOrderController.getOldOrderExpanded(data.orderId);
        },
      );
    } else {
      // 最后一项，显示加载更多布局
      if (getOrderController.oldData.value.data.currentPage < getOrderController.oldData.value.data.totalPage) {
        return buildLoadMoreItem();
      } else {
        return buildLoadNotMoreItem();
      }
    }
  }
}
