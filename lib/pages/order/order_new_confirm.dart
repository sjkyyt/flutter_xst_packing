import 'package:flutter/material.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/pages/order/widgets.dart';
import 'package:tdesign_flutter/tdesign_flutter.dart';

class ConfirmOrderPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String mark;
  final Function(String mark) onReturn;

  const ConfirmOrderPage({super.key, required this.scaffoldKey, required this.mark, required this.onReturn});

  @override
  State<ConfirmOrderPage> createState() => _ConfirmOrderPageState();
}

class _ConfirmOrderPageState extends State<ConfirmOrderPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  late TextEditingController abnormalTextController;
  FocusNode commentFocus = FocusNode();
  late String markText, selectedText;
  List<String> conditions = [
    "无异常",
    "缺货",
    "不新鲜",
    "其它原因",
  ];

  @override
  void initState() {
    markText = widget.mark.isEmpty ? '无异常' : widget.mark;
    selectedText = conditions.contains(markText) ? markText : '其它原因';
    abnormalTextController = TextEditingController()
      ..text = markText
      ..addListener(() {
        markText = abnormalTextController.text;
        selectedText = conditions.contains(markText) ? markText : '其它原因';
        setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: XSTColors.background,
      appBar: AppBar(
        title: const Text('异常原因', style: TextStyle(color: Colors.white, fontSize: 18)),
        centerTitle: true,
        toolbarHeight: 35,
        backgroundColor: Colors.red[400],
        leading: IconButton(
          onPressed: () => widget.scaffoldKey.currentState!.closeEndDrawer(),
          icon: const Icon(Icons.close, size: 25, color: Colors.white),
        ),
      ),
      body: _bodyWidget(),
    );
  }

  Widget _bodyWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(child: _showAbnormalWidget()),
        Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              FilledButton(
                style: FilledButton.styleFrom(
                  backgroundColor: Colors.grey,
                ),
                onPressed: () {
                  widget.scaffoldKey.currentState!.closeEndDrawer();
                },
                child: const SizedBox(
                  width: 60,
                  child: Text('取消', style: TextStyle(fontSize: 14), textAlign: TextAlign.center),
                ),
              ),
              FilledButton(
                style: FilledButton.styleFrom(
                  backgroundColor: Colors.red[800],
                ),
                onPressed: () {
                  var state = formKey.currentState as FormState;
                  if (state.validate()) {
                    state.save();
                    if (markText == '无异常') {
                      widget.onReturn(markText);
                      widget.scaffoldKey.currentState!.closeEndDrawer();
                    } else {
                      showAlertDialog(context, title: '异常商品？', content: '确定该商品异常？', onTap: () {
                        widget.onReturn(markText);
                        widget.scaffoldKey.currentState!.closeEndDrawer();
                      });
                    }
                  }
                },
                child: const SizedBox(
                  width: 60,
                  child: Text('确认', style: TextStyle(fontSize: 14), textAlign: TextAlign.center),
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _showAbnormalWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          color: Colors.white,
          padding: const EdgeInsets.only(top: 15, left: 10, right: 10),
          child: Form(
            key: formKey,
            child: TextFormField(
              style: const TextStyle(fontSize: 14),
              controller: abnormalTextController,
              focusNode: commentFocus,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red, width: 1)),
                errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red, width: 1)),
                labelText: '异常原因',
                labelStyle: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold),
                hintText: '请输入异常原因',
                hintStyle: TextStyle(fontSize: 14, color: Colors.red),
                helperText: '',
                contentPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                errorStyle: TextStyle(fontSize: 12, color: Colors.red),
              ),
              cursorColor: Colors.red,
              minLines: 3,
              maxLines: 5,
              autofocus: false,
              keyboardType: TextInputType.text,
              validator: (value) {
                return value!.isNotEmpty ? null : '异常原因不能为空';
              },
            ),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: TDRadioGroup(
                selectId: selectedText,
                direction: Axis.vertical,
                contentDirection: TDContentDirection.left,
                spacing: 1,
                directionalTdRadios: _radioList(),
                onRadioGroupChange: (value) {
                  if (value == '其它原因') {
                    abnormalTextController.text = '';
                    FocusScope.of(context).requestFocus(commentFocus);
                  } else {
                    abnormalTextController.text = value!;
                    FocusScope.of(context).unfocus();
                  }
                }),
          ),
        ),
      ],
    );
  }

  List<TDRadio> _radioList() {
    List<TDRadio> radios = [];
    for (String item in conditions) {
      radios.add(_radioItem(item));
    }
    return radios;
  }

  TDRadio _radioItem(String title) {
    return TDRadio(
      id: title,
      title: title,
      radioStyle: TDRadioStyle.circle,
      showDivider: true,
    );
  }
}
