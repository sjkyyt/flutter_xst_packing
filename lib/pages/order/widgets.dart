import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:packing/libs/components/countdown/countdown.dart';
import 'package:packing/libs/components/expand_panel_list/enhance_expand_panel.dart';
import 'package:packing/libs/models/goods.dart';
import 'package:packing/libs/models/order.dart';
import 'package:packing/libs/models/order_state.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/tools.dart';
import 'package:packing/provider/home_provider.dart';

// 加载更多布局
Widget buildLoadMoreItem() {
  return Center(
    child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            width: 13,
            height: 13,
            child: CircularProgressIndicator(
              strokeWidth: 3,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
            ),
          ),
          const SizedBox(width: 10),
          Text('加载中...', style: TextStyle(fontSize: 12, color: XSTColors.info)),
        ],
      ),
    ),
  );
}

Widget buildLoading() {
  return const Center(
    child: SizedBox(
      width: 30,
      height: 30,
      child: CircularProgressIndicator(
        strokeWidth: 5,
        valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
      ),
    ),
  );
}

// 没有更多订单
Widget buildLoadNotMoreItem({String contents = '没有更多的拣货单了'}) {
  return Center(
    child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Text(contents, style: TextStyle(fontSize: 12, color: XSTColors.info)),
    ),
  );
}

// 订单关闭状态
Widget orderClosed({
  bool open = true,
  String openText = '数据为空',
  String closedText = '离线状态中，无法查看最新的数据',
}) {
  return Center(
    heightFactor: 1,
    child: Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border.all(width: 1.0, color: open ? XSTColors.info : XSTColors.danger),
        color: Colors.white,
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      ),
      height: 100,
      margin: const EdgeInsets.only(left: 20, right: 20),
      child: Text(
        open ? openText : closedText,
        style: TextStyle(color: open ? Colors.black38 : XSTColors.danger),
      ),
    ),
  );
}

// 新订单列表
Widget widgetNewOrderList(
  OrderModel data, {
  List<OrderStateItem>? stateBox,
  required bool alone,
  required Color rightBoxTextColor,
  required Color rightBoxColor,
  required int goodsShowCount,
  required Function(bool isExpanded) expandedGoods,
  required void Function() transfer,
  required void Function() abnormal,
  required void Function() confirm,
  required void Function() picking,
}) {
  return Container(
    alignment: Alignment.topLeft,
    margin: const EdgeInsets.only(bottom: 10),
    decoration: const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    ),
    child: Stack(
      fit: StackFit.loose,
      alignment: AlignmentDirectional.topStart,
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              // 列表头部
              widgetOrderHeader(data),
              Divider(
                height: 0.5,
                color: XSTColors.line,
              ),
              const SizedBox(height: 10),
              // 时间模块
              widgetNewOrderStatus(data),
              // 用户备注
              widgetOrderMark(data.userMark),
              // 订单商品
              widgetOrderGoods(data, stateBox: stateBox, goodsShowCount: goodsShowCount, expandedGoods: expandedGoods),
              // 底部按钮
              widgetOrderFooter(data, alone: alone, transfer: transfer, abnormal: abnormal, confirm: confirm, picking: picking),
            ],
          ),
        ),
        _rightBox(data, textColor: rightBoxTextColor, boxColor: rightBoxColor),
      ],
    ),
  );
}

// 新订单列表
Widget widgetNewOrderDetail(
  OrderModel data, {
  required List<OrderStateItem> stateBox,
  required Color rightBoxTextColor,
  required Color rightBoxColor,
  required void Function(GoodsModel good) onTap,
  required void Function() closed,
  required void Function() confirm,
}) {
  return Container(
    alignment: Alignment.topLeft,
    margin: const EdgeInsets.only(bottom: 10),
    decoration: const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    ),
    child: Stack(
      fit: StackFit.loose,
      alignment: AlignmentDirectional.topStart,
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              // 列表头部
              widgetOrderHeader(data),
              Divider(height: 0.5, color: XSTColors.line),
              const SizedBox(height: 10),
              // 时间模块
              widgetNewOrderStatus(data),
              // 用户备注
              widgetOrderMark(data.userMark),
              // 订单商品
              widgetOrderGoods(data, stateBox: stateBox, goodsShowCount: data.goods.length, onTap: onTap),
              // 底部按钮
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _footerButton('关闭返回', onTap: closed, background: XSTColors.info),
                  _footerButton('拣货完成', onTap: confirm),
                ],
              ),
            ],
          ),
        ),
        _rightBox(data, textColor: rightBoxTextColor, boxColor: rightBoxColor),
      ],
    ),
  );
}

// 转单列表
Widget widgetTransferOrderList(
  OrderModel data, {
  required int packerId,
  required int goodsShowCount,
  required Function(bool isExpanded) expandedGoods,
  required void Function() cancel,
  required void Function() refuse,
  required void Function() agree,
}) {
  return Container(
    alignment: Alignment.topLeft,
    margin: const EdgeInsets.only(bottom: 10),
    decoration: const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    ),
    child: Stack(
      fit: StackFit.loose,
      alignment: AlignmentDirectional.topStart,
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              // 列表头部
              widgetOrderHeader(data),
              Divider(color: XSTColors.line, height: 0.5),
              const SizedBox(height: 10),
              // 订单状态
              widgetTransferStatus(data),
              // 用户备注
              widgetOrderMark(data.userMark),
              // 订单商品
              widgetOrderGoods(data, goodsShowCount: goodsShowCount, expandedGoods: expandedGoods),
              // 转单Footer
              widgetTransferFooter(data, packerId: packerId, cancel: cancel, refuse: refuse, agree: agree),
            ],
          ),
        ),
        _rightBox(data, textColor: Colors.white, boxColor: XSTColors.warning2),
      ],
    ),
  );
}

// 历史订单列表
Widget widgetOldOrderList(
  OrderModel data, {
  List<OrderStateItem>? stateBox,
  required int goodsShowCount,
  required Function(bool isExpanded) expandedGoods,
}) {
  return Container(
    alignment: Alignment.topLeft,
    margin: const EdgeInsets.only(bottom: 10),
    decoration: const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    ),
    child: Stack(
      fit: StackFit.loose,
      alignment: AlignmentDirectional.topStart,
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              // 列表头部
              widgetOrderHeader(data),
              Divider(color: XSTColors.line, height: 0.5),
              const SizedBox(height: 10),
              // 订单状态
              widgetOrderStatus(data),
              // 用户备注
              widgetOrderMark(data.userMark),
              // 订单商品
              widgetOrderGoods(data, stateBox: stateBox, goodsShowCount: goodsShowCount, expandedGoods: expandedGoods, showBottomLine: false),
            ],
          ),
        ),
        _rightBox(data, textColor: Colors.white, boxColor: XSTColors.success),
      ],
    ),
  );
}

// 头部右侧当天订单号BOX
Widget _rightBox(
  OrderModel data, {
  Color textColor = Colors.black,
  Color boxColor = Colors.white,
}) {
  return Positioned(
    right: 0,
    top: 0,
    child: Container(
      width: 90,
      height: 45,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(5),
          bottomLeft: Radius.circular(50),
          bottomRight: Radius.circular(5),
          topRight: Radius.circular(5),
        ),
        color: boxColor,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            width: 5,
          ),
          Text.rich(
            TextSpan(
                text: '#',
                style: TextStyle(
                  color: textColor,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
                children: [
                  TextSpan(
                    text: data.orderDayNumber.toString(),
                    style: const TextStyle(fontSize: 25),
                  ),
                ]),
          ),
        ],
      ),
    ),
  );
}

// 订单商品列表
Widget widgetOrderGoods(
  OrderModel order, {
  List<OrderStateItem>? stateBox,
  required int goodsShowCount,
  Function(bool isExpanded)? expandedGoods,
  bool showBottomLine = true,
  void Function(GoodsModel good)? onTap,
}) {
  List<GoodsModel> goods = order.goods;
  List<List<GoodsModel>> goodsNotExpanded = [];
  bool onShowExpanded = false;
  int len = goods.length;
  if (goods.length > goodsShowCount) {
    onShowExpanded = true;
    len = goodsShowCount;
  }
  List<GoodsModel> goodsNot = [];
  for (int i = 0; i < len; i++) {
    goodsNot.add(goods[i]);
  }
  goodsNotExpanded.add(goodsNot);
  return StatefulBuilder(
    builder: (BuildContext context, void Function(void Function()) setState) {
      return EnhanceExpansionPanelList(
        expandedHeaderPadding: const EdgeInsets.only(top: 0, bottom: 0),
        expansionCallback: (int index, bool isExpanded) => expandedGoods!(isExpanded),
        elevation: 0,
        children: goodsNotExpanded.map<EnhanceExpansionPanel>((items) {
          return EnhanceExpansionPanel(
              isExpanded: order.isExpanded,
              backgroundColor: Colors.white,
              canTapOnHeader: false,
              arrowPosition: EnhanceExpansionPanelArrowPosition.bottom,
              arrowPadding: const EdgeInsets.all(5),
              headerBuilder: (BuildContext context, bool isExpanded) {
                List<Widget> goodsList = [];
                for (GoodsModel item in items) {
                  late OrderStateItem state;
                  if (stateBox != null && stateBox.isNotEmpty) {
                    state = stateBox.firstWhere((e) => e.skuId == item.skuId);
                    goodsList.add(onTap != null ? _actionOrderGoodsListItem(item, onTap: onTap, state: state) : _orderGoodsListItem(item, state: state));
                  } else {
                    goodsList.add(onTap != null ? _actionOrderGoodsListItem(item, onTap: onTap) : _orderGoodsListItem(item));
                  }
                }
                return Column(children: goodsList);
              },
              body: _orderExpandedGoodsList(order, goodsShowCount: goodsShowCount, stateBox: stateBox),
              footerBuilder: (BuildContext context, bool isExpanded) {
                late Widget arrowIcon;
                late String text;
                if (isExpanded) {
                  arrowIcon = Icon(Icons.keyboard_arrow_up, color: XSTColors.success);
                  text = '收缩商品列表';
                } else {
                  arrowIcon = Icon(Icons.keyboard_arrow_down, color: XSTColors.shop);
                  text = '查看更多商品';
                }
                Decoration boxDecoration = showBottomLine
                    ? BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            width: 0.5,
                            color: XSTColors.line,
                          ),
                        ),
                      )
                    : const BoxDecoration();
                Widget footer = InkWell(
                  child: Container(
                    margin: const EdgeInsets.only(bottom: 5),
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.center,
                    decoration: boxDecoration,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        arrowIcon,
                        Text(text, style: TextStyle(color: XSTColors.info, fontSize: 13)),
                        arrowIcon,
                      ],
                    ),
                  ),
                  onTap: () => expandedGoods!(!isExpanded),
                );
                return onShowExpanded ? footer : const SizedBox();
              });
        }).toList(),
      );
    },
  );
}

// 展开后的商品列表
Widget _orderExpandedGoodsList(
  OrderModel order, {
  required int goodsShowCount,
  List<OrderStateItem>? stateBox,
}) {
  List<Widget> goodsList = [];
  for (int i = goodsShowCount; i < order.goods.length; i++) {
    late OrderStateItem state;
    if (stateBox != null && stateBox.isNotEmpty) {
      state = stateBox.where((e) => e.skuId == order.goods[i].skuId).first;
      goodsList.add(_orderGoodsListItem(order.goods[i], state: state));
    } else {
      goodsList.add(_orderGoodsListItem(order.goods[i]));
    }
  }
  return Container(
    padding: const EdgeInsets.all(5),
    child: Column(
      children: goodsList,
    ),
  );
}

// 订单列表单个商品有动作
Widget _actionOrderGoodsListItem(
  GoodsModel good, {
  required void Function(GoodsModel good) onTap,
  OrderStateItem? state,
}) {
  return InkWell(
    onTap: () => onTap(good),
    child: state != null ? _orderGoodsListItem(good, state: state) : _orderGoodsListItem(good),
  );
}

// 订单列表单个商品
Widget _orderGoodsListItem(
  GoodsModel good, {
  OrderStateItem? state,
}) {
  Widget showStateWidget = const SizedBox();
  if (state != null && state.mark.isNotEmpty) {
    Color textColor = state.abnormal ? XSTColors.danger : XSTColors.success;
    showStateWidget = Text(state.mark, style: TextStyle(color: textColor, fontSize: 12));
  }
  return Container(
    margin: const EdgeInsets.only(bottom: 5),
    padding: const EdgeInsets.only(bottom: 5),
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(
          color: XSTColors.line,
          width: 0.5,
          style: BorderStyle.solid,
        ),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        cachedNetworkImage(good),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        good.goodsName,
                        maxLines: 3,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontSize: 13),
                      ),
                      Row(
                        children: [
                          Text(
                            '[${good.skuBarcode}]',
                            style: TextStyle(color: XSTColors.info, fontSize: 11),
                          ),
                          const SizedBox(width: 10),
                          InkWell(
                            child: Icon(
                              Icons.copy,
                              color: XSTColors.danger,
                              size: 14,
                              weight: 500,
                            ),
                            onTap: () {
                              Clipboard.setData(ClipboardData(text: good.skuBarcode));
                              EasyLoading.showSuccess(
                                '条码${good.skuBarcode}已成功复制',
                                duration: const Duration(milliseconds: 500),
                              );
                            },
                          )
                        ],
                      ),
                      showStateWidget,
                    ],
                  ),
                ),
              ),
              Container(
                constraints: const BoxConstraints(minWidth: 30, maxWidth: 60),
                alignment: Alignment.centerRight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    const Text(
                      'x',
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                      textHeightBehavior: TextHeightBehavior(applyHeightToLastDescent: false),
                    ),
                    Text(
                      good.skuBuyNum.toString(),
                      style: const TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w600),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget cachedNetworkImage(
  GoodsModel good, {
  double width = 65,
  double height = 65,
  double progressIndicatorWidth = 20,
  double progressIndicatorHeight = 20,
  double errorIconSize = 40,
}) {
  return CachedNetworkImage(
    imageUrl: good.image,
    width: width,
    height: height,
    imageBuilder: (context, imageProvider) => Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        image: DecorationImage(
          image: imageProvider,
          fit: BoxFit.cover,
        ),
      ),
    ),
    progressIndicatorBuilder: (context, url, downloadProgress) => Container(
      padding: const EdgeInsets.all(15),
      width: progressIndicatorWidth,
      height: progressIndicatorHeight,
      child: CircularProgressIndicator(
        color: Colors.grey[300],
        value: downloadProgress.progress,
        strokeWidth: 5,
        strokeCap: StrokeCap.round,
      ),
    ),
    errorWidget: (context, url, error) => Container(
      decoration: BoxDecoration(
        color: Colors.grey[200],
        borderRadius: BorderRadius.circular(5),
      ),
      child: Icon(
        Icons.error_outline_sharp,
        color: Colors.grey,
        size: errorIconSize,
      ),
    ),
  );
}

// 列表头部
Widget widgetOrderHeader(OrderModel order) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Text(
        order.orderSn,
        style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
      ),
      const SizedBox(width: 10, height: 30),
      InkWell(
        child: Icon(
          Icons.copy,
          color: XSTColors.shop,
          size: 20,
          weight: 500,
        ),
        onTap: () {
          Clipboard.setData(ClipboardData(text: order.orderSn));
          EasyLoading.showSuccess('订单号\n${order.orderSn}\n已成功复制', duration: const Duration(milliseconds: 500));
        },
      ),
    ],
  );
}

// 新订单状态模块
Widget widgetNewOrderStatus(OrderModel order) {
  return Container(
    alignment: Alignment.topLeft,
    child: Column(
      children: [
        showText('订单类型：', HomePageProvider.setOrderDeliveryType(order.deliveryType, order.packTransferStatus)),
        order.packTransferStatus == 2
            ? showText('转单状态：', '转单中')
            : order.packTransferStatus == 3
                ? showText('转单状态：', '转单订单')
                : const SizedBox(),
        showTime('派单时间：', order.startTime),
        if (order.deliveryType == 1) showTime('超时时间：', order.startTime + (20 * 60), countDown: true, textColor: XSTColors.shop),
      ],
    ),
  );
}

// 转单状态模块
Widget widgetTransferStatus(OrderModel order) {
  return Container(
    alignment: Alignment.topLeft,
    child: Column(
      children: [
        // showText('订单类型：', HomePageViews.setOrderDeliveryType(order.deliveryType, order.packTransferStatus)),
        showTime('派单时间：', order.startTime + (20 * 60), countDown: true, textColor: XSTColors.shop),
        showText('转单状态：', HomePageProvider.setOrderTransferStatus(order.orderTransferStatus)),
        showText('转　　出：', order.inUserRealname),
        showText('接　　收：', order.outUserRealname),
        // showText('拣货状态：', HomePageViews.setOrderStatus(order.packOrderStatus, order.isNormal), textColor: colors, bottom: 8),
      ],
    ),
  );
}

// 历史订单状态模块
Widget widgetOrderStatus(OrderModel order) {
  Color colors = order.packOrderStatus == 1 || order.packOrderStatus == 7
      ? Colors.black
      : order.packOrderStatus == 2
          ? XSTColors.danger
          : order.isNormal == 1
              ? XSTColors.success
              : XSTColors.warning;
  return Container(
    alignment: Alignment.topLeft,
    child: Column(
      children: [
        showText('订单类型：', HomePageProvider.setOrderDeliveryType(order.deliveryType, order.packTransferStatus)),
        showTime('派单时间：', order.startTime),
        if (order.endTime > 0) showTime('完成时间：', order.endTime, otherText: order.isOvertime == 2 ? '超时' : '', textColor: colors),
        showText('拣货状态：', HomePageProvider.setOrderStatus(order.packOrderStatus, order.isNormal), textColor: colors),
      ],
    ),
  );
}

Widget showText(
  String title,
  String txt, {
  Color titleColor = Colors.black,
  Color textColor = Colors.black,
  double bottom = 5.0,
}) {
  return Container(
    margin: EdgeInsets.only(bottom: bottom),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: 75,
          child: Text(title, style: TextStyle(fontSize: 12, color: titleColor)),
        ),
        Expanded(
          child: Text(txt, style: TextStyle(fontSize: 12, color: textColor)),
        ),
      ],
    ),
  );
}

Widget showTime(
  String title,
  int times, {
  bool countDown = false,
  String otherText = '',
  Color titleColor = Colors.black,
  Color textColor = Colors.black,
  double bottom = 5.0,
}) {
  bool isTimes = times >= Tools.getNowTimestamp();
  return Container(
    margin: EdgeInsets.only(bottom: bottom),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 75,
              child: Text(title, style: TextStyle(fontSize: 12, color: titleColor)),
            ),
            Text(Tools.getTimeFormat(times), style: TextStyle(fontSize: 12, color: textColor)),
          ],
        ),
        countDown
            ? Countdown(
                times: times,
                timer: !isTimes,
                style: TextStyle(color: isTimes ? XSTColors.primary : XSTColors.shop, fontSize: 12),
              )
            : otherText.isNotEmpty
                ? Text(otherText, style: TextStyle(fontSize: 12, color: XSTColors.danger))
                : const SizedBox(),
      ],
    ),
  );
}

// 用户备注
Widget widgetOrderMark(String mark) {
  return mark.isNotEmpty
      ? Container(
          decoration: BoxDecoration(
            color: XSTColors.line,
            borderRadius: const BorderRadius.all(Radius.circular(4)),
          ),
          margin: const EdgeInsets.only(top: 5, bottom: 10),
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          width: double.infinity,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('备注：', style: TextStyle(fontWeight: FontWeight.w600)),
              Expanded(
                child: Text(mark, maxLines: 3, softWrap: true, overflow: TextOverflow.ellipsis),
              ),
            ],
          ),
        )
      : Container(
          margin: const EdgeInsets.only(bottom: 5),
          child: Divider(height: 0.5, color: XSTColors.line),
        );
}

// 底部按钮
Widget widgetOrderFooter(
  OrderModel order, {
  required bool alone,
  required void Function() transfer,
  required void Function() abnormal,
  required void Function() confirm,
  required void Function() picking,
}) {
  List<Widget> footerButton = [];
  if (order.packTransferStatus != 2) {
    if (order.refundStatus == 2 && order.refundType == 2) {
      footerButton = [messageExpanded('客户申请退款中，无法进行操作')];
    } else {
      if (alone) {
        footerButton = [
          _footerButton(
            '订单转单',
            width: 100,
            padding: const EdgeInsets.symmetric(horizontal: 18),
            onTap: () => transfer(),
            background: XSTColors.primary,
          ),
          _footerButton(
            '开始拣货',
            width: 100,
            onTap: () => picking(),
            background: XSTColors.success,
          ),
        ];
      } else {
        footerButton = [
          _footerButton(
            '订单转单',
            padding: const EdgeInsets.symmetric(horizontal: 18),
            fontSize: 12,
            onTap: () => transfer(),
            background: XSTColors.primary,
          ),
          _footerButton(
            '异常确认',
            padding: const EdgeInsets.symmetric(horizontal: 18),
            fontSize: 12,
            onTap: () => abnormal(),
            background: XSTColors.warning,
          ),
          _footerButton(
            '拣货确认',
            padding: const EdgeInsets.symmetric(horizontal: 18),
            fontSize: 12,
            onTap: () => confirm(),
            background: Colors.red[800],
          ),
        ];
      }
    }
  } else {
    footerButton = [messageExpanded('订单转单状态中，无法进行操作')];
  }
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: footerButton,
  );
}

Widget _footerButton(
  String text, {
  required void Function() onTap,
  double? width,
  EdgeInsetsGeometry? padding,
  double fontSize = 14,
  Color? fontColor,
  Color? background,
}) {
  return FilledButton(
    style: FilledButton.styleFrom(
      padding: padding,
      backgroundColor: background,
    ),
    onPressed: onTap,
    child: SizedBox(
      width: width ?? 60,
      child: Text(text, style: TextStyle(fontSize: fontSize, color: fontColor), textAlign: TextAlign.center),
    ),
  );
}

Widget widgetTransferFooter(
  OrderModel data, {
  required int packerId,
  required void Function() cancel,
  required void Function() refuse,
  required void Function() agree,
}) {
  List<Widget> children = [];
  if (data.inUserId == packerId) {
    children = [
      _footerButton(
        '取消转单',
        width: 80,
        onTap: () => cancel(),
        background: XSTColors.primary,
      ),
    ];
  } else {
    children = [
      _footerButton(
        '接受转单',
        width: 80,
        onTap: () => agree(),
        background: XSTColors.success,
      ),
      _footerButton(
        '拒绝转单',
        width: 80,
        onTap: () => refuse(),
        background: XSTColors.danger,
      ),
    ];
  }
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: children,
  );
}

void showAlertDialog(
  BuildContext context, {
  bool throttleClick = false,
  bool showCancel = true,
  String cancelText = '取消',
  String confirmText = '确定',
  bool barrierDismissible = true,
  double? contentHeight,
  required String title,
  required String content,
  required void Function() onTap,
}) {
  showDialog<String>(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (BuildContext context) => AlertDialog(
      title: Text(title),
      titleTextStyle: const TextStyle(fontSize: 18, color: Colors.black),
      // contentPadding: const EdgeInsets.only(top: 15, left: 20, right: 20),
      content: SizedBox(
        height: contentHeight,
        child: SingleChildScrollView(
          child: Text(content, softWrap: true),
        ),
      ),
      surfaceTintColor: Colors.white,
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            showCancel
                ? _footerButton(
                    cancelText,
                    onTap: () => Get.back(),
                    background: XSTColors.info,
                  )
                : const SizedBox(),
            _footerButton(
              confirmText,
              onTap: () {
                onTap();
                Get.back();
              },
            ),
          ],
        ),
      ],
    ),
  );
}

Widget showCustomDialog({
  required String title,
  required String subTitle,
  required String content,
  String cancelText = '取消',
  String confirmText = '确定',
  required Function() onConfirm,
}) =>
    Dialog(
      surfaceTintColor: Colors.white,
      elevation: 5,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Container(
        constraints: const BoxConstraints(
          maxWidth: 400,
        ),
        padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: const TextStyle(fontSize: 20),
            ),
            const SizedBox(height: 20),
            Text(subTitle),
            const SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(content, softWrap: true),
            ),
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: XSTColors.info,
                  ),
                  onPressed: () => Get.back(),
                  child: SizedBox(
                    width: 60,
                    child: Text(cancelText, style: const TextStyle(color: Colors.white), textAlign: TextAlign.center),
                  ),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: XSTColors.shop,
                  ),
                  onPressed: onConfirm,
                  child: SizedBox(width: 60, child: Text(confirmText, style: const TextStyle(color: Colors.white), textAlign: TextAlign.center)),
                ),
              ],
            )
          ],
        ),
      ),
    );

Widget messageExpanded(String message, {TextStyle? textStyle}) {
  TextStyle? style = textStyle ?? TextStyle(color: XSTColors.danger2);
  return Expanded(
    child: Text(message, style: style, textAlign: TextAlign.center, softWrap: true),
  );
}

void showBottomMessage(
  BuildContext context, {
  required String message,
  HandleType type = HandleType.success,
  TextStyle? messageStyle,
  Color? backgroundColor,
  EdgeInsetsGeometry? padding,
  double? elevation,
  Duration? duration,
  ShapeBorder? shape,
  SnackBarAction? action,
  void Function()? onVisible,
  bool showCloseIcon = true,
  Color? closeIconColor,
  DismissDirection? dismissDirection,
}) {
  Color defaultColor;
  switch (type) {
    case HandleType.primary:
      defaultColor = XSTColors.primary;
      break;
    case HandleType.error:
      defaultColor = XSTColors.danger;
      break;
    case HandleType.warning:
      defaultColor = XSTColors.warning;
      break;
    case HandleType.info:
      defaultColor = XSTColors.info;
      break;
    case HandleType.success:
    default:
      defaultColor = XSTColors.success;
      break;
  }
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      backgroundColor: backgroundColor ?? defaultColor,
      duration: duration ?? const Duration(milliseconds: 2000),
      padding: padding,
      elevation: elevation ?? 3,
      showCloseIcon: showCloseIcon,
      closeIconColor: closeIconColor ?? Colors.white,
      shape: shape ??
          const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
            ),
          ),
      content: Text(message, style: messageStyle ?? const TextStyle(color: Colors.white)),
      onVisible: onVisible,
      action: action,
      dismissDirection: DismissDirection.endToStart,
    ),
  );
}

Widget windowsTableNewWidget(
  List<OrderModel> orderList, {
  required void Function(OrderModel order) transfer,
  required void Function(OrderModel order) picking,
}) {
  return windowsTableViewOrderWidget(
    orderList,
    type: OrderType.order,
    transfer: (order) => transfer(order),
    picking: (order) => picking(order),
  );
}

Widget windowsTableViewOrderWidget(
  List<OrderModel> orderList, {
  required OrderType type,
  int? packerId,
  void Function(OrderModel order)? transfer,
  void Function(OrderModel order)? picking,
  void Function(OrderModel order)? cancel,
  void Function(OrderModel order)? refuse,
  void Function(OrderModel order)? agree,
}) {
  int index = 0;
  final List<String> headerCell = ['序号', '订单号', '类型', '状态', '时间', '商品列表', '操作'];
  return Container(
    margin: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 20),
    padding: const EdgeInsets.all(10),
    decoration: const BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(5))),
    child: Table(
      columnWidths: const {
        0: FixedColumnWidth(60),
        1: FixedColumnWidth(200),
        2: FixedColumnWidth(75),
        3: FixedColumnWidth(75),
        4: FixedColumnWidth(150),
        6: FixedColumnWidth(120),
      },
      border: TableBorder.all(color: HexColor('#ECEEF4'), width: 0.5),
      children: [
        TableRow(
            decoration: BoxDecoration(
              color: HexColor('#F5F7FA'),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(5),
                topRight: Radius.circular(5),
              ),
            ),
            children: headerCell
                .map((e) => TableCell(
                    child: SizedBox(
                        height: 40,
                        child: Center(
                          child: Text(e, style: const TextStyle(fontSize: 15)),
                        ))))
                .toList()),
        ...orderList.map((item) {
          index++;
          Color colors = item.packOrderStatus == 1 || item.packOrderStatus == 7
              ? Colors.black
              : item.packOrderStatus == 2
                  ? XSTColors.primary
                  : item.isNormal == 1
                      ? XSTColors.success
                      : XSTColors.warning;
          return TableRow(
            decoration: BoxDecoration(
              color: index % 2 == 0 ? HexColor('#FAFAFA') : Colors.white,
            ),
            children: [
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Center(
                  heightFactor: 1,
                  child: Text.rich(
                    TextSpan(
                      text: '#',
                      style: const TextStyle(color: Colors.grey, fontSize: 13),
                      children: [
                        TextSpan(
                          text: item.orderDayNumber.toString(),
                          style: const TextStyle(color: Colors.black, fontSize: 18),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(item.orderSn, style: TextStyle(color: Colors.grey[600], fontSize: 13)),
                      const SizedBox(width: 10),
                      InkWell(
                        child: Icon(
                          Icons.copy,
                          color: XSTColors.shop,
                          size: 20,
                          weight: 500,
                          semanticLabel: '复制订单号',
                        ),
                        onTap: () {
                          Clipboard.setData(ClipboardData(text: item.orderSn));
                          EasyLoading.showSuccess('订单号\n${item.orderSn}\n已成功复制', duration: const Duration(milliseconds: 1000));
                        },
                      )
                    ],
                  ),
                ),
              ),
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Center(
                    child: Text(HomePageProvider.setOrderDeliveryType(item.deliveryType, item.packTransferStatus), style: const TextStyle(fontSize: 13)),
                  ),
                ),
              ),
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Center(
                    child: Column(
                      children: [
                        Text(HomePageProvider.setOrderStatus(item.packOrderStatus, item.isNormal), style: TextStyle(color: colors, fontSize: 13)),
                        if (item.endTime > 0 && item.deliveryType == 1 && item.isOvertime == 2) Text('超时', style: TextStyle(color: XSTColors.danger, fontSize: 13)),
                      ],
                    ),
                  ),
                ),
              ),
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(Tools.getTimeFormat(item.startTime), style: const TextStyle(fontSize: 13)),
                        if (item.endTime > 0) Text(Tools.getTimeFormat(item.endTime), style: TextStyle(color: colors, fontSize: 13))
                      ],
                    ),
                  ),
                ),
              ),
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Container(
                  // height: 100,
                  padding: const EdgeInsets.all(5),
                  margin: const EdgeInsets.symmetric(vertical: 5),
                  constraints: const BoxConstraints(
                    maxHeight: 250,
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: item.goods
                          .map(
                            (good) => Container(
                              margin: const EdgeInsets.only(top: 2.5, bottom: 2.5, right: 15),
                              child: Row(
                                children: [
                                  cachedNetworkImage(good, width: 40, height: 40),
                                  const SizedBox(width: 10),
                                  Expanded(
                                    child: SizedBox(width: 350, child: Text(good.goodsName, style: const TextStyle(fontSize: 13), softWrap: true)),
                                  ),
                                  const SizedBox(width: 10),
                                  SizedBox(
                                    width: 45,
                                    child: Text.rich(
                                      TextSpan(
                                        text: 'x',
                                        style: const TextStyle(fontSize: 10, color: Colors.grey),
                                        children: [
                                          TextSpan(text: good.skuBuyNum.toString(), style: const TextStyle(fontSize: 14, color: Colors.black)),
                                        ],
                                      ),
                                      textAlign: TextAlign.right,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ),
              ),
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Container(
                  constraints: const BoxConstraints(minHeight: 80, maxHeight: double.infinity),
                  child: type == OrderType.order
                      ? footerButtonNew(
                          item,
                          transfer: transfer,
                          picking: picking,
                        )
                      : footerButtonTransfer(
                          item,
                          packerId: packerId,
                          cancel: cancel,
                          refuse: refuse,
                          agree: agree,
                        ),
                ),
              ),
            ],
          );
        }),
      ],
    ),
  );
}

Widget footerButtonNew(
  OrderModel order, {
  void Function(OrderModel order)? transfer,
  void Function(OrderModel order)? picking,
}) {
  List<Widget> footerButtonList = [];
  if (order.packTransferStatus != 2) {
    if (order.refundStatus == 2 && order.refundType == 2) {
      footerButtonList = [const Text('退款中')];
    } else {
      footerButtonList = [
        _footerButton(
          '订单转单',
          padding: const EdgeInsets.symmetric(horizontal: 18),
          onTap: () => transfer!(order),
          background: XSTColors.primary,
        ),
        _footerButton(
          '开始拣货',
          padding: const EdgeInsets.symmetric(horizontal: 18),
          onTap: () => picking!(order),
          background: XSTColors.success,
        ),
      ];
    }
  } else {
    footerButtonList = [const Text('转单中')];
  }

  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: footerButtonList,
  );
}

Widget footerButtonTransfer(
  OrderModel order, {
  int? packerId,
  void Function(OrderModel order)? cancel,
  void Function(OrderModel order)? refuse,
  void Function(OrderModel order)? agree,
}) {
  List<Widget> children = [];
  if (order.inUserId == packerId) {
    children = [
      _footerButton(
        '取消转单',
        padding: const EdgeInsets.symmetric(horizontal: 18),
        onTap: () => cancel!(order),
        background: XSTColors.primary,
      ),
    ];
  } else {
    children = [
      _footerButton(
        '接受转单',
        padding: const EdgeInsets.symmetric(horizontal: 18),
        onTap: () => agree!(order),
        background: XSTColors.success,
      ),
      _footerButton(
        '拒绝转单',
        padding: const EdgeInsets.symmetric(horizontal: 18),
        onTap: () => refuse!(order),
        background: XSTColors.danger,
      ),
    ];
  }
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: children,
  );
}
