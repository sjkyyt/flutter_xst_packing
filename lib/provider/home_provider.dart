import 'dart:convert';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:packing/libs/apis/api.dart';
import 'package:packing/libs/models/order.dart';
import 'package:packing/libs/models/packer.dart';
import 'package:packing/libs/models/result_base.dart';
import 'package:packing/libs/utils/tools.dart';

class HomePageProvider {
  static Future<ResultList<OrderModel>> getOrderNew({required int type}) async {
    ResultList<OrderModel> orderData = ResultList.setNull();
    Map<String, dynamic> params = {
      'delivery_type': type,
    };
    await API.order.getPackerPackOrder(params).then((value) {
      Map res = json.decode(value.data);
      if (res['status'] == 200) {
        ResultList<OrderModel> obj = ResultList.fromJson(value.data, (json) => OrderModel.fromJson(json));
        orderData = obj;
      } else {
        EasyLoading.showError(res['msg']);
      }
    });
    return orderData;
  }

  // 确认订单
  static Future<bool> finishPack(int orderId, {int normal = 1, String mark = ''}) async {
    bool success = false;
    EasyLoading.show(status: '加载中...', maskType: EasyLoadingMaskType.black);
    Map<String, dynamic> params = {
      'pack_order_id': orderId,
      'is_normal': normal,
      'pack_mark': mark,
    };
    await API.order.finishPackOrder(params).then((value) {
      EasyLoading.dismiss();
      Map res = json.decode(value.data);
      if (res['status'] == 200) {
        success = true;
      } else {
        EasyLoading.showError(res['msg']);
      }
    });
    return success;
  }

  // 创建转单
  static Future<ResultObj> createPackTransfer(int orderId, int packerId) async {
    late ResultObj success;
    EasyLoading.show(status: '加载中...');
    Map<String, dynamic> params = {
      'pack_order_id': orderId,
      'out_user_id': packerId,
    };
    await API.order.createPackTransferOrder(params).then((value) {
      EasyLoading.dismiss();
      Map res = json.decode(value.data);
      success = ResultObj.fromJsonNull(value.data);
      if (res['status'] != 200) {
        EasyLoading.showError(res['msg']);
      }
    });
    return success;
  }

  // 取得同店在线拣货员
  static Future<ResultList<PackerModel>> getPackerList() async {
    ResultList<PackerModel> packerData = ResultList.setNull();
    await API.gateway.getPackerList({}).then((value) {
      Map res = json.decode(value.data);
      if (res['status'] == 200) {
        ResultList<PackerModel> obj = ResultList.fromJson(value.data, (json) => PackerModel.fromJson(json));
        packerData = obj;
      } else {
        EasyLoading.showError(res['msg']);
      }
    });
    return packerData;
  }

  // 获取转单列表
  static Future<ResultList<OrderModel>> getPackTransferOrder() async {
    ResultList<OrderModel> transferData = ResultList.setNull();
    await API.order.getPackTransferOrder({}).then((value) {
      Map res = json.decode(value.data);
      if (res['status'] == 200) {
        ResultList<OrderModel> obj = ResultList.fromJson(value.data, (json) => OrderModel.fromJson(json));
        transferData = obj;
      } else {
        EasyLoading.showError(res['msg']);
      }
    });
    return transferData;
  }

  // 取消转单
  static Future<ResultObj> closeTransferOrder(int packOrderTransferId) async {
    late ResultObj success;
    EasyLoading.show(status: '加载中...');
    Map<String, dynamic> params = {
      'pack_order_transfer_id': packOrderTransferId,
    };
    await API.order.closeTransferOrder(params).then((value) {
      EasyLoading.dismiss();
      Map res = json.decode(value.data);
      success = ResultObj.fromJsonNull(value.data);
      if (res['status'] != 200) {
        EasyLoading.showError(res['msg']);
      }
    });
    return success;
  }

  // 拒绝转单
  static Future<ResultObj> refuseTransferOrder(int packOrderTransferId) async {
    late ResultObj success;
    EasyLoading.show(status: '加载中...');
    Map<String, dynamic> params = {
      'pack_order_transfer_id': packOrderTransferId,
    };
    await API.order.refuseTransferOrder(params).then((value) {
      EasyLoading.dismiss();
      Map res = json.decode(value.data);
      success = ResultObj.fromJsonNull(value.data);
      if (res['status'] != 200) {
        EasyLoading.showError(res['msg']);
      }
    });
    return success;
  }

  // 同意转单
  static Future<ResultObj> receiveTransferOrder(int packOrderTransferId) async {
    late ResultObj success;
    EasyLoading.show(status: '加载中...');
    Map<String, dynamic> params = {
      'pack_order_transfer_id': packOrderTransferId,
    };
    await API.order.receiveTransferOrder(params).then((value) {
      EasyLoading.dismiss();
      Map res = json.decode(value.data);
      success = ResultObj.fromJsonNull(value.data);
      if (res['status'] != 200) {
        EasyLoading.showError(res['msg']);
      }
    });
    return success;
  }

  // 取得历史订单
  static Future<ResultPage<OrderModel>> getOrderOld(int pages, {int pageSize = 10, String startTime = '', String endTime = ''}) async {
    ResultPage<OrderModel> orderData = ResultPage.setNull();
    String timeStart = startTime.isNotEmpty ? startTime : '2024-01-01 00:00:00';
    String timeEnd = endTime.isNotEmpty ? endTime : Tools.getTimeFormat(Tools.getNowTimestamp());
    Map<String, dynamic> params = {
      'p': pages,
      'limit': pageSize,
      'start_time': timeStart,
      'end_time': timeEnd,
    };
    await API.order.getPackOrderByDay(params).then((value) {
      Map res = json.decode(value.data);
      if (res['status'] == 200) {
        ResultPage<OrderModel> obj = ResultPage.fromJson(value.data, (json) => OrderModel.fromJson(json));
        orderData = obj;
      } else {
        EasyLoading.showError(res['msg']);
      }
    });
    return orderData;
  }

  static String setOrderStatus(int packOrderStatus, [int normal = 1]) {
    List<String> arrString = ['', '未拣货', '拣货中', normal == 1 ? '正常完成' : '异常完成', '', '', '', '全退已关'];
    return arrString[packOrderStatus];
  }

  static String setOrderTransferStatus(int transferStatus) {
    List<String> arrString = ['', '转单中', '转单拒绝', '转单接受'];
    return arrString[transferStatus];
  }

  static String setOrderDeliveryType(int deliveryType, int transfer) {
    List<String> arrString = ['', '城区配送', '乡镇配送', '超市自提'];
    String str = arrString[deliveryType];
    if (transfer > 2) str += ' 转单';
    return str;
  }
}
