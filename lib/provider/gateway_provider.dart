import 'dart:convert';
import 'package:packing/libs/apis/api.dart';
import 'package:packing/libs/models/gateway.dart';
import 'package:packing/libs/models/result_base.dart';
import 'package:packing/libs/models/version.dart';

class GatewayProvider {
  static Future<ResultObj<LoginModel>> loginResult({required String account, required String password, required String registrationId}) async {
    late ResultObj<LoginModel> obj;

    Map<String, dynamic> arr = {
      'account': account,
      'password': password,
      'registration_id': registrationId,
    };
    await API.gateway.login(arr).then((value) {
      Map jsonMap = json.decode(value.data);
      if (jsonMap['status'] == 200) {
        obj = ResultObj.fromJson(value.data, (json) => LoginModel.fromJson(json));
      } else {
        String errMsg = '';
        if (jsonMap['msg'].runtimeType.toString() == 'String') {
          errMsg = jsonMap['msg'];
        } else {
          Map<String, dynamic> err = {
            'msg': jsonMap['msg']['err'],
            'file': jsonMap['msg']['file'],
          };
          errMsg = err['msg'];
        }
        LoginModel data = LoginModel();
        obj = ResultObj(
          status: jsonMap['status'] as int,
          msg: errMsg,
          data: data,
        );
      }
    });
    return obj;
  }

  static Future<ResultObj<LoginModel>> getPackerInfo() async {
    late ResultObj<LoginModel> obj;
    await API.gateway.getPackerInfo({}).then((value) {
      Map jsonMap = json.decode(value.data);
      if (jsonMap['status'] == 200) {
        obj = ResultObj.fromJson(value.data, (json) => LoginModel.fromJson(json));
      } else {
        String errMsg = '';
        if (jsonMap['msg'].runtimeType.toString() == 'String') {
          errMsg = jsonMap['msg'];
        } else {
          Map<String, dynamic> err = {
            'msg': jsonMap['msg']['err'],
            'file': jsonMap['msg']['file'],
          };
          errMsg = err['msg'];
        }
        LoginModel data = LoginModel();
        obj = ResultObj(
          status: jsonMap['status'] as int,
          msg: errMsg,
          data: data,
        );
      }
    });
    return obj;
  }

  static Future<bool> onLine() async {
    bool action = false;
    await API.gateway.onLine({}).then((value) {
      Map res = json.decode(value.data);
      if (res['status'] == 200) {
        action = true;
      }
    });
    return action;
  }

  static Future<bool> offLine() async {
    bool action = false;
    await API.gateway.offLine({}).then((value) {
      Map res = json.decode(value.data);
      if (res['status'] == 200) {
        action = true;
      }
    });
    return action;
  }

  static Future<ResultList<VersionModel>> getPackerAppVersion() async {
    late ResultList<VersionModel> obj;
    try {
      await API.gateway.getPackerAppVersion({}).then((value) {
        Map jsonMap = json.decode(value.data);
        if (jsonMap['status'] == 200) {
          obj = ResultList.fromJson(value.data, (json) => VersionModel.fromJson(json));
        } else {
          String errMsg = '';
          if (jsonMap['msg'].runtimeType.toString() == 'String') {
            errMsg = jsonMap['msg'];
          } else {
            Map<String, dynamic> err = {
              'msg': jsonMap['msg']['err'],
              'file': jsonMap['msg']['file'],
            };
            errMsg = err['msg'];
          }
          List<VersionModel> data = [];
          obj = ResultList(
            status: jsonMap['status'] as int,
            msg: errMsg,
            data: data,
          );
        }
      });
    } catch(e) {
      List<VersionModel> data = [];
      obj = ResultList(
        status: 201,
        msg: e.toString(),
        data: data,
      );
    }
    return obj;
  }
}
