import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:packing/libs/models/user_login.dart';
import 'package:packing/libs/models/order_state.dart';
import 'package:packing/libs/utils/routers.dart';
import 'package:window_manager/window_manager.dart';

import 'libs/models/gateway.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Hive.initFlutter();
  Hive.registerAdapter(UserLoginModelAdapter());
  Hive.registerAdapter(LoginModelAdapter());
  Hive.registerAdapter(OrderStateAdapter());
  Hive.registerAdapter(OrderStateItemAdapter());
  //打开盒子
  await Hive.openBox<UserLoginModel>('userBox');
  await Hive.openBox('newOrderBox');
  await Hive.openBox('oldOrderBox');
  await Hive.openBox<OrderState>('abnormalBox');
  await Hive.openBox('settingBox');
  await Hive.openBox('loginBox');

  if (GetPlatform.isWindows) {
    // 必须加上这一行。
    await windowManager.ensureInitialized();
    const Size size = Size(1250, 900);
    const title = '鲜升通拣货端 - Windows版';
    WindowOptions windowOptions = const WindowOptions(
      size: size,
      minimumSize: size,
      // maximumSize: size,
      center: true,
      backgroundColor: Colors.green,
      alwaysOnTop: false,
      skipTaskbar: false,
      title: title,
      titleBarStyle: TitleBarStyle.normal,
      windowButtonVisibility: true,
    );
    windowManager.waitUntilReadyToShow(windowOptions, () async {
      await windowManager.show();
      await windowManager.focus();
    });
  }

  runApp(const MyApp());
  configLoading();
}

void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.custom
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = Colors.white //进度条颜色
    ..backgroundColor = Colors.grey[800]
    ..indicatorColor = Colors.white // 动画颜色
    ..textColor = Colors.white // 文字颜色
    ..maskColor = Colors.black.withOpacity(0.5)
    ..userInteractions = true
    ..dismissOnTap = false;
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      localizationsDelegates: const [
        //此处
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        //此处
        Locale('zh', 'CH'),
        Locale('en', 'US'),
      ],
      locale: const Locale('zh', 'CN'),
      debugShowCheckedModeBanner: false,
      // 不显示右上角的 debug
      // 设置navigatorKey
      title: '鲜升通拣货端',
      builder: EasyLoading.init(),
      theme: ThemeData(
        fontFamily: !GetPlatform.isAndroid ? '微软雅黑' : null,
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(
          primary: Colors.red[900],
          seedColor: Colors.red,
          brightness: Brightness.light,
        ),
      ),
      initialRoute: '/',
      // defaultTransition: Transition.rightToLeftWithFade,
      // 注册路由表
      getPages: Routers.getRouters,
      // routes: Routers.routes,
    );
  }
}
