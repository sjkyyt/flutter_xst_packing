import 'package:flutter/material.dart';
import 'package:tdesign_flutter/tdesign_flutter.dart';

class Settings {
  /// API接口默认地址
  static String baseUrl = 'https://pack.tbzmy.com.cn';
  // static String baseUrl = 'https://pack.tbzmy.com';
  /// 超时间时
  static int connectTimeout = 6000;
}

enum TimeType { start, end }

enum DrawerType { transfer, abnormal, old }

enum OrderType { order, transfer, old }

enum HandleType { primary, success, error, warning, info }

enum Plat {
  web,
  android,
  ios,
  macOs,
  windows,
  fuchsia,
  linux;

  String get name {
    switch (this) {
      case Plat.android:
        return 'android';
      case Plat.ios:
        return 'ios';
      case Plat.macOs:
        return 'macOs';
      case Plat.windows:
        return 'windows';
      case Plat.fuchsia:
        return 'fuchsia';
      case Plat.linux:
        return 'linux';
      case Plat.web:
      default:
        return 'web';
    }
  }
}

enum Tabbar {
  home,
  my;

  Icon get icon {
    switch (this) {
      case Tabbar.home:
        return const Icon(TDIcons.view_module);
      case Tabbar.my:
        return const Icon(Icons.account_circle_sharp);
    }
  }

  String get label {
    switch (this) {
      case Tabbar.home:
        return '拣货单';
      case Tabbar.my:
        return '我的';
    }
  }
}

class XSTColors {
  static Color primary = const Color.fromRGBO(94, 154, 248, 1);
  static Color primary2 = const Color.fromRGBO(75, 123, 199, 1);
  static Color primary3 = const Color.fromRGBO(137, 184, 250, 1);
  static Color primary5 = const Color.fromRGBO(170, 205, 251, 1);
  static Color primary7 = const Color.fromRGBO(204, 225, 253, 1);
  static Color primary8 = const Color.fromRGBO(221, 235, 253, 1);
  static Color primary9 = const Color.fromRGBO(238, 245, 254, 1);
  static Color success = const Color.fromRGBO(121, 193, 82, 1);
  static Color success2 = const Color.fromRGBO(96, 155, 65, 1);
  static Color success3 = const Color.fromRGBO(159, 212, 129, 1);
  static Color success5 = const Color.fromRGBO(186, 225, 164, 1);
  static Color success7 = const Color.fromRGBO(213, 237, 200, 1);
  static Color success8 = const Color.fromRGBO(227, 243, 218, 1);
  static Color success9 = const Color.fromRGBO(241, 249, 236, 1);
  static Color info = const Color.fromRGBO(145, 147, 152, 1);
  static Color info2 = const Color.fromRGBO(116, 118, 122, 1);
  static Color info3 = const Color.fromRGBO(177, 179, 184, 1);
  static Color info5 = const Color.fromRGBO(200, 201, 204, 1);
  static Color info7 = const Color.fromRGBO(222, 223, 224, 1);
  static Color info8 = const Color.fromRGBO(233, 233, 235, 1);
  static Color info9 = const Color.fromRGBO(244, 244, 245, 1);
  static Color warning = const Color.fromRGBO(220, 165, 77, 1);
  static Color warning2 = const Color.fromRGBO(176, 133, 61, 1);
  static Color warning3 = const Color.fromRGBO(230, 192, 128, 1);
  static Color warning5 = const Color.fromRGBO(237, 211, 163, 1);
  static Color warning7 = const Color.fromRGBO(244, 228, 200, 1);
  static Color warning8 = const Color.fromRGBO(247, 237, 218, 1);
  static Color warning9 = const Color.fromRGBO(252, 246, 237, 1);
  static Color danger = const Color.fromRGBO(230, 115, 109, 1);
  static Color danger2 = const Color.fromRGBO(184, 92, 87, 1);
  static Color danger3 = const Color.fromRGBO(236, 156, 152, 1);
  static Color danger5 = const Color.fromRGBO(241, 184, 182, 1);
  static Color danger7 = const Color.fromRGBO(246, 212, 211, 1);
  static Color danger8 = const Color.fromRGBO(249, 227, 226, 1);
  static Color danger9 = const Color.fromRGBO(252, 240, 240, 1);
  static Color shop = HexColor('#dc201f');
  static Color background = HexColor('#f2f2f2');
  static Color line = HexColor('#f1f1f1');
  static Color defaultColor = HexColor('#dedede');
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
