import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:packing/app.dart';
import 'package:packing/pages/gateway/login.dart';
import 'package:packing/pages/index.dart';
import 'package:packing/pages/setting/index.dart';
import 'package:packing/pages/setting/sound.dart';
import 'package:packing/pages/setting/alone.dart';

class Routers {
  static List<GetPage<dynamic>> getRouters = [
    GetPage(name: "/", page: () => const Apps()),
    GetPage(name: "/home", page: () => const IndexPage()),
    GetPage(name: "/login", page: () => const LoginPage()),
    GetPage(name: "/setting", page: () => const SettingsPage()),
    GetPage(name: "/setting/sound", page: () => SoundPage()),
    GetPage(name: "/setting/alone", page: () => const SettingAlonePage()),
  ];
}