import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:packing/libs/components/app_update/app_update.dart';
import 'package:packing/libs/models/version.dart';
import 'package:packing/provider/gateway_provider.dart';

import 'constants.dart';

class Version {
  /// newVersion 新版本号
  /// old 老版本号
  static bool isUpdateVersion(String newVersion, String old) {
    if (newVersion.isEmpty || old.isEmpty) {
      return false;
    }
    int newVersionInt, oldVersion;
    var newList = newVersion.split('.');
    var oldList = old.split('.');
    if (newList.isEmpty || oldList.isEmpty) {
      return false;
    }
    for (int i = 0; i < newList.length; i++) {
      newVersionInt = int.parse(newList[i]);
      oldVersion = int.parse(oldList[i]);
      if (newVersionInt > oldVersion) {
        return true;
      } else if (newVersionInt < oldVersion) {
        return false;
      }
    }
    return false;
  }

  static void checkVersion(
    BuildContext context, {
    required String plat,
    String? title,
    bool autoUpdate = false,
    required String version,
    required void Function() skipExecution,
    void Function(bool isNew, String backText, int plat)? updateBack,
  }) async {
    try {
      if (!autoUpdate) {
        EasyLoading.show(status: '加载中...', maskType: EasyLoadingMaskType.black);
      }
      GatewayProvider.getPackerAppVersion().then((res) {
        if (EasyLoading.isShow) {
          EasyLoading.dismiss();
        }
        if (res.status == 200) {
          bool isNew = false;
          bool forceUpdated = false;
          late VersionModel ver;
          if (res.data.isNotEmpty) {
            ver = res.data[0];
            isNew = isUpdateVersion(ver.version, version);
            forceUpdated = ver.isForce == 1 ? true : !autoUpdate;
          }
          if (isNew && forceUpdated && ((ver.plat == 1 && plat == Plat.android.name) || (ver.plat == 4 && plat == Plat.windows.name))) {
            if (forceUpdated) {
              updateBack!(true, '检测到新版本，正在下载中...', ver.plat);
            } else {
              updateBack!(true, '检测到新版本，非强制升级，跳过...', ver.plat);
            }
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) => AppUpdatePage(
                plat: plat,
                version: ver,
                oldVersion: version,
                autoUpdate: autoUpdate,
                onClosed: () => Get.back(),
              ),
            );
          } else {
            updateBack!(false, '没有新版本，页面跳转中...', 1);
            skipExecution();
          }
        } else {
          EasyLoading.showError(res.msg);
        }
      });
    } catch (e) {
      updateBack!(false, '出现未知错误', 1);
      skipExecution();
    }
  }
}
