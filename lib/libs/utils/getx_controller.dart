import 'dart:async';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:packing/libs/models/gateway.dart';
import 'package:packing/libs/models/goods.dart';
import 'package:packing/libs/models/order.dart';
import 'package:packing/libs/models/order_state.dart';
import 'package:packing/libs/models/packer.dart';
import 'package:packing/libs/models/result_base.dart';
import 'package:packing/libs/models/user_login.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/tools.dart';
import 'package:packing/provider/home_provider.dart';
import 'package:packing/provider/gateway_provider.dart';

class GetXDrawerController extends GetxController {
  DrawerType selectedDrawer = DrawerType.old;
  int orderId = 0;
  int packOrderId = 0;
  int skuId = 0;
  String mark = '';

  void setDrawer(DrawerType drawerType) {
    selectedDrawer = drawerType;
    update(['drawer']);
  }

  void setOrderId(int id) {
    orderId = id;
    update(['drawer']);
  }

  void setSkuId(int packOrderId, int skuId, String mark) {
    this.packOrderId = packOrderId;
    this.skuId = skuId;
    this.mark = mark;
    update(['detail']);
  }
}

class GetStateController extends GetxController {
  final userBox = Hive.box<UserLoginModel>('userBox');

  void userAdd(UserLoginModel user) {
    Iterable keys = userBox.keys.where((key) => userBox.get(key)?.account == user.account);
    if (keys.isNotEmpty) {
      userBox.delete(keys.first);
    }
    userBox.add(user);
    if (userBox.length > 5) {
      userBox.deleteAt(0);
    }
  }

  void userEdit(UserLoginModel user) {
    Iterable keys = userBox.keys.where((key) => userBox.get(key)?.account == user.account);
    if (keys.isNotEmpty) {
      userBox.put(keys.first, user);
    }
  }

  void userDelete(UserLoginModel user) {
    Iterable keys = userBox.keys.where((key) => userBox.get(key)?.account == user.account);
    if (keys.isNotEmpty) {
      userBox.delete(keys.first);
    }
  }

  void userDeleteAt(int index) {
    userBox.deleteAt(index);
  }

  void userClear() {
    userBox.clear();
  }
}

class GetXOrderController extends GetxController {
  final GetSettingController getSettingController = Get.find();
  final newOrderBox = Hive.box('newOrderBox');
  final oldOrderBox = Hive.box('oldOrderBox');
  final abnormalBox = Hive.box<OrderState>('abnormalBox');

  void setNewOrderExpanded(int orderId, bool value) {
    newOrderBox.put(orderId, value);
    update();
  }

  bool getNewOrderExpanded(int orderId) {
    return newOrderBox.get(orderId, defaultValue: false);
  }

  void deleteNewOrderExpanded(int orderId) {
    newOrderBox.delete(orderId);
    update();
  }

  void setOldOrderExpanded(int orderId, bool value) {
    oldOrderBox.put(orderId, value);
    update();
  }

  bool getOldOrderExpanded(int orderId) {
    return oldOrderBox.get(orderId, defaultValue: false);
  }

  void deleteOldOrderExpanded(int orderId) {
    oldOrderBox.delete(orderId);
    update();
  }

  void setDefaultGoodsAbnormal({required OrderModel order, required List<GoodsModel> goods}) {
    if (abnormalBox.get(order.packOrderId) == null) {
      List<OrderStateItem> items = [];
      for (GoodsModel good in goods) {
        OrderStateItem item = OrderStateItem(
          skuId: good.skuId,
          image: good.image,
          goodName: good.goodsName,
          number: good.skuBuyNum,
          abnormal: false,
          mark: '',
        );
        items.add(item);
      }
      OrderState state = OrderState(
        packOrderId: order.packOrderId,
        orderSn: order.orderSn,
        startTime: order.startTime,
        endTime: order.endTime,
        packOrderStatus: order.packOrderStatus,
        goods: items,
      );
      abnormalBox.put(order.packOrderId, state);
      update();
    }
  }

  List<OrderStateItem> getOrderAbnormalGoods(int packOrderId) {
    OrderState? state = abnormalBox.get(packOrderId, defaultValue: OrderState(goods: []));
    List<OrderStateItem> states = [];
    for (OrderStateItem item in state!.goods) {
      states.add(item);
    }
    return states;
  }

  void setGoodsAbnormal({required int packOrderId, required int skuId, required bool abnormal, required String mark}) {
    OrderState? states = abnormalBox.get(packOrderId);
    OrderStateItem state = states!.goods.firstWhere((e) => e.skuId == skuId);
    int index = states.goods.indexOf(state);
    state.abnormal = abnormal;
    state.mark = mark;
    states.goods[index] = state;
    states.save();
    abnormalBox.put(packOrderId, states);
    update();
  }

  OrderStateItem getGoodsAbnormal({required int packOrderId, required int skuId}) {
    List<OrderStateItem> states = getOrderAbnormalGoods(packOrderId);
    return states.firstWhere((e) => e.skuId == skuId);
  }

  List<OrderState> get abnormalList {
    List<OrderState> states = [];
    for (OrderState items in abnormalBox.values) {
      List<OrderStateItem> goods = [];
      for (OrderStateItem good in items.goods) {
        goods.add(good);
      }
      items.goods = goods;
      items.save();
      states.add(items);
    }
    return states;
  }

  void deleteOrderAbnormal(int packOrderId) {
    abnormalBox.delete(packOrderId);
    update();
  }

  void deleteOrderAbnormalList(List<int> packOrderIds) {
    for (int i in packOrderIds) {
      abnormalBox.delete(i);
    }
    update();
  }

  final onlineLoading = false.obs;
  final tabIndex = 0.obs;
  final isOnline = false.obs;
  final loading = false.obs;
  final moreLoading = false.obs;
  final polling = true.obs;
  final pollTime = 0.obs;
  String selectedTime = '', startTime = '', endTime = '';

  final currentPage = 1.obs;

  final deliveryNumber = 0.obs;
  final townshipNumber = 0.obs;
  final transferNumber = 0.obs;
  final oldNumber = 0.obs;

  // ResultList<OrderModel> deliveryData = ResultList.setNull();
  // ResultList<OrderModel> townshipData = ResultList.setNull();
  // ResultList<OrderModel> transferData = ResultList.setNull();
  // ResultPage<OrderModel> oldData = ResultPage.setNull();
  final deliveryData = Rx<ResultList<OrderModel>>(ResultList.setNull());
  final townshipData = Rx<ResultList<OrderModel>>(ResultList.setNull());
  final transferData = Rx<ResultList<OrderModel>>(ResultList.setNull());
  final oldData = Rx<ResultPage<OrderModel>>(ResultPage.setNull());

  @override
  void onInit() {
    super.onInit();

    isOnline.value = getSettingController.login.online == 1;
    ever(isOnline, (callback) {
      LoginModel login = getSettingController.login;
      login.online = isOnline.isTrue ? 1 : 2;
      login.save();
      getSettingController.setUser<LoginModel>('user', login);
    });

    debounce(polling, (callback) {
      if (polling.isFalse) {
        setPoll(true);
      }
    }, time: const Duration(seconds: 31));

    interval(pollTime, (callback) {
      if (polling.isTrue) {
        onRefresh(old: true);
      }
    }, time: const Duration(seconds: 30));
  }

  @override
  void onReady() {
    super.onReady();
    onNewData();
  }

  void setOnline(int value) {
    isOnline.value = value == 1 ? true : false;
    LoginModel login = getSettingController.login;
    login.online = value;
    login.save();
    getSettingController.setUser<LoginModel>('user', login);
  }

  void setLoading(bool value) => loading.value = value;

  void setMoreLoading(bool value) => moreLoading.value = value;

  void setPoll(bool value) => polling.value = value;

  void setPollTime() => pollTime.value = DateTime.now().millisecondsSinceEpoch;

  void pageTo(int index) => tabIndex.value = index;

  void setOnlineLoading(bool value) => onlineLoading.value = value;

  void setStartTime(String startTime) {
    this.startTime = startTime;
    update();
  }

  void setEndTime(String endTime) {
    this.endTime = endTime;
    update();
  }

  void setSelectedTime(String selectedTime) {
    this.selectedTime = selectedTime;
    update();
  }

  void onNewData() {
    setPollTime();
    if (getSettingController.isLogin) {
      setLoading(true);
      getNewData();
      setLoading(false);
    }
  }

  void getNewData({bool old = true}) {
    getNewOrder(1);
    getNewOrder(2);
    getTransferOrder();
    if (old) {
      getOldOrder(pages: currentPage.value, plat: getSettingController.get('plat'), startTime: startTime, endTime: endTime);
    }
  }

  void onRefresh({bool old = true}) {
    setPollTime();
    if (getSettingController.isLogin) {
      getNewData(old: old);
    }
  }

  Future<void> getPackerInfo() async {
    GatewayProvider.getPackerInfo().then((res) {
      if (res.status == 200) {
        setOnline(res.data!.online);
      }
    });
  }

  Future<ResultList<OrderModel>> getNewOrder(int type) async {
    if (type == 1) {
      await HomePageProvider.getOrderNew(type: 1).then((res) {
        if (res != deliveryData.value) {
          deliveryData.value = res;
          deliveryNumber.value = res.data.length;
          update();
        }
      });
      return deliveryData.value;
    } else {
      await HomePageProvider.getOrderNew(type: 2).then((res) {
        if (res != townshipData.value) {
          townshipData.value = res;
          townshipNumber.value = res.data.length;
          update();
        }
      });
      return townshipData.value;
    }
  }

  Future<ResultList<OrderModel>> getTransferOrder() async {
    await HomePageProvider.getPackTransferOrder().then((res) {
      if (res != transferData.value) {
        transferData.value = res;
        transferNumber.value = res.data.length;
        update();
      }
    });
    return transferData.value;
  }

  void setOldOrderNull() {
    oldData.value = ResultPage.setNull();
    update();
  }

  void setOldOrderData(ResultPage<OrderModel> data) {
    oldData.value = data;
    update();
  }

  Future<ResultPage<OrderModel>> getOldOrder({int pages = 1, String? plat, String startTime = '', String endTime = ''}) async {
    String plats = plat ?? Plat.android.name;
    currentPage.value = pages;
    if (startTime.isNotEmpty && endTime.isNotEmpty) {
      this.startTime = startTime;
      this.endTime = endTime;
    }
    if (plats == Plat.android.name) {
      ResultPage<OrderModel> newOrder = ResultPage.setNull();
      for (int i = 1; i <= currentPage.value; i++) {
        List<OrderModel> data = newOrder.data.list;
        await HomePageProvider.getOrderOld(i, startTime: startTime, endTime: endTime).then((res) {
          oldNumber.value = res.data.totalCount;
          newOrder = res;
          data.addAll(res.data.list);
        });
        newOrder.data.list = data;
      }
      if (oldData.value != newOrder) {
        oldData.value = newOrder;
        oldNumber.value = oldData.value.data.totalCount;
        update();
      }
    } else {
      await HomePageProvider.getOrderOld(pages, startTime: startTime, endTime: endTime).then((res) {
        oldNumber.value = res.data.totalCount;
        oldData.value = res;
        update();
      });
    }
    return oldData.value;
  }
}

class GetXPackerController extends GetxController {
  List<PackerModel> packerDataList = [];
  int packerUserId = 0;
  String packerUserName = '';
  bool loading = false;

  void renew(List<PackerModel> list, int id, String name) {
    packerDataList = list;
    packerUserId = id;
    packerUserName = name;
  }

  void setLoading(bool value) {
    loading = value;
    update();
  }

  void setId(int id) {
    packerUserId = id;
    update();
  }

  void setName(String name) {
    packerUserName = name;
    update();
  }
}

class GetSettingController extends GetxController {
  final settingBox = Hive.box('settingBox');
  final loginBox = Hive.box('loginBox');

  LoginModel get login {
    return loginBox.get('user', defaultValue: LoginModel());
  }

  void set<T>(String key, T value) {
    settingBox.put(key, value);
    update();
  }

  T get<T>(String key, {dynamic defaultValue}) {
    return settingBox.get(key, defaultValue: defaultValue);
  }

  void setUser<T>(String key, T value) async {
    loginBox.put(key, value);
    update();
  }

  T getUser<T>(String key, {dynamic defaultValue}) {
    return loginBox.get(key, defaultValue: defaultValue);
  }

  bool get isLogin {
    int expire = login.expire!;
    int nowTime = Tools.getNowTimestamp();
    return expire > 0 && expire >= nowTime ? true : false;
  }
}
