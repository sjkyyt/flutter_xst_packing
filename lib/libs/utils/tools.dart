import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

class Tools {
  static isNotEmpty(String? str) {
    return str?.isNotEmpty ?? false;
  }

  static int getTimestamp(String time) {
    return DateTime.parse('$time.0000000').millisecondsSinceEpoch ~/ 1000;
  }

  static int getNowTimestamp() {
    return DateTime.timestamp().millisecondsSinceEpoch ~/ 1000;
  }

  static String getTimeFormat(int timestamp, {String format = 'yyyy-MM-dd HH:mm:ss'}) {
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    String formatDate = DateFormat(format).format(date);
    return formatDate;
  }

  static DateTime timestampToDate(int timestamp) {
    DateTime dateTime = DateTime.now();

    ///如果是十三位时间戳返回这个
    if (timestamp.toString().length == 10) {
      var time = int.parse('${timestamp}000');
      dateTime = DateTime.fromMillisecondsSinceEpoch(time);
    } else if (timestamp.toString().length == 13) {
      dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp);
    } else if (timestamp.toString().length == 16) {
      ///如果是十六位时间戳
      dateTime = DateTime.fromMicrosecondsSinceEpoch(timestamp);
    }
    return dateTime;
  }

  //时间格式化，根据总秒数转换为对应的 hh:mm:ss 格式
  static String constructTime(int seconds) {
    int hour = seconds ~/ 3600;
    int minute = seconds % 3600 ~/ 60;
    int second = seconds % 60;
    return "${formatTime(hour)}:${formatTime(minute)}:${formatTime(second)}";
  }

  //数字格式化，将 0~9 的时间转换为 00~09
  static String formatTime(int timeNum) {
    return timeNum < 10 ? "0$timeNum" : timeNum.toString();
  }

  static String formatDateTime(DateTime dateTime) {
    return '${dateTime.year.toString().padLeft(4, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.day.toString().padLeft(2, '0')} ${dateTime.hour.toString().padLeft(2, '0')}:${dateTime.minute.toString().padLeft(2, '0')}:${dateTime.second.toString().padLeft(2, '0')}';
  }
}


