// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_state.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class OrderStateAdapter extends TypeAdapter<OrderState> {
  @override
  final int typeId = 0;

  @override
  OrderState read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return OrderState(
      packOrderId: fields[0] as int,
      orderSn: fields[1] as String,
      startTime: fields[2] as int,
      endTime: fields[3] as int,
      packOrderStatus: fields[4] as int,
      goods: (fields[5] as List).cast<OrderStateItem>(),
    );
  }

  @override
  void write(BinaryWriter writer, OrderState obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.packOrderId)
      ..writeByte(1)
      ..write(obj.orderSn)
      ..writeByte(2)
      ..write(obj.startTime)
      ..writeByte(3)
      ..write(obj.endTime)
      ..writeByte(4)
      ..write(obj.packOrderStatus)
      ..writeByte(5)
      ..write(obj.goods);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OrderStateAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class OrderStateItemAdapter extends TypeAdapter<OrderStateItem> {
  @override
  final int typeId = 1;

  @override
  OrderStateItem read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return OrderStateItem(
      skuId: fields[0] as int,
      abnormal: fields[1] as bool,
      image: fields[2] as String,
      goodName: fields[3] as String,
      number: fields[4] as int,
      mark: fields[5] as String,
    );
  }

  @override
  void write(BinaryWriter writer, OrderStateItem obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.skuId)
      ..writeByte(1)
      ..write(obj.abnormal)
      ..writeByte(2)
      ..write(obj.image)
      ..writeByte(3)
      ..write(obj.goodName)
      ..writeByte(4)
      ..write(obj.number)
      ..writeByte(5)
      ..write(obj.mark);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OrderStateItemAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
