import 'package:hive/hive.dart';
import 'package:packing/libs/models/safe_convert.dart';

part 'gateway.g.dart';

@HiveType(typeId: 3)
class LoginModel extends HiveObject {
  @HiveField(0)
  String? tokenType;
  @HiveField(1)
  String? accessToken;
  @HiveField(2)
  int? expire;
  @HiveField(3)
  String? refreshToken;
  @HiveField(4)
  int? refreshExpire;
  @HiveField(5)
  int packerId;
  @HiveField(6)
  String account;
  @HiveField(7)
  int storeId;
  @HiveField(8)
  String? storeName;
  @HiveField(9)
  String avatar;
  @HiveField(10)
  String realName;
  @HiveField(11)
  int loginCount;
  @HiveField(12)
  String lastIp;
  @HiveField(13)
  int createdAt;
  @HiveField(14)
  int updatedAt;
  @HiveField(15)
  int online;
  @HiveField(16)
  int status;
  @HiveField(17)
  int? isDel;

  LoginModel({
    this.tokenType = "",
    this.accessToken = "",
    this.expire = 0,
    this.refreshToken = "",
    this.refreshExpire = 0,
    this.packerId = 0,
    this.account = "",
    this.storeId = 0,
    this.storeName = "",
    this.avatar = "",
    this.realName = "",
    this.loginCount = 0,
    this.lastIp = "",
    this.createdAt = 0,
    this.updatedAt = 0,
    this.online = 2,
    this.status = 0,
    this.isDel = 0,
  });

  factory LoginModel.fromJson(Map<String, dynamic>? json) => LoginModel(
        tokenType: asString(json, 'token_type'),
        accessToken: asString(json, 'access_token'),
        expire: asInt(json, 'expire'),
        refreshToken: asString(json, 'refresh_token'),
        refreshExpire: asInt(json, 'refresh_expire'),
        packerId: asInt(json, 'packer_id'),
        account: asString(json, 'account'),
        storeId: asInt(json, 'store_id'),
        storeName: asString(json, 'store_name'),
        avatar: asString(json, 'avatar'),
        realName: asString(json, 'real_name'),
        loginCount: asInt(json, 'login_count'),
        lastIp: asString(json, 'last_ip'),
        createdAt: asInt(json, 'created_at'),
        updatedAt: asInt(json, 'updated_at'),
        online: asInt(json, 'online'),
        status: asInt(json, 'status'),
        isDel: asInt(json, 'is_del'),
      );

  Map<String, dynamic> toJson() => {
        'token_type': tokenType,
        'access_token': accessToken,
        'expire': expire,
        'refresh_token': refreshToken,
        'refresh_expire': refreshExpire,
        'packer_id': packerId,
        'account': account,
        'store_id': storeId,
        'store_name': storeName,
        'avatar': avatar,
        'real_name': realName,
        'login_count': loginCount,
        'last_ip': lastIp,
        'created_at': createdAt,
        'updated_at': updatedAt,
        'online': online,
        'status': status,
        'is_del': isDel,
      };
}
