import 'package:packing/libs/models/safe_convert.dart';

class GoodsModel {
  int id;
  int orderId;
  int storeId;
  int goodsId;
  String goodsName;
  int skuId;
  String skuBarcode;
  int skuPrice;
  int skuBuyingPrice;
  int promotionId;
  int skuPromotionPayPrice;
  int skuPromotionBuyNum;
  int commissionRatio;
  int pointsRatio;
  int skuCanRefundPriceTotal;
  int skuBrokerage;
  String skuRefundType;
  int skuRefundStatus;
  int skuPayPrice;
  int skuBuyNum;
  String skuInfo;
  String image;
  int skuType;
  String composedAttrJson;
  String skuStoreName;
  String skuCode;

  GoodsModel({
    this.id = 0,
    this.orderId = 0,
    this.storeId = 0,
    this.goodsId = 0,
    this.goodsName = "",
    this.skuId = 0,
    this.skuBarcode = "",
    this.skuPrice = 0,
    this.skuBuyingPrice = 0,
    this.promotionId = 0,
    this.skuPromotionPayPrice = 0,
    this.skuPromotionBuyNum = 0,
    this.commissionRatio = 0,
    this.pointsRatio = 0,
    this.skuCanRefundPriceTotal = 0,
    this.skuBrokerage = 0,
    this.skuRefundType = "",
    this.skuRefundStatus = 0,
    this.skuPayPrice = 0,
    this.skuBuyNum = 0,
    this.skuInfo = "",
    this.image = "",
    this.skuType = 0,
    this.composedAttrJson = "",
    this.skuStoreName = "",
    this.skuCode = "",
  });

  factory GoodsModel.fromJson(Map<String, dynamic> json) => GoodsModel(
        id: asInt(json, 'id'),
        orderId: asInt(json, 'order_id'),
        storeId: asInt(json, 'store_id'),
        goodsId: asInt(json, 'goods_id'),
        goodsName: asString(json, 'goods_name'),
        skuId: asInt(json, 'sku_id'),
        skuBarcode: asString(json, 'sku_barcode'),
        skuPrice: asInt(json, 'sku_price'),
        skuBuyingPrice: asInt(json, 'sku_buying_price'),
        promotionId: asInt(json, 'promotion_id'),
        skuPromotionPayPrice: asInt(json, 'sku_promotion_pay_price'),
        skuPromotionBuyNum: asInt(json, 'sku_promotion_buy_num'),
        commissionRatio: asInt(json, 'commission_ratio'),
        pointsRatio: asInt(json, 'points_ratio'),
        skuCanRefundPriceTotal: asInt(json, 'sku_can_refund_price_total'),
        skuBrokerage: asInt(json, 'sku_brokerage'),
        skuRefundType: asString(json, 'sku_refund_type'),
        skuRefundStatus: asInt(json, 'sku_refund_status'),
        skuPayPrice: asInt(json, 'sku_pay_price'),
        skuBuyNum: asInt(json, 'sku_buy_num'),
        skuInfo: asString(json, 'sku_info'),
        image: asString(json, 'image'),
        skuType: asInt(json, 'sku_type'),
        composedAttrJson: asString(json, 'composed_attr_json'),
        skuStoreName: asString(json, 'sku_store_name'),
        skuCode: asString(json, 'sku_code'),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'order_id': orderId,
        'store_id': storeId,
        'goods_id': goodsId,
        'goods_name': goodsName,
        'sku_id': skuId,
        'sku_barcode': skuBarcode,
        'sku_price': skuPrice,
        'sku_buying_price': skuBuyingPrice,
        'promotion_id': promotionId,
        'sku_promotion_pay_price': skuPromotionPayPrice,
        'sku_promotion_buy_num': skuPromotionBuyNum,
        'commission_ratio': commissionRatio,
        'points_ratio': pointsRatio,
        'sku_can_refund_price_total': skuCanRefundPriceTotal,
        'sku_brokerage': skuBrokerage,
        'sku_refund_type': skuRefundType,
        'sku_refund_status': skuRefundStatus,
        'sku_pay_price': skuPayPrice,
        'sku_buy_num': skuBuyNum,
        'sku_info': skuInfo,
        'image': image,
        'sku_type': skuType,
        'composed_attr_json': composedAttrJson,
        'sku_store_name': skuStoreName,
        'sku_code': skuCode,
      };
}
