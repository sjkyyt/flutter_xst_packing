import 'dart:convert';

class ResultObj<T> {
  String msg;
  int status;
  T? data;

  ResultObj({
    this.msg = '',
    this.status = 201,
    required this.data,
  });

  factory ResultObj.setNull() => ResultObj(
        status: 201,
        msg: '',
        data: null,
      );

  factory ResultObj.fromJsonNull(String obj) {
    Map jsonMap = json.decode(obj);
    return ResultObj(
      msg: jsonMap['msg'].toString(),
      status: jsonMap['status'],
      data: jsonMap['data'],
    );
  }

  factory ResultObj.fromJson(String obj, T Function(dynamic json) fromJsonT) {
    Map jsonMap = json.decode(obj);
    return ResultObj(
      msg: jsonMap['msg'],
      status: jsonMap['status'],
      data: fromJsonT(jsonMap['data'] as Map<String, dynamic>),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = this.status;
    data['msg'] = this.msg;
    data['data'] = this.data;
    return data;
  }
}

class ResultList<T> {
  String msg;
  int status;
  List<T> data;

  ResultList({
    this.msg = '',
    this.status = 201,
    required this.data,
  });

  factory ResultList.setNull() => ResultList(
        status: 201,
        msg: '',
        data: [],
      );

  factory ResultList.fromJson(String obj, T Function(dynamic json) fromJsonT) {
    Map jsonMap = json.decode(obj);
    return ResultList(
      status: jsonMap['status'],
      msg: jsonMap['msg'],
      data: (jsonMap['data'] as List<dynamic>).map((e) => fromJsonT(e)).toList(),
    );
  }
}

class ResultPage<T> {
  String msg;
  int status;
  Data<T> data;

  ResultPage({
    this.msg = '',
    this.status = 201,
    required this.data,
  });

  factory ResultPage.setNull() => ResultPage(
        status: 201,
        msg: '',
        data: Data.fromNull(),
      );

  factory ResultPage.fromJson(String obj, T Function(dynamic json) fromJsonT) {
    Map jsonMap = json.decode(obj);
    return ResultPage(
      status: jsonMap['status'],
      msg: jsonMap['msg'],
      data: Data.fromJson(jsonMap['data'], (json) => fromJsonT(json)),
    );
  }
}

class Data<T> {
  int totalCount;
  int pageSize;
  int totalPage;
  int currentPage;
  List<T> list;

  Data({
    this.totalCount = 0,
    this.pageSize = 10,
    this.totalPage = 0,
    this.currentPage = 1,
    required this.list,
  });

  factory Data.fromNull() => Data(
        totalCount: 0,
        pageSize: 10,
        totalPage: 0,
        currentPage: 1,
        list: [],
      );

  factory Data.fromJson(Map<String, dynamic> json, T Function(dynamic json) fromJsonT) {
    return Data(
      totalCount: json['total'],
      pageSize: json['per_page'],
      totalPage: json['last_page'],
      currentPage: json['current_page'],
      list: (json['data'] as List<dynamic>).map((e) => fromJsonT(e)).toList(),
    );
  }
}

class ErrorModel {
  String err;
  String file;

  ErrorModel({
    this.err = '',
    this.file = '',
  });

  factory ErrorModel.fromJson(Map<String, dynamic> json, Function(dynamic json) fromJsonT) {
    return ErrorModel(
      err: json['err'],
      file: json['file'],
    );
  }
}
