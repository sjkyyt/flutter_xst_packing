import 'package:hive/hive.dart';
import 'package:packing/libs/models/safe_convert.dart';

part 'order_state.g.dart';

@HiveType(typeId: 0)
class OrderState extends HiveObject {
  @HiveField(0)
  int packOrderId;
  @HiveField(1)
  String orderSn;
  @HiveField(2)
  int startTime;
  @HiveField(3)
  int endTime;
  @HiveField(4)
  int packOrderStatus;
  @HiveField(5)
  List<OrderStateItem> goods;

  OrderState({
    this.packOrderId = 0,
    this.orderSn = '',
    this.startTime = 0,
    this.endTime = 0,
    this.packOrderStatus = 0,
    required this.goods,
  });

  factory OrderState.fromJson(Map<String, dynamic>? json) => OrderState(
    packOrderId: asInt(json, 'packOrderId'),
    orderSn: asString(json, 'orderSn'),
    startTime: asInt(json, 'startTime'),
    endTime: asInt(json, 'endTime'),
    packOrderStatus: asInt(json, 'packOrderStatus'),
    goods: asList(json, 'goods').map((e) => OrderStateItem.fromJson(e)).toList(),
  );

  Map<String, dynamic> toJson() => {
    'packOrderId': packOrderId,
    'orderSn': orderSn,
    'startTime': startTime,
    'endTime': endTime,
    'packOrderStatus': packOrderStatus,
    'goods': goods,
  };
}

@HiveType(typeId: 1)
class OrderStateItem extends HiveObject {
  @HiveField(0)
  int skuId;
  @HiveField(1)
  bool abnormal;
  @HiveField(2)
  String image;
  @HiveField(3)
  String goodName;
  @HiveField(4)
  int number;
  @HiveField(5)
  String mark;

  OrderStateItem({
    this.skuId = 0,
    this.abnormal = false,
    this.image = '',
    this.goodName = '',
    this.number = 0,
    this.mark = '',
  });

  factory OrderStateItem.fromJson(Map<String, dynamic>? json) => OrderStateItem(
        skuId: asInt(json, 'skuId'),
        abnormal: asBool(json, 'abnormal'),
        image: asString(json, 'image'),
        goodName: asString(json, 'goodName'),
        number: asInt(json, 'number'),
        mark: asString(json, 'mark'),
      );

  Map<String, dynamic> toJson() => {
        'skuId': skuId,
        'abnormal': abnormal,
        'image': image,
        'goodName': goodName,
        'number': number,
        'mark': mark,
      };
}
