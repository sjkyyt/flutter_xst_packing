import 'package:hive/hive.dart';
import 'package:packing/libs/models/safe_convert.dart';

@HiveType(typeId: 2)
class UserLoginModel extends HiveObject {
  @HiveField(0)
  String account;
  @HiveField(1)
  String userName;
  @HiveField(2)
  String passWord;

  UserLoginModel({
    this.account = '',
    this.userName = '',
    this.passWord = '',
  });

  factory UserLoginModel.fromJson(Map<String, dynamic>? json) => UserLoginModel(
        account: asString(json, 'account'),
        userName: asString(json, 'userName'),
        passWord: asString(json, 'passWord'),
      );

  Map<String, dynamic> toJson() => {
        'account': account,
        'userName': userName,
        'passWord': passWord,
      };
}

class UserLoginModelAdapter extends TypeAdapter<UserLoginModel> {
  @override
  final int typeId = 2;

  @override
  UserLoginModel read(BinaryReader reader) {
    return UserLoginModel(
      account: reader.read(),
      userName: reader.read(),
      passWord: reader.read(),
    );
  }

  @override
  void write(BinaryWriter writer, obj) {
    writer.write(obj.account);
    writer.write(obj.userName);
    writer.write(obj.passWord);
  }
}
