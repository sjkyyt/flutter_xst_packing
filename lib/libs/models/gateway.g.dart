// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateway.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LoginModelAdapter extends TypeAdapter<LoginModel> {
  @override
  final int typeId = 3;

  @override
  LoginModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return LoginModel(
      tokenType: fields[0] as String?,
      accessToken: fields[1] as String?,
      expire: fields[2] as int?,
      refreshToken: fields[3] as String?,
      refreshExpire: fields[4] as int?,
      packerId: fields[5] as int,
      account: fields[6] as String,
      storeId: fields[7] as int,
      storeName: fields[8] as String?,
      avatar: fields[9] as String,
      realName: fields[10] as String,
      loginCount: fields[11] as int,
      lastIp: fields[12] as String,
      createdAt: fields[13] as int,
      updatedAt: fields[14] as int,
      online: fields[15] as int,
      status: fields[16] as int,
      isDel: fields[17] as int?,
    );
  }

  @override
  void write(BinaryWriter writer, LoginModel obj) {
    writer
      ..writeByte(18)
      ..writeByte(0)
      ..write(obj.tokenType)
      ..writeByte(1)
      ..write(obj.accessToken)
      ..writeByte(2)
      ..write(obj.expire)
      ..writeByte(3)
      ..write(obj.refreshToken)
      ..writeByte(4)
      ..write(obj.refreshExpire)
      ..writeByte(5)
      ..write(obj.packerId)
      ..writeByte(6)
      ..write(obj.account)
      ..writeByte(7)
      ..write(obj.storeId)
      ..writeByte(8)
      ..write(obj.storeName)
      ..writeByte(9)
      ..write(obj.avatar)
      ..writeByte(10)
      ..write(obj.realName)
      ..writeByte(11)
      ..write(obj.loginCount)
      ..writeByte(12)
      ..write(obj.lastIp)
      ..writeByte(13)
      ..write(obj.createdAt)
      ..writeByte(14)
      ..write(obj.updatedAt)
      ..writeByte(15)
      ..write(obj.online)
      ..writeByte(16)
      ..write(obj.status)
      ..writeByte(17)
      ..write(obj.isDel);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LoginModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
