import 'package:packing/libs/models/safe_convert.dart';

class VersionModel {
  int id;
  int appType;
  int isForce;
  String version;
  String desc;
  int versionCode;
  int plat;
  int status;
  String url;
  int createdAt;

  VersionModel({
    this.id = 0,
    this.appType = 3,
    this.isForce = 0,
    this.version = "",
    this.desc = "",
    this.versionCode = 0,
    this.plat = 0,
    this.status = 0,
    this.url = "",
    this.createdAt = 0,
  });

  factory VersionModel.fromJson(Map<String, dynamic>? json) => VersionModel(
        id: asInt(json, 'id'),
        appType: asInt(json, 'app_type'),
        isForce: asInt(json, 'is_force'),
        version: asString(json, 'version'),
        desc: asString(json, 'desc'),
        versionCode: asInt(json, 'version_code'),
        plat: asInt(json, 'plat'),
        status: asInt(json, 'status'),
        url: asString(json, 'url'),
        createdAt: asInt(json, 'created_at'),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'app_type': appType,
        'is_force': isForce,
        'version': version,
        'desc': desc,
        'version_code': versionCode,
        'plat': plat,
        'status': status,
        'url': url,
        'created_at': createdAt,
      };
}
