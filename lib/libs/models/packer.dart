import 'package:packing/libs/models/safe_convert.dart';

class PackerModel {
  int packerId;
  int storeId;
  String avatar;
  String realName;
  int loginCount;
  String lastIp;
  int online;
  int status;
  int isDel;

  PackerModel({
    this.packerId = 0,
    this.storeId = 0,
    this.avatar = "",
    this.realName = "",
    this.loginCount = 0,
    this.lastIp = "",
    this.online = 0,
    this.status = 0,
    this.isDel = 0,
  });

  factory PackerModel.fromJson(Map<String, dynamic>? json) => PackerModel(
        packerId: asInt(json, 'packer_id'),
        storeId: asInt(json, 'store_id'),
        avatar: asString(json, 'avatar'),
        realName: asString(json, 'real_name'),
        loginCount: asInt(json, 'login_count'),
        lastIp: asString(json, 'last_ip'),
        online: asInt(json, 'online'),
        status: asInt(json, 'status'),
        isDel: asInt(json, 'is_del'),
      );

  Map<String, dynamic> toJson() => {
        'packer_id': packerId,
        'store_id': storeId,
        'avatar': avatar,
        'real_name': realName,
        'login_count': loginCount,
        'last_ip': lastIp,
        'online': online,
        'status': status,
        'is_del': isDel,
      };
}
