import 'package:packing/libs/models/goods.dart';
import 'package:packing/libs/models/safe_convert.dart';

class OrderModel {
  int packOrderTransferId;
  int packOrderId;
  int inUserId;
  String inUserRealname;
  int outUserId;
  String inUserPhone;
  String outUserRealname;
  String outUserPhone;
  int orderTransferStatus;
  String desc;
  int orderId;
  int createdAt;
  int isNormal;
  int packOrderStatus;
  int packUserId;
  String packUserNickname;
  int isOvertime;
  int sysType;
  int packTransferStatus;
  String packMark;
  int startTime;
  int endTime;
  String orderSn;
  int storeId;
  String addressName;
  String addressPhone;
  String address;
  String addressLocation;
  int payPrice;
  int refundStatus;
  int refundType;
  int packPrice;
  int deliveryType;
  int deliveryPrice;
  int orderDayNumber;
  String userMark;
  int goodsNum;
  String storeName;
  String servicePhone;
  String storeLatitude;
  String storeLongitude;
  bool isExpanded;
  List<GoodsModel> goods;

  OrderModel({
    this.packOrderTransferId = 0,
    this.packOrderId = 0,
    this.inUserId = 0,
    this.inUserRealname = "",
    this.outUserId = 0,
    this.inUserPhone = "",
    this.outUserRealname = "",
    this.outUserPhone = "",
    this.orderTransferStatus = 0,
    this.desc = "",
    this.orderId = 0,
    this.createdAt = 0,
    this.isNormal = 0,
    this.packOrderStatus = 0,
    this.packUserId = 0,
    this.packUserNickname = "",
    this.isOvertime = 0,
    this.sysType = 0,
    this.packTransferStatus = 0,
    this.packMark = "",
    this.startTime = 0,
    this.endTime = 0,
    this.orderSn = "",
    this.storeId = 0,
    this.addressName = "",
    this.addressPhone = "",
    this.address = "",
    this.addressLocation = "",
    this.payPrice = 0,
    this.refundStatus = 0,
    this.refundType = 0,
    this.packPrice = 0,
    this.deliveryType = 0,
    this.deliveryPrice = 0,
    this.orderDayNumber = 0,
    this.userMark = "",
    this.goodsNum = 0,
    this.storeName = "",
    this.servicePhone = "",
    this.storeLatitude = "",
    this.storeLongitude = "",
    this.isExpanded = false,
    required this.goods,
  });

  factory OrderModel.fromJson(Map<String, dynamic>? json) => OrderModel(
        packOrderTransferId: asInt(json, 'pack_order_transfer_id'),
        packOrderId: asInt(json, 'pack_order_id'),
        inUserId: asInt(json, 'in_user_id'),
        inUserRealname: asString(json, 'in_user_realname'),
        outUserId: asInt(json, 'out_user_id'),
        inUserPhone: asString(json, 'in_user_phone'),
        outUserRealname: asString(json, 'out_user_realname'),
        outUserPhone: asString(json, 'out_user_phone'),
        orderTransferStatus: asInt(json, 'order_transfer_status'),
        desc: asString(json, 'desc'),
        orderId: asInt(json, 'order_id'),
        createdAt: asInt(json, 'created_at'),
        isNormal: asInt(json, 'is_normal'),
        packOrderStatus: asInt(json, 'pack_order_status'),
        packUserId: asInt(json, 'pack_user_id'),
        packUserNickname: asString(json, 'pack_user_nickname'),
        isOvertime: asInt(json, 'is_overtime'),
        sysType: asInt(json, 'sys_type'),
        packTransferStatus: asInt(json, 'pack_transfer_status'),
        packMark: asString(json, 'pack_mark'),
        startTime: asInt(json, 'start_time'),
        endTime: asInt(json, 'end_time'),
        orderSn: asString(json, 'order_sn'),
        storeId: asInt(json, 'store_id'),
        addressName: asString(json, 'address_name'),
        addressPhone: asString(json, 'address_phone'),
        address: asString(json, 'address'),
        addressLocation: asString(json, 'address_location'),
        payPrice: asInt(json, 'pay_price'),
        refundStatus: asInt(json, 'refund_status'),
        refundType: asInt(json, 'refund_type'),
        packPrice: asInt(json, 'pack_price'),
        deliveryType: asInt(json, 'delivery_type'),
        deliveryPrice: asInt(json, 'delivery_price'),
        orderDayNumber: asInt(json, 'order_day_number'),
        userMark: asString(json, 'user_mark'),
        goodsNum: asInt(json, 'goods_num'),
        storeName: asString(json, 'store_name'),
        servicePhone: asString(json, 'service_phone'),
        storeLatitude: asString(json, 'store_latitude'),
        storeLongitude: asString(json, 'store_longitude'),
        goods: asList(json, 'goods').map((e) => GoodsModel.fromJson(e)).toList(),
      );

  Map<String, dynamic> toJson() => {
        'pack_order_transfer_id': packOrderTransferId,
        'pack_order_id': packOrderId,
        'in_user_id': inUserId,
        'in_user_realname': inUserRealname,
        'out_user_id': outUserId,
        'in_user_phone': inUserPhone,
        'out_user_realname': outUserRealname,
        'out_user_phone': outUserPhone,
        'order_transfer_status': orderTransferStatus,
        'desc': desc,
        'order_id': orderId,
        'created_at': createdAt,
        'is_normal': isNormal,
        'pack_order_status': packOrderStatus,
        'pack_user_id': packUserId,
        'pack_user_nickname': packUserNickname,
        'is_overtime': isOvertime,
        'sys_type': sysType,
        'pack_transfer_status': packTransferStatus,
        'pack_mark': packMark,
        'start_time': startTime,
        'end_time': endTime,
        'order_sn': orderSn,
        'store_id': storeId,
        'address_name': addressName,
        'address_phone': addressPhone,
        'address': address,
        'address_location': addressLocation,
        'pay_price': payPrice,
        'refund_status': refundStatus,
        'refund_type': refundType,
        'pack_price': packPrice,
        'delivery_type': deliveryType,
        'delivery_price': deliveryPrice,
        'order_day_number': orderDayNumber,
        'user_mark': userMark,
        'goods_num': goodsNum,
        'store_name': storeName,
        'service_phone': servicePhone,
        'store_latitude': storeLatitude,
        'store_longitude': storeLongitude,
        'goods': goods.map((e) => e.toJson()).toList(),
      };
}
