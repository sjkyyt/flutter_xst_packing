import 'dart:async';
import 'package:flutter/material.dart';
import 'package:packing/libs/utils/tools.dart';

class Countdown extends StatefulWidget {
  final TextStyle style;
  final int times;
  final bool timer;

  const Countdown({super.key, required this.times, required this.style, this.timer = false});

  @override
  State<Countdown> createState() => _CountdownState();
}

class _CountdownState extends State<Countdown> {
  final ValueNotifier<int> seconds = ValueNotifier(0);
  late Timer _timer;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ValueListenableBuilder<int>(
        valueListenable: seconds,
        builder: (BuildContext context, int value, Widget? child) {
          return Text(
            '${widget.timer ? '超' : ''}${Tools.constructTime(value)}',
            style: widget.style,
          );
        },
      ),
    );
  }

  @override
  void initState() {
    //获取当期时间
    var now = DateTime.now();
    var date = Tools.timestampToDate(widget.times);
    if (widget.timer) {
      if (date.isBefore(now)) {
        var lateTimes = now.difference(date);
        seconds.value = lateTimes.inSeconds;
      } else {
        seconds.value = 0;
      }
    } else {
      if (date.isAfter(now)) {
        var lateTimes = date.difference(now);
        seconds.value = lateTimes.inSeconds;
      } else {
        seconds.value = 0;
      }
    }
    startTimer();
    super.initState();
  }

  void startTimer() {
    //设置 1 秒回调一次
    const period = Duration(seconds: 1);
    _timer = Timer.periodic(period, (timer) {
      //更新界面
      if (widget.timer) {
        //秒数加一，因为一秒回调一次
        seconds.value++;
      } else {
        //秒数减一，因为一秒回调一次
        seconds.value--;
        if (seconds.value == 0) {
          //倒计时秒数为0，取消定时器
          cancelTimer();
        }
      }
    });
  }

  void cancelTimer() {
    // if (_timer != null) {
    //   _timer.cancel();
    //   // _timer = null;
    // }
    _timer.cancel();
  }

  @override
  void dispose() {
    super.dispose();
    cancelTimer();
  }
}
