import 'package:flutter/material.dart';

import 'enhance_expand_icon.dart';

const double _kPanelHeaderCollapsedHeight = kMinInteractiveDimension;
const EdgeInsets _kPanelHeaderExpandedDefaultPadding = EdgeInsets.symmetric(
  vertical: 64.0 - _kPanelHeaderCollapsedHeight,
);

class _SaltedEnhanceKey<S, V> extends LocalKey {
  const _SaltedEnhanceKey(this.salt, this.value);

  final S salt;
  final V value;

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is _SaltedEnhanceKey<S, V> && other.salt == salt && other.value == value;
  }

  @override
  int get hashCode => hashValues(runtimeType, salt, value);

  @override
  String toString() {
    final String saltString = S == String ? "<'$salt'>" : '<$salt>';
    final String valueString = V == String ? "<'$value'>" : '<$value>';
    return '[$saltString $valueString]';
  }
}

enum EnhanceExpansionPanelArrowPosition {
  leading,
  tailing,
  bottom,
  none
}

class EnhanceExpansionPanel {

  EnhanceExpansionPanel({
    required this.headerBuilder,
    required this.body,
    required this.footerBuilder,
    this.isExpanded = false,
    this.canTapOnHeader = false,
    this.backgroundColor,
    this.arrowColor = Colors.black54,
    this.arrowPadding,
    this.arrowPosition = EnhanceExpansionPanelArrowPosition.tailing,
    this.arrowExpanded,
    this.arrow,
  });

  final ExpansionPanelHeaderBuilder headerBuilder;
  final ExpansionPanelHeaderBuilder footerBuilder;
  final Widget body;
  late bool isExpanded;
  final bool canTapOnHeader;
  final Color? backgroundColor;
  final Color? arrowColor;
  final EnhanceExpansionPanelArrowPosition arrowPosition;
  final EdgeInsetsGeometry? arrowPadding;
  final Widget? arrow;
  final Widget? arrowExpanded;
}

class EnhanceExpansionPanelRadio extends EnhanceExpansionPanel {

  EnhanceExpansionPanelRadio({
    required this.value,
    required super.headerBuilder,
    required super.body,
    required super.footerBuilder,
    super.canTapOnHeader,
    super.backgroundColor,
  });

  final Object value;
}

class EnhanceExpansionPanelList extends StatefulWidget {
  const EnhanceExpansionPanelList({
    super.key,
    this.children = const <EnhanceExpansionPanel>[],
    this.expansionCallback,
    this.animationDuration = kThemeAnimationDuration,
    this.expandedHeaderPadding = _kPanelHeaderExpandedDefaultPadding,
    this.dividerColor,
    this.elevation = 2,
  })  : _allowOnlyOnePanelOpen = false,
        initialOpenPanelValue = null;

  const EnhanceExpansionPanelList.radio({
    super.key,
    this.children = const <EnhanceExpansionPanelRadio>[],
    this.expansionCallback,
    this.animationDuration = kThemeAnimationDuration,
    this.initialOpenPanelValue,
    this.expandedHeaderPadding = _kPanelHeaderExpandedDefaultPadding,
    this.dividerColor,
    this.elevation = 2,
  }) : _allowOnlyOnePanelOpen = true;

  final List<EnhanceExpansionPanel> children;

  final ExpansionPanelCallback? expansionCallback;

  final Duration animationDuration;

  final bool _allowOnlyOnePanelOpen;

  final Object? initialOpenPanelValue;

  final EdgeInsets expandedHeaderPadding;

  final Color? dividerColor;

  final double elevation;

  @override
  State<StatefulWidget> createState() => _EnhanceExpansionPanelListState();
}

class _EnhanceExpansionPanelListState extends State<EnhanceExpansionPanelList> {
  EnhanceExpansionPanelRadio? _currentOpenPanel;

  @override
  void initState() {
    super.initState();
    if (widget._allowOnlyOnePanelOpen) {
      assert(_allIdentifiersUnique(), 'All EnhanceExpansionPanelRadio identifier values must be unique.');
      if (widget.initialOpenPanelValue != null) {
        _currentOpenPanel = searchPanelByValue(widget.children.cast<EnhanceExpansionPanelRadio>(), widget.initialOpenPanelValue);
      }
    }
  }

  @override
  void didUpdateWidget(EnhanceExpansionPanelList oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget._allowOnlyOnePanelOpen) {
      assert(_allIdentifiersUnique(), 'All EnhanceExpansionPanelRadio identifier values must be unique.');
      // If the previous widget was non-radio EnhanceExpansionPanelList, initialize the
      // open panel to widget.initialOpenPanelValue
      if (!oldWidget._allowOnlyOnePanelOpen) {
        _currentOpenPanel = searchPanelByValue(widget.children.cast<EnhanceExpansionPanelRadio>(), widget.initialOpenPanelValue);
      }
    } else {
      _currentOpenPanel = null;
    }
  }

  bool _allIdentifiersUnique() {
    final Map<Object, bool> identifierMap = <Object, bool>{};
    for (final EnhanceExpansionPanelRadio child in widget.children.cast<EnhanceExpansionPanelRadio>()) {
      identifierMap[child.value] = true;
    }
    return identifierMap.length == widget.children.length;
  }

  bool _isChildExpanded(int index) {
    if (widget._allowOnlyOnePanelOpen) {
      final EnhanceExpansionPanelRadio radioWidget = widget.children[index] as EnhanceExpansionPanelRadio;
      return _currentOpenPanel?.value == radioWidget.value;
    }
    return widget.children[index].isExpanded;
  }

  void _handlePressed(bool isExpanded, int index) {
    widget.expansionCallback?.call(index, isExpanded);

    if (widget._allowOnlyOnePanelOpen) {
      final EnhanceExpansionPanelRadio pressedChild = widget.children[index] as EnhanceExpansionPanelRadio;

      // If another EnhanceExpansionPanelRadio was already open, apply its
      // expansionCallback (if any) to false, because it's closing.
      for (int childIndex = 0; childIndex < widget.children.length; childIndex += 1) {
        final EnhanceExpansionPanelRadio child = widget.children[childIndex] as EnhanceExpansionPanelRadio;
        if (widget.expansionCallback != null && childIndex != index && child.value == _currentOpenPanel?.value) {
          widget.expansionCallback!(childIndex, false);
        }
      }

      setState(() {
        _currentOpenPanel = isExpanded ? null : pressedChild;
      });
    }
  }

  EnhanceExpansionPanelRadio? searchPanelByValue(List<EnhanceExpansionPanelRadio> panels, Object? value) {
    for (final EnhanceExpansionPanelRadio panel in panels) {
      if (panel.value == value) return panel;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    assert(
      kElevationToShadow.containsKey(widget.elevation),
      'Invalid value for elevation. See the kElevationToShadow constant for'
      ' possible elevation values.',
    );

    final List<MergeableMaterialItem> items = <MergeableMaterialItem>[];

    for (int index = 0; index < widget.children.length; index += 1) {
      if (_isChildExpanded(index) && index != 0 && !_isChildExpanded(index - 1)) {
        items.add(MaterialGap(key: _SaltedEnhanceKey<BuildContext, int>(context, index * 2 - 1)));
      }

      final EnhanceExpansionPanel child = widget.children[index];
      final Widget headerWidget = child.headerBuilder(
        context,
        _isChildExpanded(index),
      );
      final Widget footerWidget = child.footerBuilder(
        context,
        _isChildExpanded(index),
      );

      Widget expandIconContainer = child.arrow == EnhanceExpansionPanelArrowPosition.none
          ? const SizedBox()
          : Container(
              margin: const EdgeInsetsDirectional.only(end: 8.0),
              child: EnhanceExpandIcon(
                arrow: child.arrow,
                arrowExpanded: child.arrowExpanded,
                color: child.arrowColor,
                isExpanded: _isChildExpanded(index),
                padding: child.arrowPadding ?? const EdgeInsets.all(16.0),
                onPressed: !child.canTapOnHeader ? (bool isExpanded) => _handlePressed(isExpanded, index) : null,
              ),
            );
      if (!child.canTapOnHeader) {
        final MaterialLocalizations localizations = MaterialLocalizations.of(context);
        expandIconContainer = Semantics(
          label: _isChildExpanded(index) ? localizations.expandedIconTapHint : localizations.collapsedIconTapHint,
          container: true,
          child: expandIconContainer,
        );
      }
      Widget header = Row(
        children: <Widget>[
          if (child.arrow != EnhanceExpansionPanelArrowPosition.none && child.arrowPosition == EnhanceExpansionPanelArrowPosition.leading) expandIconContainer,
          Expanded(
            child: AnimatedContainer(
              duration: widget.animationDuration,
              curve: Curves.fastOutSlowIn,
              margin: _isChildExpanded(index) ? widget.expandedHeaderPadding : EdgeInsets.zero,
              child: ConstrainedBox(
                constraints: const BoxConstraints(minHeight: _kPanelHeaderCollapsedHeight),
                child: headerWidget,
              ),
            ),
          ),
          if (child.arrow != EnhanceExpansionPanelArrowPosition.none && child.arrowPosition == EnhanceExpansionPanelArrowPosition.tailing) expandIconContainer,
        ],
      );
      Widget footer = Row(
        children: [
          Expanded(
            child: AnimatedContainer(
              duration: widget.animationDuration,
              curve: Curves.linear,
              margin: EdgeInsets.zero,
              child: ConstrainedBox(
                constraints: const BoxConstraints(minHeight: 0),
                child: footerWidget,
              ),
            ),
          ),
        ],
      );
      if (child.canTapOnHeader) {
        header = MergeSemantics(
          child: InkWell(
            onTap: () => _handlePressed(_isChildExpanded(index), index),
            child: header,
          ),
        );
      }
      items.add(
        MaterialSlice(
          key: _SaltedEnhanceKey<BuildContext, int>(context, index * 2),
          color: child.backgroundColor,
          child: Column(
            children: <Widget>[
              header,
              AnimatedCrossFade(
                firstChild: Container(height: 0.0),
                secondChild: child.body,
                firstCurve: const Interval(0.0, 0.6, curve: Curves.fastOutSlowIn),
                secondCurve: const Interval(0.4, 1.0, curve: Curves.fastOutSlowIn),
                sizeCurve: Curves.fastOutSlowIn,
                crossFadeState: _isChildExpanded(index) ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                duration: widget.animationDuration,
              ),
              if (child.arrowPosition == EnhanceExpansionPanelArrowPosition.bottom) footer,
            ],
          ),
        ),
      );

      if (_isChildExpanded(index) && index != widget.children.length - 1) {
        items.add(MaterialGap(key: _SaltedEnhanceKey<BuildContext, int>(context, index * 2 + 1)));
      }
    }

    return MergeableMaterial(
      hasDividers: true,
      dividerColor: widget.dividerColor,
      elevation: widget.elevation,
      children: items,
    );
  }
}
