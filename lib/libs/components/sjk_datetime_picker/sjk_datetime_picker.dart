library sjk_datetime_picker;

import 'package:flutter/material.dart';
import 'package:packing/libs/components/sjk_datetime_picker/src/sjk_datetime_picker.dart';
import 'package:packing/libs/components/sjk_datetime_picker/src/sjk_datetime_range_picker.dart';

Future<DateTime?> showSJKDateTimePicker({
  required BuildContext context,
  Widget? title,
  String? timeText,
  Widget? separator,
  DateTime? initialDate,
  DateTime? firstDate,
  DateTime? lastDate,
  EdgeInsetsGeometry? padding,
  double? timeItemHeight,
  bool? is24HourMode,
  bool? isShowSeconds,
  int? minutesInterval,
  int? secondsInterval,
  bool? isForce2Digits,
  BorderRadiusGeometry? borderRadius,
  BoxConstraints? constraints,
  Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
  transitionBuilder,
  Duration? transitionDuration,
  bool? barrierDismissible,
  SJKDateTimePickerType type = SJKDateTimePickerType.dateAndTime,
  final bool Function(DateTime)? selectableDayPredicate,
  ThemeData? theme,
}) {
  return showGeneralDialog(
    context: context,
    transitionBuilder: transitionBuilder ??
            (context, anim1, anim2, child) {
          return FadeTransition(
            opacity: anim1.drive(
              Tween(
                begin: 0,
                end: 1,
              ),
            ),
            child: child,
          );
        },
    transitionDuration: transitionDuration ?? const Duration(milliseconds: 200),
    barrierDismissible: barrierDismissible ?? true,
    barrierLabel: 'SJKDateTimePicker',
    pageBuilder: (BuildContext context, anim1, anim2) {
      return Theme(
        data: theme ?? Theme.of(context),
        child: SJKDateTimePicker(
          separator: separator,
          title: title,
          timeText: timeText,
          type: type,
          initialDate: initialDate,
          firstDate: firstDate,
          lastDate: lastDate,
          padding: padding,
          timeItemHeight: timeItemHeight,
          is24HourMode: is24HourMode,
          isShowSeconds: isShowSeconds,
          minutesInterval: minutesInterval,
          secondsInterval: secondsInterval,
          isForce2Digits: isForce2Digits,
          borderRadius: borderRadius,
          constraints: constraints,
          selectableDayPredicate: selectableDayPredicate,
        ),
      );
    },
  );
}

Future<List<DateTime>?> showSJKDateTimeRangePicker({
  required BuildContext context,
  String? startTimeText,
  String? endTimeText,
  DateTime? startInitialDate,
  DateTime? startFirstDate,
  DateTime? startLastDate,
  DateTime? endInitialDate,
  DateTime? endFirstDate,
  DateTime? endLastDate,
  EdgeInsetsGeometry? padding,
  double? timeItemHeight,
  bool? is24HourMode,
  bool? isShowSeconds,
  int? minutesInterval,
  int? secondsInterval,
  bool? isForce2Digits,
  BorderRadiusGeometry? borderRadius,
  BoxConstraints? constraints,
  Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
  transitionBuilder,
  Duration? transitionDuration,
  bool? barrierDismissible,
  SJKDateTimePickerType type = SJKDateTimePickerType.dateAndTime,
  bool Function(DateTime)? selectableDayPredicate,
  ThemeData? theme,
  DefaultView defaultView = DefaultView.start,
}) {
  return showGeneralDialog(
    context: context,
    transitionBuilder: transitionBuilder ??
            (context, anim1, anim2, child) {
          return FadeTransition(
            opacity: anim1.drive(
              Tween(
                begin: 0,
                end: 1,
              ),
            ),
            child: child,
          );
        },
    transitionDuration: transitionDuration ?? const Duration(milliseconds: 200),
    barrierDismissible: barrierDismissible ?? true,
    barrierLabel: 'SJKDateTimeRangePicker',
    pageBuilder: (BuildContext context, anim1, anim2) {
      return Theme(
        data: theme ?? Theme.of(context),
        child: SJKDateTimeRangePicker(
          type: type,
          startTimeText: startTimeText,
          endTimeText: endTimeText,
          startInitialDate: startInitialDate,
          startFirstDate: startFirstDate,
          startLastDate: startLastDate,
          endInitialDate: endInitialDate,
          endFirstDate: endFirstDate,
          endLastDate: endLastDate,
          padding: padding,
          timeItemHeight: timeItemHeight,
          is24HourMode: is24HourMode,
          isShowSeconds: isShowSeconds,
          minutesInterval: minutesInterval,
          secondsInterval: secondsInterval,
          isForce2Digits: isForce2Digits,
          borderRadius: borderRadius,
          constraints: constraints,
          selectableDayPredicate: selectableDayPredicate,
          defaultView: defaultView,
        ),
      );
    },
  );
}


enum SJKDateTimePickerType {
  date,
  dateAndTime,
}

/// Decides which tab open by default
enum DefaultView { start, end }
