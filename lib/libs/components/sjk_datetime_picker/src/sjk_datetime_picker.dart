import 'package:flutter/material.dart';
import 'package:packing/libs/components/sjk_datetime_picker/sjk_datetime_picker.dart';
import 'package:packing/libs/components/sjk_datetime_picker/src/variants/sjk_datetime_picker_variants/sjk_dtp_basic.dart';

class SJKDateTimePicker extends StatelessWidget {
  const SJKDateTimePicker(
      {super.key,
      this.timeText,
      this.separator,
      this.title,
      this.initialDate,
      this.firstDate,
      this.lastDate,
      this.padding,
      this.timeItemHeight,
      this.isShowSeconds,
      this.is24HourMode,
      this.minutesInterval,
      this.secondsInterval,
      this.isForce2Digits,
      this.borderRadius,
      this.constraints,
      required this.type,
      this.selectableDayPredicate});

  /// A widget that separates the [title] - if not null - and the calendar, also separates between date and time pickers
  final String? timeText;
  final Widget? separator;
  final Widget? title;
  final DateTime? initialDate;
  final DateTime? firstDate;
  final DateTime? lastDate;
  final EdgeInsetsGeometry? padding;
  final double? timeItemHeight;
  final bool? isShowSeconds;
  final bool? is24HourMode;
  final int? minutesInterval;
  final int? secondsInterval;
  final bool? isForce2Digits;
  final BorderRadiusGeometry? borderRadius;
  final BoxConstraints? constraints;
  final SJKDateTimePickerType type;
  final bool Function(DateTime)? selectableDayPredicate;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      alignment: Alignment.center,
      shape: Theme.of(context).useMaterial3
          ? null
          : borderRadius != null
              ? RoundedRectangleBorder(
                  borderRadius: borderRadius!,
                )
              : null,
      child: SJKDtpBasic(
        title: title,
        timeText: timeText,
        separator: separator,
        initialDate: initialDate,
        firstDate: firstDate,
        lastDate: lastDate,
        padding: padding,
        timeItemHeight: timeItemHeight,
        is24HourMode: is24HourMode,
        isShowSeconds: isShowSeconds,
        minutesInterval: minutesInterval,
        secondsInterval: secondsInterval,
        isForce2Digits: isForce2Digits,
        constraints: constraints,
        type: type,
        selectableDayPredicate: selectableDayPredicate,
      ),
    );
  }
}
