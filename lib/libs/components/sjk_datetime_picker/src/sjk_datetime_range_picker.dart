import 'package:flutter/material.dart';
import 'package:packing/libs/components/sjk_datetime_picker/src/variants/sjk_datetime_range_picker_variants/sjk_dtp_range.dart';
import 'package:packing/libs/components/sjk_datetime_picker/sjk_datetime_picker.dart';


class SJKDateTimeRangePicker extends StatelessWidget {
  const SJKDateTimeRangePicker({
    super.key,
    this.startTimeText,
    this.startInitialDate,
    this.endTimeText,
    this.startFirstDate,
    this.startLastDate,
    this.endInitialDate,
    this.endFirstDate,
    this.endLastDate,
    this.padding,
    this.timeItemHeight,
    this.isShowSeconds,
    this.is24HourMode,
    this.minutesInterval,
    this.secondsInterval,
    this.isForce2Digits,
    this.borderRadius,
    this.constraints,
    required this.type,
    this.selectableDayPredicate,
    this.defaultView = DefaultView.start,
  });
  final String? startTimeText;
  final String? endTimeText;
  final DateTime? startInitialDate;
  final DateTime? startFirstDate;
  final DateTime? startLastDate;
  final EdgeInsetsGeometry? padding;
  final DateTime? endInitialDate;
  final DateTime? endFirstDate;
  final DateTime? endLastDate;

  final double? timeItemHeight;
  final bool? isShowSeconds;
  final bool? is24HourMode;
  final int? minutesInterval;
  final int? secondsInterval;
  final bool? isForce2Digits;
  final BorderRadiusGeometry? borderRadius;
  final BoxConstraints? constraints;
  final SJKDateTimePickerType type;
  final bool Function(DateTime)? selectableDayPredicate;
  final DefaultView defaultView;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      alignment: Alignment.center,
      shape: Theme.of(context).useMaterial3
          ? null
          : borderRadius != null
              ? RoundedRectangleBorder(
                  borderRadius: borderRadius!,
                )
              : null,
      child: SJKDtpRange(
        startTimeText: startTimeText,
        endTimeText: endTimeText,
        startInitialDate: startInitialDate,
        startFirstDate: startFirstDate,
        startLastDate: startLastDate,
        endInitialDate: endInitialDate,
        endFirstDate: endFirstDate,
        endLastDate: endLastDate,
        padding: padding,
        timeItemHeight: timeItemHeight,
        is24HourMode: is24HourMode,
        isShowSeconds: isShowSeconds,
        minutesInterval: minutesInterval,
        secondsInterval: secondsInterval,
        isForce2Digits: isForce2Digits,
        constraints: constraints,
        type: type,
        selectableDayPredicate: selectableDayPredicate,
        defaultView: defaultView,
      ),
    );
  }
}
