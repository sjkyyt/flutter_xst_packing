import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:install_plugin/install_plugin.dart';
import 'package:open_filex/open_filex.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:path_provider/path_provider.dart';
import 'package:packing/libs/models/version.dart';
import 'package:packing/libs/utils/tools.dart';
import 'package:window_manager/window_manager.dart';

class AppUpdatePage extends StatelessWidget {
  final String plat;
  final VersionModel version;
  final String oldVersion;
  final bool autoUpdate;
  final void Function() onClosed;

  const AppUpdatePage({
    super.key,
    required this.plat,
    required this.version,
    required this.oldVersion,
    this.autoUpdate = true,
    required this.onClosed,
  });

  @override
  Widget build(BuildContext context) {
    List<String> descList = [];
    if (version.desc.isNotEmpty) {
      if (version.desc.contains('\n')) {
        for (String item in version.desc.split('\n')) {
          descList.add(item);
        }
      } else {
        descList.add(version.desc);
      }
    }
    double sizeHeight = Get.height;
    return Container(
      margin: EdgeInsets.symmetric(vertical: sizeHeight / 7, horizontal: sizeHeight / 10),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 250,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/update.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: Stack(
              children: [
                Container(
                  width: double.infinity,
                  margin: const EdgeInsets.only(top: 60),
                  child: Center(
                    child: Text.rich(
                      TextSpan(
                        text: '有新版本',
                        style: const TextStyle(color: Colors.white, fontSize: 20),
                        children: [
                          TextSpan(
                            text: 'v${version.version}',
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 20,
                  bottom: 0,
                  child: Text('v${version.version}更新说明', style: const TextStyle(fontWeight: FontWeight.w600)),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            height: 200,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: SingleChildScrollView(
                        child: Text(version.desc, style: const TextStyle(fontSize: 13), softWrap: true, overflow: TextOverflow.visible),
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(color: HexColor('#f3565b'), width: 0.5),
                    ),
                  ),
                  child: checkButtonOrProgress(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget checkButtonOrProgress() {
    final ValueNotifier<bool> isButton = ValueNotifier(true);
    final ValueNotifier<double> updateProgress = ValueNotifier(0.0);

    return ValueListenableBuilder(
      valueListenable: isButton,
      builder: (BuildContext context, bool value, Widget? child) {
        if (version.isForce == 1 && autoUpdate) {
          downloadApp(version.url, plat: plat, version: version.version, onProgress: (filePath, progress) => updateProgress.value = progress);
          return showProgressWidget(updateProgress);
        } else {
          return value ? showButtonWidget(isButton, (progress) => updateProgress.value = progress) : showProgressWidget(updateProgress);
        }
      },
    );
  }

  Widget showButtonWidget(ValueNotifier<bool> isButton, void Function(double progress) onCallback) {
    return version.isForce == 1
        ? updateButtonWidget(isButton, (progress) => onCallback(progress))
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: TextButton(
                  style: TextButton.styleFrom(
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30)),
                    ),
                  ),
                  onPressed: onClosed,
                  child: SizedBox(
                    height: 50,
                    child: Center(
                      child: Text('忽略', style: TextStyle(color: Colors.grey[800], fontWeight: FontWeight.w600)),
                    ),
                  ),
                ),
              ),
              VerticalDivider(color: HexColor('#f3565b'), width: 0.5),
              Expanded(
                child: updateButtonWidget(isButton, (progress) => onCallback(progress)),
              ),
            ],
          );
  }

  Widget updateButtonWidget(ValueNotifier<bool> isButton, void Function(double progress) onCallback) {
    return TextButton(
      style: TextButton.styleFrom(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(30)),
        ),
      ),
      onPressed: () {
        isButton.value = false;
        downloadApp(version.url, plat: plat, version: version.version, onProgress: (filePath, progress) => onCallback(progress));
      },
      child: const SizedBox(
        height: 50,
        child: Center(
          child: Text('升级', style: TextStyle(fontWeight: FontWeight.w600)),
        ),
      ),
    );
  }

  Widget showProgressWidget(ValueNotifier<double> updateProgress) {
    return Container(
      width: double.infinity,
      height: 50,
      padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 10),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      child: ValueListenableBuilder(
        valueListenable: updateProgress,
        builder: (BuildContext context, double progress, Widget? child) {
          String text = "${(progress * 100).toStringAsFixed(0)}%";
          return Column(
            children: [
              Container(
                padding: const EdgeInsets.all(2),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 1, color: HexColor('#f3565b')),
                  borderRadius: BorderRadius.circular(3),
                ),
                child: LinearProgressIndicator(
                  minHeight: 13,
                  value: progress,
                  backgroundColor: Colors.white,
                  color: HexColor('#d44044'),
                  borderRadius: const BorderRadius.all(Radius.circular(3)),
                ),
              ),
              const SizedBox(height: 3),
              Center(
                child: Text.rich(
                  TextSpan(
                    text: '下载中...',
                    style: const TextStyle(fontSize: 13),
                    children: [
                      TextSpan(text: text),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  static void downloadApp(
    String fileUrl, {
    required String plat,
    String version = '',
    required Function(String filePath, double progress) onProgress,
  }) async {
    double progressValue = 0.0;
    late Directory? appDownDir;
    late String savePath;
    String verCode = version.isNotEmpty ? version : Tools.getNowTimestamp().toString();

    if (plat == Plat.android.name) {
      appDownDir = await getTemporaryDirectory();
      savePath = "${appDownDir.path}/xst_packer_${verCode}_release.apk";
    } else if (plat == Plat.windows.name) {
      // appDownDir = Directory.current.path;
      appDownDir = await getDownloadsDirectory();
      savePath = "${appDownDir!.path}/xst_packer_${verCode}_install.exe";
    }

    await Dio().download(fileUrl, savePath, onReceiveProgress: (count, total) {
      final value = count / total;
      if (progressValue != value) {
        if (progressValue < 1.0) {
          progressValue = count / total;
        } else {
          progressValue = 0.0;
        }
        onProgress(savePath, progressValue);
      }
    });

    if (plat == Plat.android.name) {
      await InstallPlugin.install(savePath);
    }
    if (plat == Plat.windows.name) {
      await OpenFilex.open(savePath);
      await windowManager.destroy();
    }
  }

  /// 安装apk
  static void installApk(String path) {
    if (path.isEmpty) {
      // print('make sure the apk file is set');
      return;
    }

    InstallPlugin.installApk(path).then((result) {
      // print('install apk $result');
    }).catchError((error) {
      // print('install apk error: $error');
    });
  }
}
