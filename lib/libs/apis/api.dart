import 'package:packing/libs/apis/gateway.dart';
import 'package:packing/libs/apis/order.dart';

class API {
  static final Gateway gateway = Gateway();
  static final Order order = Order();
}
