import 'package:dio/dio.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart' show Get, GetNavigation, Inst;
import 'package:packing/libs/models/gateway.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/getx_controller.dart';

bool isContinue = false;

class Http {
  // final GetXOrderController getOrderController = Get.put(GetXOrderController());
  final GetXOrderController getOrderController = Get.find();
  final GetSettingController getSettingController = Get.find();
  late Dio _dio;

  Http() {
    _dio = Dio()
      ..options.baseUrl = Settings.baseUrl
      ..options.connectTimeout = Duration(milliseconds: Settings.connectTimeout)
      ..options.sendTimeout = Duration(milliseconds: Settings.connectTimeout)
      ..options.receiveTimeout = Duration(milliseconds: Settings.connectTimeout)
      ..interceptors.add(InterceptorsWrapper(
        onRequest: (RequestOptions options, RequestInterceptorHandler handler) {
          // 创建一个终端列表，这些不需要token
          final offTokenPaths = <String>['/packerLogin', '/getPackerAppVersion'];
          // 检查如果请求端点匹配
          if (offTokenPaths.contains(options.path.toString())) {
            // 如果端点匹配然后跳到追加令牌
            return handler.next(options);
          }
          // 在这里加载令牌到 header
          var token = getSettingController.login.accessToken;
          var tokenType = getSettingController.login.tokenType;
          options.headers.addAll({'Authorization': '$tokenType $token'});
          return handler.next(options);
        },
        onResponse: (Response response, ResponseInterceptorHandler handler) {
          // print("\n================== 响应数据 ==========================");
          // print("code = ${response.statusCode}");
          // print("data = ${response.data}");
          // print("\n");
          return handler.next(response);
        },
        onError: (DioException err, ErrorInterceptorHandler handler) {
          // print("\n================== 错误响应数据 ======================");
          // print("type = ${err.type}");
          // print("code = ${err.response?.statusCode}");
          // print("message = ${err.message}");
          // print("\n");
          _handleException(err);
          return handler.next(err);
        },
      ));
  }

  // GET方法
  Future<Response> get(String path, {Map<String, dynamic>? params}) async {
    return _dio.get(path, queryParameters: params).then((response) => response).catchError((err) => throw Exception('网络请求失败'));
  }

  // POST方法
  Future<Response> post(String path, {Map<String, dynamic>? params}) async {
    return _dio.post(path, data: params).then((response) => response).catchError((err) => throw Exception('网络请求失败'));
  }

  void _handleException(DioException err) {
    switch (err.type) {
      case DioExceptionType.connectionTimeout:
        EasyLoading.showError('连接超时');
        break;
      case DioExceptionType.sendTimeout:
        EasyLoading.showError('发送超时');
        break;
      case DioExceptionType.receiveTimeout:
        EasyLoading.showError('接收超时');
        break;
      case DioExceptionType.badCertificate:
        EasyLoading.showError('证书错误');
        break;
      case DioExceptionType.badResponse:
      // print('服务器响应出错');
      // EasyLoading.showError('服务器响应出错');
        int? statusCode = err.response?.statusCode;
        switch (statusCode) {
          case 400:
            EasyLoading.showError('请求语法错误');
            break;
          case 401:
            if (!isContinue) {
              isContinue = true;
              EasyLoading.showError('证书已过期，请重新登录');
              LoginModel login = getSettingController.login;
              login.expire = 0;
              login.save();
              getSettingController.setUser<LoginModel>('user', login);
              getOrderController.setPoll(false);
              Get.offAndToNamed('/login', arguments: {'registrationId': getSettingController.get<String>('registrationId', defaultValue: '')});
              Future.delayed(const Duration(seconds: 5)).then((value) => isContinue = false);
              break;
            }
          case 403:
            EasyLoading.showError('服务器拒绝执行');
            break;
          case 404:
            EasyLoading.showError('请求资源不存在');
            break;
          case 405:
            EasyLoading.showError('请求方法被禁止');
            break;
          case 500:
            EasyLoading.showError('服务器内部错误');
            break;
          case 502:
            EasyLoading.showError('无效请求');
            break;
          case 503:
            EasyLoading.showError('服务器异常');
            break;
          default:
            EasyLoading.showError('未知错误');
            break;
        }
      case DioExceptionType.cancel:
        EasyLoading.showError('链接被取消');
        break;
      case DioExceptionType.connectionError:
        EasyLoading.showError('连接错误');
        break;
      case DioExceptionType.unknown:
        EasyLoading.showError('未知链接错误');
        break;
    }
  }
}
