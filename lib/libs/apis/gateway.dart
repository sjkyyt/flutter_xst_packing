import 'package:dio/src/response.dart';
import 'package:packing/libs/apis/http.dart';

class Gateway extends Http {
  final Http http = Http();

  Future<Response> login(Map<String, dynamic>? params) async {
    return http.post('/packerLogin', params: params);
  }

  Future<Response> getPackerInfo(Map<String, dynamic>? params) async {
    return http.get('/v1/getPackerInfo', params: params);
  }

  Future<Response> onLine(Map<String, dynamic>? params) async {
    return http.post('/v1/packerOnLine', params: params);
  }

  Future<Response> offLine(Map<String, dynamic>? params) async {
    return http.post('/v1/packerOffLine', params: params);
  }

  // 获取同店其他在线拣货员
  Future<Response> getPackerList(Map<String, dynamic>? params) async {
    return http.get('/v1/getOnlinePacker', params: params);
  }

  // 获取APP版本信息
  Future<Response> getPackerAppVersion(Map<String, dynamic>? params) async {
    return http.get('/getPackerAppVersion', params: params);
  }
}
