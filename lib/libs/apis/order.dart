import 'package:dio/src/response.dart';
import 'package:packing/libs/apis/http.dart';

class Order {
  final Http http = Http();

  // 获取可拣货的订单
  Future<Response> getPackerPackOrder(Map<String, dynamic>? params) async {
    return http.get('/v1/getPackerPackOrder', params: params);
  }

  // 获取历史订单
  Future<Response> getPackOrderByDay(Map<String, dynamic>? params) async {
    return http.get('/v1/getPackOrderByDay', params: params);
  }

  // 完成订单
  Future<Response> finishPackOrder(Map<String, dynamic>? params) async {
    return http.post('/v1/finishPackOrder', params: params);
  }

  // 创建转单
  Future<Response> createPackTransferOrder(Map<String, dynamic>? params) async {
    return http.post('/v1/createPackTransferOrder', params: params);
  }

  // 取消转单
  Future<Response> closeTransferOrder(Map<String, dynamic>? params) async {
    return http.post('/v1/closeTransferOrder', params: params);
  }

  // 拒绝转单
  Future<Response> refuseTransferOrder(Map<String, dynamic>? params) async {
    return http.post('/v1/refuseTransferOrder', params: params);
  }

  // 同意转单
  Future<Response> receiveTransferOrder(Map<String, dynamic>? params) async {
    return http.post('/v1/receiveTransferOrder', params: params);
  }

  // 获取转单列表
  Future<Response> getPackTransferOrder(Map<String, dynamic>? params) async {
    return http.get('/v1/getPackTransferOrder', params: params);
  }
}
