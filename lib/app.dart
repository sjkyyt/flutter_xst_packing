import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jpush_flutter/jpush_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:packing/libs/utils/constants.dart';
import 'package:packing/libs/utils/version.dart';
import 'package:packing/libs/utils/getx_controller.dart';

class Apps extends StatefulWidget {
  const Apps({super.key});

  @override
  AppsState createState() => AppsState();
}

class AppsState extends State<Apps> {
  final GetSettingController getSettingController = Get.put(GetSettingController(), permanent: true);
  final GetXOrderController getOrderController = Get.put(GetXOrderController(), permanent: true);
  final ValueNotifier<String> initializationText = ValueNotifier('初始化');
  final JPush jpush = JPush();

  late bool isLogin;
  late Plat plat;
  String _registrationId = '';

  @override
  void initState() {
    if (GetPlatform.isFuchsia) {
      plat = Plat.fuchsia;
    } else if (GetPlatform.isWindows) {
      plat = Plat.windows;
    } else if (GetPlatform.isAndroid) {
      plat = Plat.android;
    } else if (GetPlatform.isIOS) {
      plat = Plat.ios;
    } else if (GetPlatform.isMacOS) {
      plat = Plat.macOs;
    } else if (GetPlatform.isLinux) {
      plat = Plat.linux;
    } else {
      plat = Plat.web;
    }
    getSettingController.set<String>('plat', plat.name);
    initialization();
    super.initState();
  }

  Future initialization() async {
    if (plat == Plat.android) {
      /// 极光推送初始化
      initializationText.value = '极光推送初始化...';
      jpush.setup(
        appKey: "6940936629b2068898a4f73c",
        channel: "thisChannel",
        production: true,
        debug: false, // 设置是否打印 debug 日志
      );

      jpush.applyPushAuthority(const NotificationSettingsIOS(sound: true, alert: true, badge: true));

      /// 获取注册的id
      initializationText.value = '正在获取极光推送ID...';
      _registrationId = await jpush.getRegistrationID();
      getSettingController.set<String>('registrationId', _registrationId);
      initializationText.value = '已获取极光推送ID';
    }

    getVersion(_registrationId);

    // 设置别名  实现指定用户推送
    //   jpush.setAlias(_registrationId).then((map) {
    //     // print("设置别名成功,$map");
    //   });
  }

  void getVersion(String registrationId) {
    initializationText.value = '获取当前版本...';
    PackageInfo.fromPlatform().then((packageInfo) {
      initializationText.value = '检查新版本...';
      Version.checkVersion(
        context,
        plat: plat.name,
        title: '您需要升级到新版本',
        version: packageInfo.version,
        autoUpdate: true,
        skipExecution: () {
          skipExecution(registrationId: registrationId);
        },
        updateBack: (isNew, backText, platNumber) {
          if ((platNumber == 1 && plat.name == Plat.android.name) || (platNumber == 4 && plat.name == Plat.windows.name)) {
            getSettingController.set<bool>('newVersion', isNew);
          }
          initializationText.value = backText;
        },
      );
    });
  }

  void skipExecution({String? registrationId}) {
    Future.delayed(const Duration(milliseconds: 100)).then((_) {
      if (mounted) {
        if (getSettingController.isLogin) {
          Get.offAllNamed('/home');
        } else {
          Get.offAllNamed('/login', arguments: {'registrationId': registrationId});
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffff4444),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            const Image(
              image: AssetImage('assets/images/logo.png'),
              fit: BoxFit.fill,
              width: 150,
              height: 150,
            ),
            const SizedBox(height: 10),
            ValueListenableBuilder(
              valueListenable: initializationText,
              builder: (BuildContext context, String text, Widget? child) => Text(text, style: const TextStyle(color: Colors.white, fontSize: 13)),
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: 30,
              width: 30,
              child: CircularProgressIndicator(
                backgroundColor: Colors.grey[400],
                valueColor: const AlwaysStoppedAnimation<Color>(Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
